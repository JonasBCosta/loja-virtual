<%-- <%@page import="br.com.nomedositeinvertido.negocio.impl.VerificaTimerCarrinho"%> --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="br.com.nomedositeinvertido.controller.impl.Fachada" %>
<%@ page import="br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<title>Cadastro</title>

		<script type="text/javascript">
		/* M�scaras ER */
		function mascara(o,f){

		    v_obj=o
		    v_fun=f
		    setTimeout("execmascara()",1)
		}
		function execmascara(){

		    v_obj.value=v_fun(v_obj.value)
		}
		function mtel(v){

		    v=v.replace(/D/g,"");             //Remove tudo o que n�o � d�gito
		    v=v.replace(/^(d{2})(d)/g,"($1) $2"); //Coloca par�nteses em volta dos dois primeiros d�gitos
		    v=v.replace(/(d)(d{4})$/,"$1-$2");    //Coloca h�fen entre o quarto e o quinto d�gitos
		    return v;
		}
		function id( el ){

			return document.getElementById( el );
		}
		window.onload = function(){

			id('txtTelefone').onkeypress = function(){
				mascara( this, mtel );
			}
		}
		</script>
		
		<script type="text/javascript">
			function menuCateg(categoria){				
				document.getElementById('hdnCateg').value = categoria;
				document.formMenuCateg.submit();
			}
			function valCampRequer(){
				var x = document.getElementsByClassName("required");
				var i;
				for (i = 0; i < x.length; i++) {
					if(x[i].value == ""){
						alert("Insira dados no campos marcados com *");
						i = x.length;
					}else if(i == (x.length-1)){
						document.formSalvarCliente.submit();
					}
				}
			}
			function alerta(){				
				var retorno = confirm("Tem certeza que quer deletar este cadastro?");
				if(retorno == true){
					document.formExcluir.submit();
				}				
			}
			function valCampErro(){
				alert("Este");
				alert('${requestScope.mensagemErro}');
			}
		</script>
</head>
<body>	

	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />	
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
				        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
				        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
				        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
				        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
				        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
				        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${sessionScope.clienteLogado.id > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${sessionScope.clienteLogado.id}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<div class="navegacao">
			<!-- menu de navega��o Busca de produtos por parametro -->
			<h3> Compre por departamento</h3>
			<ul>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Eletrodomesticos&logon=${sessionScope.clienteLogado.id}" >Eletrodom�sticos</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Telefones&logon=${sessionScope.clienteLogado.id}" >Telefones e Celulares</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Computadores&logon=${sessionScope.clienteLogado.id}" >Computadores e Notebooks</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Moveis&logon=${sessionScope.clienteLogado.id}" >M�veis</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Livros&logon=${sessionScope.clienteLogado.id}" >Livros</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Utensilios&logon=${sessionScope.clienteLogado.id}" >Utens�lios</a></li><br>
			</ul>
		</div>

		<div class="secao">
			<form action="SalvarCliente" method="post" name="formSalvarCliente">			
						
			<div id="log">
				<label> Apelido: </label><label class="lblRestrict">* </label><br>            <!-- Futuramente este campo ficar� invis�vel pra quem se logar -->
				<label> Nome: </label><label class="lblRestrict">* </label><br>
				<label> CPF: </label><label class="lblRestrict">* </label><br>
				<label> Data/Nasc: </label><label class="lblRestrict">* </label><br>
				<label> Telefone: </label><br>
				<label> Celular: </label><br>
				<label> Email: </label><label class="lblRestrict">* </label><br>
				<label> Senha: </label><label class="lblRestrict">* </label><br> <!-- Futuramente este campo ficar� invis�vel pra quem se logar -->
				<label> Repita a senha: </label><label class="lblRestrict">* </label><br> <!-- Futuramente este campo ficar� invis�vel pra quem se logar -->
				<label> Cep: </label><label class="lblRestrict">* </label><br>
				<label> Estado: </label><label class="lblRestrict">* </label><br>
				<label> Cidade: </label><label class="lblRestrict">* </label><br>
				<label> Bairro: </label><label class="lblRestrict">* </label><br>
				<label> Rua: </label><label class="lblRestrict">* </label><br>
				<label> N�mero: </label><label class="lblRestrict">* </label><br>
				<label> Complemento: </label><br>
			</div>
			
			<section2>
				<input type="text" size="50" maxlength="25" class="required" name="txtLogin" value="${sessionScope.clienteLogado.login}" /> <br>
				<input type="text" size="50" maxlength="50" class="required" name="txtNome" value="${sessionScope.clienteLogado.nome}" /> <br>		
				<input type="text" size="50" maxlength="14" class="required" name="txtCPF" value="${sessionScope.clienteLogado.getCPF()}" /> <br>		
				<input type="text" size="50" maxlength="10" class="required" name="txtDataNasc" value="${sessionScope.clienteLogado.dataNascimento}" /> <br>		
				<input type="text" size="50" maxlength="14" id="txtTelefone" name="txtTelefone" value="${sessionScope.clienteLogado.telefone}" /> <br>		
				<input type="text" size="50" maxlength="14" class="nonRequi" name="txtCelular" value="${sessionScope.clienteLogado.celular}" /> <br>		
				<input type="text" size="50" maxlength="64" class="required" name="txtEmail" value="${sessionScope.clienteLogado.email}" /> <br>		
				<input type="text" size="50" maxlength="32" class="required" name="txtSenha" value="${sessionScope.clienteLogado.senha}" /> <br>		
				<input type="text" size="50" maxlength="32" class="required" name="txtRepSenha" value="${sessionScope.clienteLogado.senha}" /> <br>		
				<input type="text" size="50" maxlength="9"  class="required" name="txtCep" value="${sessionScope.clienteLogado.getEndereco().cep}" /> <br>		
				<input type="text" size="50" maxlength="2"  class="required" name="txtEstado" value="${sessionScope.clienteLogado.getEndereco().estado}" /> <br>		
				<input type="text" size="50" maxlength="25" class="required" name="txtCidade" value="${sessionScope.clienteLogado.getEndereco().cidade}" /> <br>		
				<input type="text" size="50" maxlength="25" class="required" name="txtBairro" value="${sessionScope.clienteLogado.getEndereco().bairro}" /> <br>		
				<input type="text" size="50" maxlength="25" class="required" name="txtRua" value="${sessionScope.clienteLogado.getEndereco().rua}" /> <br>		
				<input type="text" size="50" maxlength="5"  class="required" name="txtNumero" value="${sessionScope.clienteLogado.getEndereco().numero}" /> <br>	
				<input type="text" size="50" maxlength="40" class="nonRequi" name="txtComplemento" value="${sessionScope.clienteLogado.getEndereco().complemento}" /> <br>
			</section2>	
			<c:if test="${!empty sessionScope.mensagemErro}">
			<label>${sessionScope.mensagemErro}</label>
				<script>
					valCampErro();
				</script>
			</c:if>
			<c:choose>
				<c:when test="${empty sessionScope.clienteLogado}">
					<input type="button" value="Cadastrar" onclick="valCampRequer();"/>
					<input type="hidden" name="OPERACAO" value="Cadastrar" />
				</c:when>
				<c:otherwise>
					<input type="button" value="Alterar" onclick="valCampRequer();"/>
					<input type="button" value="Exclu�r" onclick="alerta();"/>
					<input type="hidden" name="OPERACAO" value="Alterar" />
				</c:otherwise>			
			</c:choose>					
			</form>
		</div>
		<div class="ofertas">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=SetorOfertas"/>
		</div>
		<div class="rodap�">	
			<div id="linha">
				<c:set var="i" value="${0}"/>
				<c:forEach items="${produtos}" var="produto">
					<c:set var="total" value="${total+1}"/>
					<c:if test="${total != 12}">
					    <li>
					    <div id="img">
					    <a href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${sessionScope.clienteLogado.id}">
						<img alt="${produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" /></a></div><br> <!-- Exibe a Imagem do produto--> 
						<h3><label> ${produto.nome} </label></h3>	<!-- Exibe o link-nome do produto-->    
						<label>Pre�o: </label> ${produto.preco} <!-- Exibe o pre�o do produto -->
						</li>   
					</c:if>
				</c:forEach>
			</div>
		<a name="contato"></a>
			<div class="contato">
				FALE CONOSCO <br><br>
				
				COMPRE PELO TELEFONE: <br> 
				(11) 1234-1234 - (11) 4321-4321<br><br>
				 
				SAC: (11) 2314-2314 - (11) 3412-3412
			</div>
		</div>
	</div>
	<form name="formExcluir" action="ExcluirCliente" method="post">
		<input type="hidden" name="OPERACAO" value="Exclu�r" />
	</form>

</body>
</html>