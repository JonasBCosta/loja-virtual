<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import= "java.util.ArrayList"%>
<%@ page import= "java.util.List"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ page import= "br.com.nomedositeinvertido.jdbc.impl.ItemDAO"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.item.Item"%>
<%@ page import= "br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb"%>
<%@ page import= "br.com.nomedositeinvertido.controller.IFachada" %>
<%@ page import= "br.com.nomedositeinvertido.controller.impl.Fachada"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.Resultado"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<title>Hist�rico de compras</title>
<!-- 	<script type="text/javascript"> 
// 		function excPedido(){
// 			var retorno = confirm("Tem certeza que quer cancelar este pedido?");
// 			if(retorno == true){
// 				document.formExcluirPedido.submit();
// 			}
// 		}
	
// 		document.formValidarPedido.submit();
<!-- 	</script> -->
		<script type="text/javascript">
			function menuCateg(categoria){				
				document.getElementById('hdnCateg').value = categoria;
				document.formMenuCateg.submit();
			}
		</script>

</head>
<body>

	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />	
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
						        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
						        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
						        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
						        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
						        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
						        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${sessionScope.clienteLogado.id > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${sessionScope.clienteLogado.id}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<form action="ValidarPedido" method="post" name="formValidarPedido"></form>
		<header>
			<div id="topo2">
				<label style="font-size: 20px;"> Bem vindo ao seu hist�rico de compras de compras sr(a) ${sessionScope.clienteLogado.login}</label><br><br>
			</div>	
		</header>
		<nav>
		</nav>	
		<section>
		</section>	
		<article>
		</article>	
		<aside>
		</aside>
		<footer>
		<c:if test="${not empty sessionScope.pedidosCliente}"> 
			<c:forEach var="i" begin="0" end="${fn:length(pedidosCliente)-1}"> 
				<c:set var="pedidoCliente" scope="session" value="${pedidosCliente.get(i)}"/>
				<div class="tabela">
					<br><table width="420px">
						<tr><td align="left"><label>STATUS: </label> ${pedidoCliente.status} </td></tr>
						<c:choose>
							<c:when test="${pedidoCliente.status == 'Finalizado'}">
								<tr><td align="left"><label>Data do pedido: </label> ${pedidoCliente.data} </td></tr>
								<tr><td align="left"><label>Conclu�do em: </label> ${pedidoCliente.expiracao} </td></tr>
							</c:when>				
							<c:otherwise>
								<tr><td align="left"><label>Data do pedido: </label> ${pedidoCliente.data} </td></tr>
								<tr><td align="left"><label>Expira em: </label> ${pedidoCliente.expiracao} </td></tr>
							</c:otherwise>
						</c:choose>
					</table>
					<table  border="2">
						<tr>
						<th width="60px"> FOTO </th>
						<th width="300px"> NOME </th> 
						<th width="80px"> PRE�O UNIT. </th>
					    <th width="80px"> QUANTIDADE </th></tr>
					    
						<c:set var="total" value="${0}"/>											
						<c:forEach var="j" begin="0" end="${fn:length(pedidoCliente.itens)-1}">
							<c:set var="itemCliente" scope="session" value="${pedidosCliente.get(i).itens.get(j)}"/>
							<tr>
								<td><img alt="${itemCliente.produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${itemCliente.produto.id}" width="60" height="60"/></td>
								<td>${itemCliente.produto.nome} </td>  						
								<td>${itemCliente.produto.preco} </td>
								<td>${itemCliente.quantidade}</td>
								<c:set var="valor" value="${itemCliente.produto.preco * itemCliente.quantidade}"/>
								<c:set var="total" value="${valor + total}" />						
								</form>					
							</tr>			
						</c:forEach>						
						
					</table>
					<table width="420px">
						<tr><td align="right"><label>Total: </label> ${total} </td></tr>
					</table>				
				</div>
			<br><br>
			</c:forEach>
		</c:if>
		
		</footer>
			<div class="contato">
				FALE CONOSCO <br><br>
				
				COMPRE PELO TELEFONE: <br> 
				(11) 1234-1234 - (11) 4321-4321<br><br>
				 
				SAC: (11) 2314-2314 - (11) 3412-3412
			</div>
	</div>
</body>
</html>