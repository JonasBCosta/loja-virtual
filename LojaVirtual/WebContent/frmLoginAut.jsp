<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import= "java.util.ArrayList"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ page import= "br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<sql:setDataSource var="dSource"
 					   driver="org.postgresql.Driver"
 					   url="jdbc:postgresql://localhost:5432/postgres"
 					   user="postgres"
 					   password="postgres"
 					   scope="request"/>
 	<sql:query var="prods" dataSource="${dSource}" >
		SELECT * FROM produtos			
	</sql:query>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<title>Login</title>
		<script type="text/javascript">
			function menuCateg(categoria){				
				document.getElementById('hdnCateg').value = categoria;
				document.formMenuCateg.submit();
			}
		</script>
</head>
<body>

	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />		
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
						        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
						        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
						        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
						        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
						        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
						        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${sessionScope.clienteLogado.id > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${sessionScope.clienteLogado.id}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<div class="navegacao">
			<!-- menu de navega��o Busca de produtos por parametro -->
			<h3> Compre por departamento</h3>
			<ul>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Eletrodom�sticos" >Eletrodom�sticos</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Telefones" >Telefones e Celulares</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Computadores" >Computadores e Notebooks</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=M�veis" >M�veis</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Livros" >Livros</a></li><br>
				<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Utens�lios" >Utens�lios</a></li><br>
			</ul>
		</div> 
		<div class="secao">
			<form action="LoginCliente" method="post">
					<label> Apelido: </label><br>
					<input type="text" name="txtLogin" value="" size="30" maxlength="25" /><br>
		
					<label> Senha: </label><br>
					<input type="text" name="txtSenha" value="" size="30" maxlength="32" /><br>
					<c:if test="${!empty resultAut}" >
						${resultAut}
					</c:if>
					<br>
					<input type="submit" name="OPERACAO" id="OPERACAO" value="Entrar" />
			</form>
			<a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">
			<input type="button" value="Ainda n�o sou Cadastrado" name="voltar" /></a>
		</div>
		<div class="ofertas">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=SetorOfertas"/>
		</div>
		<div class="rodap�">									
<!-- 			<form name="apresProds" action="LoginCliente" method="post"> -->
				<c:choose>
					<c:when test="${prods != null}">
						<div id="linha">
							<c:set var="i" value="${0}"/>
							<c:forEach items="${prods.rows}" var="produto">
								<c:set var="total" value="${total+1}"/>
								<c:if test="${total != 12}">
								    <li>
								    <div id="img">
								    <a href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${sessionScope.clienteLogado.id}">
									<img alt="${produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" /></a></div><br> <!-- Exibe a Imagem do produto--> 
									<h3><label> ${produto.nome} </label></h3>	<!-- Exibe o link-nome do produto-->    
									<label>Pre�o: </label> ${produto.preco} <!-- Exibe o pre�o do produto -->
									</li>   
								</c:if>
							</c:forEach>
						</div>
					</c:when>
					<c:otherwise>
						<script type="text/javascript">document.apresProds.submit();</script>
					</c:otherwise>
				</c:choose>
<!-- 			</form> -->
			
			<a name="contato">
			</a><div class="contato">
				FALE CONOSCO <br><br>
				
				COMPRE PELO TELEFONE: <br> 
				(11) 1234-1234 - (11) 4321-4321<br><br>
				 
				SAC: (11) 2314-2314 - (11) 3412-3412
			</div>
		</div>
	</div>
</body>
</html>