<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ page import= "br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb"%>
<%@ page import= "br.com.nomedositeinvertido.controller.impl.Fachada" %>
<%@ page import= "br.com.nomedositeinvertido.dominio.Resultado" %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<sql:setDataSource var="dSource"
 					   driver="org.postgresql.Driver"
 					   url="jdbc:postgresql://localhost:5432/postgres"
 					   user="postgres"
 					   password="postgres"
 					   scope="request"/>
 					   
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<title>Descri��o do produto</title>
		<script type="text/javascript">
			function menuCateg(categoria){				
				document.getElementById('hdnCateg').value = categoria;
				document.formMenuCateg.submit();
			}
		</script>
</head>
<body>
	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />	
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
				        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
				        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
				        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
				        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
				        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
				        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${idCliente > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${idCliente}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<div class="navegacao">

		<!-- menu de navega��o Busca de produtos por parametro -->
		<h3> Compre por departamento</h3>
		<ul>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Eletrodom�sticos&logon=${sessionScope.clienteLogado.id}" >Eletrodom�sticos</a></li><br>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Telefones&logon=${sessionScope.clienteLogado.id}" >Telefones e Celulares</a></li><br>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Computadores&logon=${sessionScope.clienteLogado.id}" >Computadores e Notebooks</a></li><br>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=M�veis&logon=${sessionScope.clienteLogado.id}" >M�veis</a></li><br>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Livros&logon=${sessionScope.clienteLogado.id}" >Livros</a></li><br>
			<li><a href="http://localhost:8083/LojaVirtual/frmProdutos.jsp?categoria=Utens�lios&logon=${sessionScope.clienteLogado.id}" >Utens�lios</a></li><br>
		</ul>

		</div>
		
<!-- ///////////// TRECHO SQL ////////////////////////////////////////// --------------------------------------------- -->
<!-- ////// Busca todos os produtos cadastrados no banco de dados ////// --------------------------------------------- -->
		
	 	<sql:query var="prods" dataSource="${dSource}" >
	 		SELECT * FROM produtos			
	 	</sql:query>
	 	
<!-- /////////////////////////////////////////////////////////////////// ---------------------------------------------- -->	 	
	 	
	 	<c:forEach var="prod" items="${prods.rows}">
	 		<c:if test="${prod.id == param.indice}">
	 			<c:set var="produto" value="${prod}"/>
	 		</c:if>
	 	</c:forEach>
	 	

		<div class="secao">
		<img alt="${produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" /><br>
		<label>Nome: </label> ${produto.nome} <br>
		<label>Pre�o: </label> ${produto.preco} <br>
		<label>Descri��o do produto: </label><br> ${produto.descricao} <br><br>	
		
		<form action="CadastroItem" method="post">
			
			<input type="hidden" name="txtProduto" value="${produto.id}"/>
			
			<!-- Considere usar um HashMap para puxar o prod depois na servlet -->
			<!-- Tente descobrir uma maneira de passar um objeto inteiro do formulario pra servlet e vice-versa -->
			<!-- Solucionar: Banco posi��o no banco de dados que cont�m outro banco de dados -->
			
			<script type="text/javascript">
				function alerta1(){
					alert("Para adicionar itens ao carrinho � necessario se cadastrar");				
				}
				function alerta2(){
					alert("Lamentamos informar mas este produto est� em falta");				
				}
			</script>
			<c:choose>
				<c:when test="${empty sessionScope.clienteLogado}">
					<input type="button" value="Adicionar ao carrinho" name="AdCarr" onclick="alerta1();"/>
				</c:when>
				<c:when test="${sessionScope.clienteLogado.id < 1}">
					<input type="button" value="Adicionar ao carrinho" name="AdCarr" onclick="alerta1();"/>
				</c:when>
				<c:when test="${produto.quantidade < 1}">
					<input type="button" value="Adicionar ao carrinho" name="AdCarr" onclick="alerta2();"/>
				</c:when>
				<c:otherwise>
					<input type="submit" name="OPERACAO" id="OPERACAO" value="Adicionar ao carrinho" name="AdCarr"/>
				</c:otherwise>
			</c:choose>
			<label>Quantidade</label>
			<input type="text" maxlength="2" value="1" name="txtQtde" size="3"/> <!-- compras acima de 5 unidades entre em contato com os nossos revendedores -->
		
		</form>
		</div>
		<div class="ofertas">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=SetorOfertas"/>
		</div>
		<div class="rodap�">									
			<form name="apresProds" action="DescricaoProduto" method="post">
				<c:choose>
					<c:when test="${prods != null}">
						<div id="linha">
							<c:set var="i" value="${0}"/>
							<c:forEach items="${prods.rows}" var="produto">
								<c:set var="total" value="${total+1}"/>
								<c:if test="${total != 12}">
								    <li>
								    <div id="img">
								    <a href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${param.logon}">
									<img alt="${produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" /></a></div><br> <!-- Exibe a Imagem do produto--> 
									<h3><label> ${produto.nome} </label></h3>	<!-- Exibe o link-nome do produto-->    
									<label>Pre�o: </label> ${produto.preco} <!-- Exibe o pre�o do produto -->
									</li>   
								</c:if>
							</c:forEach>
						</div>
					</c:when>
					<c:otherwise>
						<script type="text/javascript">document.apresProds.submit();</script>
					</c:otherwise>
				</c:choose>
			</form>
			
			<a name="contato"></a>
			<div class="contato">
				FALE CONOSCO <br><br>
				
				COMPRE PELO TELEFONE: <br> 
				(11) 1234-1234 - (11) 4321-4321<br><br>
				 
				SAC: (11) 2314-2314 - (11) 3412-3412
			</div>
			
			
		</div>
	</div>
</body>
</html>