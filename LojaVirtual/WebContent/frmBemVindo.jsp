<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ page
	import="br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="estilo.css" />

<script type="text/javascript">
	function menuCateg(categoria) {
		document.getElementById('hdnCateg').value = categoria;
		document.formMenuCateg.submit();
	}
</script>
<style>
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	width: 70%;
	margin: auto;
}

#myCarousel {
	width: 600px;
	height: 336px;
}
</style>
<title>Bem Vindo!</title>
</head>
<body>
	<div class="geral">
		<div class="topo">
			<img
				src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />
			<div class="menu">
				<ul class="menu-list">
					<li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
					<li><a href="#">Departamentos</a>
						<ul class="sub-menu">
							<c:choose>
								<c:when test="${sessionScope.categorias != null}">
																		<c:forEach items="${categorias}" var="categoria">
																			<li><input type="button" class="botaoLink"
																				value="${categoria}"
																				onclick="menuCateg('${categoria}')"></li>
																		</c:forEach>
								</c:when>
								<c:otherwise>
									<script type="text/javascript">
 										document.apresProds.submit();
 									</script> 
								</c:otherwise>
							</c:choose>
						</ul></li>
					<c:choose>
						<c:when test="${sessionScope.clienteLogado.id > 0}">
							<li><a
								href="http://localhost:8083/LojaVirtual/CadastroCliente">Ver
									meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver
									meu carrinho</a></li>
							<li><a
								href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a
								href="http://localhost:8083/LojaVirtual/CadastroCliente">N�o
									sou cadastrado </a></li>
							<li><a
								href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar
									na minha conta </a></li>
						</c:otherwise>
					</c:choose>
					<li><a href="#contato">Fale conosco</a></li>
				</ul>
			</div>
			<form name="formMenuCateg" action="FiltrarBusca" method="post">
				<input type="hidden" id="hdnIdCli" name="hdnIdCli"
					value="${sessionScope.clienteLogado.id}"> <input
					type="hidden" id="hdnCateg" name="hdnCateg">
			</form>
			<c:choose>
				<c:when test="${sessionScope.clienteLogado.id > 0}">
					<br>
					<label> Bem vindo ${sessionScope.clienteLogado.login}! </label>
					<br>
					<br>
				</c:when>
				<c:otherwise>
					<br>
					<label> Ol� visitante! </label>
					<br>
					<br>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="navegacao">
			<%-- 			<c:choose> --%>
			<%-- 				<c:when test="${sessionScope.produtos != null}"> --%>
			<!-- menu de navega��o Busca de produtos por parametro -->
			<h3>Compre por departamento</h3>
			<ul>
				<c:choose>
					<c:when test="${sessionScope.categorias != null}">
<!-- 						<li><input type="button" class="botaoLink" -->
<!-- 							value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li> -->
<!-- 						<li><input type="button" class="botaoLink" -->
<!-- 							value="Telefones e Celulares" onclick="menuCateg('Telefones')" -->
<!-- 							width="200px"></li> -->
<!-- 						<li><input type="button" class="botaoLink" -->
<!-- 							value="Computadores e Notebooks" -->
<!-- 							onclick="menuCateg('Computadores')" width="200px"></li> -->
<!-- 						<li><input type="button" class="botaoLink" value="M�veis" -->
<!-- 							onclick="menuCateg('M�veis')"></li> -->
<!-- 						<li><input type="button" class="botaoLink" value="Livros" -->
<!-- 							onclick="menuCateg('Livros')"></li> -->
<!-- 						<li><input type="button" class="botaoLink" value="Utens�lios" -->
<!-- 							onclick="menuCateg('Utens�lios')"></li> -->
										    <c:forEach items="${categorias}" var="categoria">
										        <li><input type="button" class="botaoLink" value="${categoria}" onclick="menuCateg('${categoria}')"></li>
										    </c:forEach> 
					</c:when>
					<c:otherwise>
						<script type="text/javascript">
 							document.apresProds.submit(); 
						</script> 
					</c:otherwise>
				</c:choose>

			</ul>
			<%-- 				</c:when> --%>
			<%-- 				<c:otherwise> --%>
			<!-- 					<script type="text/javascript">document.apresProds.submit();</script> -->
			<%-- 				</c:otherwise> --%>
			<%-- 			</c:choose> --%>
		</div>
		<div class="secao">
			<div class="container">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
						<li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
						<li data-target="#myCarousel" data-slide-to="5"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">

						<%-- 			    <c:choose> --%>
						<%-- 					<c:when test="${sessionScope.produtos != null}">						 --%>
						<c:choose>
							<c:when test="${sessionScope.produtos != null}">
								<c:set var="total" value="${0}" />
								<c:forEach items="${produtos}" var="produto">
									<c:set var="total" value="${total+1}" />
									<c:choose>
										<c:when test="${total == 1}">
											<div class="item active">
												<div id="quadroCar">
													<li>
														<div id="img">
															<a
																href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${sessionScope.clienteLogado.id}">
																<img alt="${produto.nome}"
																src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}"
																width="300" height="250" />
															</a>
														</div> <!-- Exibe a Imagem do produto-->
														<h3>
															<label id="lblCar"> ${produto.nome} </label>
														</h3> <!-- Exibe o link-nome do produto--> <label id="lblCar">Pre�o:
													</label> ${produto.preco} <!-- Exibe o pre�o do produto -->
													</li>
												</div>
											</div>
										</c:when>
										<c:otherwise>
											<c:if test="${total < 7}">
												<div class="item">
													<div id="quadroCar">
														<li>
															<div id="img">
																<a
																	href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${sessionScope.clienteLogado.id}">
																	<img alt="${produto.nome}"
																	src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}"
																	width="300" height="250" />
																</a>
															</div> <!-- Exibe a Imagem do produto-->
															<h3>
																<label id="lblCar"> ${produto.nome} </label>
															</h3> <!-- Exibe o link-nome do produto--> <label id="lblCar">Pre�o:
														</label> ${produto.preco} <!-- Exibe o pre�o do produto -->
														</li>
													</div>
												</div>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<script type="text/javascript">
									document.apresProds.submit();
								</script>
							</c:otherwise>
						</c:choose>
					</div>

					<!-- Left and right controls -->
					<a class="left carousel-control" href="#myCarousel" role="button"
						data-slide="prev"> <span
						class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a> <a class="right carousel-control" href="#myCarousel" role="button"
						data-slide="next"> <span
						class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

		</div>
		<div class="ofertas">
			<img
				src="${pageContext.request.contextPath}/DownloadImagens?img=SetorOfertas" />
			<%
				int cont = 0;
			%>

		</div>

		<div class="rodap�">
			<form name="apresProds" action="BemVindo" method="post">
				<c:choose>
					<c:when test="${sessionScope.produtos != null}">
						<div id="linha">
							<c:set var="total" value="${0}" />
							<c:forEach items="${produtos}" var="produto">
								<c:set var="total" value="${total+1}" />
								<c:if test="${total < 13}">
									<li>
										<div id="img">
											<a
												href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${sessionScope.clienteLogado.id}">
												<img alt="${produto.nome}"
												src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" />
											</a>
										</div> <br> <!-- Exibe a Imagem do produto-->
										<h3>
											<label> ${produto.nome} </label>
										</h3> <!-- Exibe o link-nome do produto--> <label>Pre�o: </label>
										${produto.preco} <!-- Exibe o pre�o do produto -->
									</li>
								</c:if>
							</c:forEach>
						</div>
					</c:when>
					<c:otherwise>
						<script type="text/javascript">
							document.apresProds.submit();
						</script>
					</c:otherwise>
				</c:choose>
			</form>
			<a name="contato"></a>
			<div class="contato">
				<label>FALE CONOSCO</label> <br> <br> <label>COMPRE
					PELO TELEFONE:</label> <br> <label>(11) 1234-1234 - (11)
					4321-4321</label> <br> <br> <label>SAC: (11) 2314-2314 -
					(11) 3412-3412</label>
			</div>
		</div>
	</div>
</body>
</html>