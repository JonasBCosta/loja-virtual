<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import= "java.util.ArrayList"%>
<%@ page import= "java.util.List"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ page import= "br.com.nomedositeinvertido.jdbc.impl.ItemDAO"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.item.Item"%>
<%@ page import= "br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb"%>
<%@ page import= "br.com.nomedositeinvertido.controller.IFachada" %>
<%@ page import= "br.com.nomedositeinvertido.controller.impl.Fachada"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.Resultado"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri= "http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<title>Carrinho de Compras</title>
	<script type="text/javascript">
		function excPedido(){
			var retorno = confirm("Tem certeza que quer cancelar este pedido?");
			if(retorno == true){
				document.formExcluirPedido.submit();
			}
		}
		document.formValidarPedido.submit();		
		function menuCateg(categoria){				
			document.getElementById('hdnCateg').value = categoria;
			document.formMenuCateg.submit();
		}
		function altStsPed(){
			document.formPagSeg.submit();
			setTimeout(function(){ document.formAltStsPed.submit();}, 300);   
		}
	</script>
</head>
<body>

	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />		
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
						        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
						        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
						        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
						        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
						        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
						        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${sessionScope.clienteLogado.id > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/frmCadastro.jsp">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${sessionScope.clienteLogado.id}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<form action="ValidarPedido" method="post" name="formValidarPedido"></form>
		<header>
			<div id="topo2">
				<label id="lblBV"> Bem vindo ao seu carrinho de compras sr(a) ${sessionScope.clientePedAberto.login}</label><br><br>
				<c:if test="${!empty requestScope.msgItem}">
					<c:if test="${msgItem != 'A quantidade do produto foi atualizada' }">
						<label class="lblRed">O produto n�o foi adicionado no carrinho.</label><br>
						<label class="lblRed">Motivo: ${msgItem}</label>
					</c:if>
				</c:if>
				<c:if test="${msgItem eq 'A quantidade do produto foi atualizada' }">
					<label id="lblGreen">${msgItem}</label><br>
				</c:if>
				<c:if test="${empty msgItem}">
					<c:if test="${item.id >= 0}">
						<label id="lblBlue"> O produto foi adicionado ao seu carrinho!</label><br>
					</c:if>
				</c:if>
			</div>	
		</header>
		<nav>
		</nav>	
		<section>
		</section>	
		<article>
		</article>	
		<aside>
		</aside>
		<footer>	
		<c:set var="pedido" scope="session" value="${sessionScope.clientePedAberto.pedidos.get(0)}"></c:set>
		<c:if test="${not empty pedido.itens}"> 
			<div class="tabela">
				<br><table width="420px">
					<tr><td align="left"><label>STATUS: </label> ${pedido.status} </td></tr>
				</table>
				<table  border="2">
					<tr>
					<th width="60px"> FOTO       </th> <th width="300px"> NOME    </th> <th width="80px">  PRE�O UNIT.   </th>
				    <th width="80px"> QUANTIDADE </th> <th width="150px"> ALTERAR </th> <th width="150px"> EXCLUIR </th> </tr>
							<label id="lblTab"> Seus produtos </label><br>	
							<c:set var="total" value="${0}"/>
							<c:forEach var="i" begin="0" end="${fn:length(pedido.itens)-1}">
								<tr>
									<td><img alt="${pedido.itens.get(i).produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${pedido.itens.get(i).produto.id}" width="60" height="60"/></td>  
									<td>${pedido.itens.get(i).produto.nome} </td>  						
									<td>${pedido.itens.get(i).produto.preco} </td>
									<c:set var="valor" value="${pedido.itens.get(i).produto.preco * pedido.itens.get(i).quantidade}"/>
									<c:set var="total" value="${valor + total}" />
									<form action="AlterarItem" method="post" name="alterarItem">
										<td width="50px"><input type="text" maxlength="2" value="${pedido.itens.get(i).quantidade}" name="txtQtde" size="3"/></td>
										<input type="hidden" maxlength="4" value="${pedido.itens.get(i).id}" name="txtIndice" width="10"/>
										<input type="hidden" maxlength="4" value="${pedido.itens.get(i).idPedido}" name="txtPedido" width="10"/>
										<input type="hidden" maxlength="4" value="${pedido.itens.get(i).produto.id}" name="txtProduto" width="10"/>
										<td><input type="submit" value="Alterar a quantidade" size="30px" name="OPERACAO"></td>
									</form> 
									<form name="formExcluirItem" action="ExcluirItem" method="post">								
										<input type="hidden" value="${pedido.itens.get(i).id}" name="indice"/>
									<td>	<input type="submit" value="Retirar do carrinho" name="OPERACAO"/><br>	</td>						
									</form>					
								</tr>			
							</c:forEach>
				</table>
				<table width="420px">
					<tr><td align="right"><label>Total: </label> ${total} </td></tr>
				</table>
				<form action="SalvarHistorico" method="post" name="params">
					<input type="hidden" value="Cadastrar" name="OPERACAO"/><br>
					<input type="hidden" value="1" name="tipo"/><br>						
				</form>
				
	<!-- //----------------------------------- FORMULARIO/PAGSEGURO -----------------------------------------------------------------// -->
					<!-- Declara��o do formul�rio   -->
					<form name="formPagSeg" method="post" target="pagseguro" action="https://pagseguro.uol.com.br/v2/checkout/payment.html">  		          
						<!-- Campos obrigat�rios   -->
				        <input name="receiverEmail" type="hidden" value="jonasbcosta@hotmail.com">  
				        <input name="currency" type="hidden" value="BRL">  
					  
						<c:forEach var="i" begin="0" end="${fn:length(pedido.itens)-1}">
						<!-- //---------- DADOS/PRODUTO PAGSEGURO ----------------------------------------// --> 
					  
							<!-- Itens do pagamento (ao menos um item � obrigat�rio)   -->
					        <input name="itemId${i+1}" type="hidden" 		  value="${pedido.itens.get(i).id}">  
					        <input name="itemDescription${i+1}" type="hidden" value="${pedido.itens.get(i).produto.nome}">  
					        <input name="itemAmount${i+1}" type="hidden" 	  value="${pedido.itens.get(i).produto.preco}">  
					        <input name="itemQuantity${i+1}" type="hidden" 	  value="${pedido.itens.get(i).quantidade}">  
					        <input name="itemWeight${i+1}" type="hidden" 	  value="100">   
		
						<!-- //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^// --> 
						</c:forEach>
						
						<!-- //--------- DADOS/CLIENTE PAGSEGURO -----------------------------------------// --> 	
							<!-- C�digo de refer�ncia do pagamento no seu sistema (opcional)   -->
					        <input name="reference" type="hidden" value="REF1234">  
					          
							<!-- Informa��es de frete (opcionais)   -->
					        <input name="shippingType" type="hidden" value="1">  
					        <input name="shippingAddressPostalCode" type="hidden" value="${sessionScope.clientePedAberto.endereco.cep}">  
					        <input name="shippingAddressStreet" type="hidden" 	  value="${sessionScope.clientePedAberto.endereco.rua}">  
					        <input name="shippingAddressNumber" type="hidden" 	  value="${sessionScope.clientePedAberto.endereco.numero}">  
					        <input name="shippingAddressComplement" type="hidden" value="${sessionScope.clientePedAberto.endereco.complemento}">  
					        <input name="shippingAddressDistrict" type="hidden"   value="${sessionScope.clientePedAberto.endereco.bairro}">  
					        <input name="shippingAddressCity" type="hidden" 	  value="${sessionScope.clientePedAberto.endereco.cidade}">  
					        <input name="shippingAddressState" type="hidden" 	  value="${sessionScope.clientePedAberto.endereco.estado}">  
					        <input name="shippingAddressCountry" type="hidden" 	  value="BRA">  
					  
					  		<!-- Dados do comprador (opcionais)   -->
					        <input name="senderName" type="hidden" 	   value="${sessionScope.clientePedAberto.login}">  
					        <input name="senderAreaCode" type="hidden" value="11">  
					        <input name="senderPhone" type="hidden"    value="${sessionScope.clientePedAberto.telefone}">  
					        <input name="senderEmail" type="hidden"    value="${sessionScope.clientePedAberto.email}">  
					           
					</form>
	<!-- //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^// -->
						<!-- submit do form (obrigat�rio)   -->
					    <input alt="Pague com PagSeguro" name="submit" onclick="altStsPed();" type="image"  
							src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar.gif"/>    
							
						<input id="butCan" type="button" value="Cancelar pedido" onclick="excPedido();" />	
						<form name="formExcluirPedido" action="ExcluirPedido" method="post">
							<input type="hidden" name="OPERACAO" value="Exclu�r" />
						</form>
						<form name="formAltStsPed" action="AlteraStatusPed" method="post">
					        <input name="pedidoCli" type="hidden" 		  value="${pedido.itens}">   
							<input type="hidden" name="OPERACAO" value="Alterar" />
						</form>
					</div>
				</c:if>
				<a href="http://localhost:8083/LojaVirtual/HistoricoPedidos">
				<input id="butHis" type="button" value="Ver meu hist�rico"/></a>
		</footer>
			<div class="contato">
				FALE CONOSCO <br><br>
				
				COMPRE PELO TELEFONE: <br> 
				(11) 1234-1234 - (11) 4321-4321<br><br>
				 
				SAC: (11) 2314-2314 - (11) 3412-3412
			</div>
	</div>
</body>
</html>