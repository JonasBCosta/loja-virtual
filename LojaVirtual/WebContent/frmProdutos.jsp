<%@ page import="javax.imageio.ImageIO"%>
<%@ page import="java.awt.Image"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import= "java.util.ArrayList"%>
<%@ page import= "br.com.nomedositeinvertido.dominio.produto.Produto"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="estilo.css"/>

<!-- 		<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">    -->
<!-- 		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> -->
<!-- 		<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style> -->
<!-- 		<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script> -->
<!-- 		<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!-- 		<link rel="stylesheet" type="text/css" href="estilo.css"/> -->

		<title>Exibi��o de Produtos</title>
		<script type="text/javascript">
			function menuCateg(categoria){				
				document.getElementById('hdnCateg').value = categoria;
				document.formMenuCateg.submit();
			}
		</script>
		
	</head>
<body>
	<div class="geral">
		<div class="topo">
			<img src="${pageContext.request.contextPath}/DownloadImagens?img=LogoHeader" />	
				<div class="menu">
				  <ul class="menu-list">
				    <li><a href="http://localhost:8083/LojaVirtual/BemVindo">Home</a></li>
				     <li>
				      <a href="#">Departamentos</a>
				       <ul class="sub-menu">
				        <li><input type="button" class="botaoLink" value="Eletrodom�sticos" onclick="menuCateg('Eletrodom�sticos')"></li>
				        <li><input type="button" class="botaoLink" value="Telefones e Celulares" onclick="menuCateg('Telefones')"></li>
				        <li><input type="button" class="botaoLink" value="Computadores e Notebooks" onclick="menuCateg('Computadores')"></li>
				        <li><input type="button" class="botaoLink" value="M�veis" onclick="menuCateg('M�veis')"></li>
				        <li><input type="button" class="botaoLink" value="Livros" onclick="menuCateg('Livros')"></li>
				        <li><input type="button" class="botaoLink" value="Utens�lios" onclick="menuCateg('Utens�lios')"></li>
				      </ul>
				    </li>
				    
					<c:choose>
						<c:when test="${idCliente > 0}">
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">Ver meu cadastro </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroItem">Ver meu carrinho</a></li>
							<li><a href="http://localhost:8083/LojaVirtual/LogoutCliente">Sair</a><br></li>
						</c:when>
						<c:otherwise>
							<li><a href="http://localhost:8083/LojaVirtual/CadastroCliente">N�o sou cadastrado </a></li>
							<li><a href="http://localhost:8083/LojaVirtual/frmLoginAut.jsp">Entrar na minha conta </a></li>							
						</c:otherwise>
					</c:choose>
				
				    <li><a href="#contato">Fale conosco</a></li>
				  </ul>
				</div>				
				<form name="formMenuCateg" action="FiltrarBusca" method="post">
					<input type="hidden" id="hdnIdCli" name="hdnIdCli" value="${idCliente}">
					<input type="hidden" id="hdnCateg" name="hdnCateg">					
				</form>
		</div><br>
		<c:set var="count" value="0" scope="page" />		
		<c:if test="${sessionScope.produtos != null}">
<!-- 			<div class="table-responsive" > -->
<!--             <table id="minhaTabela" class="display table" align="center" width="90%"> -->
<!--                 <thead>   -->
<!-- 			        <tr>   -->
<!-- 			            <th></th>  -->
<!-- 			            <th></th>  -->
<!-- 			            <th></th>  -->
<!-- 			            <th></th>  		             -->
<!-- 			        </tr>   -->
<!--     		    </thead>   -->
<!-- 				<tbody> -->
					<c:forEach items="${produtos}" var="produtoList">		
<!-- 					<tr> -->
<!-- 					<td> -->
						<div id="linha">
							<c:forEach items="${produtoList}" var="produto"> 										
								<li>	 										
								    <div id="img">
								    <a href="http://localhost:8083/LojaVirtual/frmDescrProd.jsp?indice=${produto.id}&logon=${param.logon}">
									<img alt="${produto.nome}" src="${pageContext.request.contextPath}/DownloadImagens?img=Produto&id=${produto.id}" /></a></div><br>
									<h3><label>${produto.nome}</label></h3>  
									<label>Pre�o: </label> ${produto.preco}										
								</li>
							</c:forEach>	 				
					 	</div>
<!-- 					</td> -->
<!-- 					</tr>	 -->
					</c:forEach>	
<!-- 				</tbody>			 -->
<!-- 			</table> -->
<!-- 			</div> -->
		</c:if>
		<a name="contato"></a>
		<div class="contato">
			<label>FALE CONOSCO</label> <br><br>
			
			<label>COMPRE PELO TELEFONE:</label> <br> 
			<label>(11) 1234-1234 - (11) 4321-4321</label> <br><br>
			 
			<label>SAC: (11) 2314-2314 - (11) 3412-3412</label>
		</div>
	</div>
<!-- 	<script> -->
<!--  		$(document).ready(function(){ -->
<!--  		    $('#minhaTabela').dataTable(); -->
<!--  		}); -->
<!-- 	</script> -->
</body>
</html>