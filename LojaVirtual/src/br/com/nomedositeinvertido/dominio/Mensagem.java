package br.com.nomedositeinvertido.dominio;

public class Mensagem {
	
private String descricao;
	
	public Mensagem(String descricao){
		this.descricao = descricao;
	}
	
	public String toString() {
		return descricao;
	}
}
