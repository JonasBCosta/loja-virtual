package br.com.nomedositeinvertido.dominio;

import java.sql.Timestamp;

public class EntidadeDominio {

	private int id;
    private java.sql.Timestamp data = new Timestamp(System.currentTimeMillis());
	    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public java.sql.Timestamp getData() {
		return data;
	}

	public void setData(java.sql.Timestamp timeStamp) {
		this.data = timeStamp;
	}
}
