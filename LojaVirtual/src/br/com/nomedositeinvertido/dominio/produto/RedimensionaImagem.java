package br.com.nomedositeinvertido.dominio.produto;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class RedimensionaImagem {
	int largura = 0;
	int altura = 0;
	int contador = 0;
	
	public BufferedImage gravaImagemNaPasta(int largurafnl, int alturafnl, BufferedImage inputImage, String nomeSrc){		

		if(largura == 0 && altura == 0){ 
	 		largura = inputImage.getWidth();
	 		altura = inputImage.getHeight();
	 	}
		if(largura < largurafnl && altura < alturafnl){ 
			largura += largura * (0.01);
			altura += altura * (0.01); 
		} //
		else{
			if(largura > largurafnl || altura > alturafnl){ 
				largura -= largura * (0.01); 
				altura -= altura * (0.01);
			}
			else{
				BufferedImage imagem = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);				
				Graphics2D g2d = imagem.createGraphics();
				g2d.drawImage(inputImage, 0, 0, largura, altura, null);
				try {
					ImageIO.write(imagem, "PNG", new File(nomeSrc));
				} catch (IOException e) {System.out.println("Erro ao gravar a imagem na pasta webcontent");	}
				return null;
			}
		}
		contador++;
		if(contador > 500){
			return null;
		}
		return gravaImagemNaPasta(largurafnl, alturafnl, inputImage, nomeSrc);
 	}
 
	public ImageIcon resizeProporcional(int largura, int altura, ImageIcon inputImage){		

	 	if(largura == 0 && altura == 0){		// Se chegar 0/0 vai pegar as dimensoes originais da imagem passada
	 		largura = inputImage.getIconWidth();
	 		altura = inputImage.getIconHeight();
	 	}
		if(largura < 221 && altura < 176){ 		// Se maior dimensiona pra menos 
			largura += largura * (0.01);
			altura += altura * (0.01); 
		} 
		else{
			if(largura > 221 || altura > 176){  // Diminui a imagem proporcionalmente dentro da janela com dimensoes de 221/176 
				largura -= largura * (0.01);
				altura -= altura * (0.01);
			}
			else{
				inputImage.setImage(inputImage.getImage().getScaledInstance(largura, altura, BufferedImage.SCALE_SMOOTH));
				return inputImage;
			}
		}
		return resizeProporcional(largura, altura, inputImage);
 	}
 
 	public ImageIcon resize(int largura, int altura, ImageIcon inputImage){		
		inputImage.setImage(inputImage.getImage().getScaledInstance(largura, altura, 100));
		return inputImage;
 	}
 	
 	public Image getImagem(Produto srcWebCont){
 		BufferedImage imagem;
		try {
			imagem = ImageIO.read(new File("C:/Users/Jonas/git/loja-virtual/LojaVirtual/"));
			return imagem;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
 	}
 	
 	
}