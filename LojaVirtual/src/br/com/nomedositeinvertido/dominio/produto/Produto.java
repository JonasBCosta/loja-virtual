package br.com.nomedositeinvertido.dominio.produto;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;

public class Produto extends EntidadeDominio{

	private String nome;
	private String enderecoImagem;
	private String srcWebCont;
	private Image imagem;
	private String descricao;
	private BigDecimal preco;
	private int quantidade = 0;
	private String categoria;
	private int vendidos;
	private int estqMinimo = -1;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEnderecoImagem() {
		return enderecoImagem;
	}
	public void setEnderecoImagem(String enderecoImagem) {
		this.enderecoImagem = enderecoImagem;
	}
	public Image getImagem() {
		try {			
			imagem = ImageIO.read(new File("C:/Users/HP/Desktop/Desenvolvimento/WorkspaceEclipse/LojaVirtual/" + srcWebCont));
		} catch (IOException e) {
			System.out.println("ERRO DE LEITURA DA IMAGEM NA CLASSE PRODUTO");
		}
		return imagem;
	}
	public void setImagem(Image imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getPreco() {
		return preco;
	}
	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}
	public String getSrcWebCont() {
		return srcWebCont;
	}
	public void setSrcWebCont(String srcWebCont) {
		this.srcWebCont = srcWebCont;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public int getVendidos() {
		return vendidos;
	}
	public void setVendidos(int vendidos) {
		this.vendidos = vendidos;
	}
	public int getEstqMinimo() {
		return estqMinimo;
	}
	public void setEstqMinimo(int estqMinimo) {
		this.estqMinimo = estqMinimo;
	}

}
