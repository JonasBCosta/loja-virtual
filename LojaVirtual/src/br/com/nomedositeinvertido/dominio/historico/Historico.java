package br.com.nomedositeinvertido.dominio.historico;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;

//public class Historico extends EntidadeDominio{
//
//	private Cliente cliente = new Cliente();
//	private List<Pedido> pedidos = new ArrayList<Pedido>();
//	private List<Item> itens = new ArrayList<Item>();
//	private Produto produto = new Produto();
//	
//	public Historico (Cliente cliente, List<Pedido> pedidos, List<Item> itens, Produto produto){
//		this.cliente = cliente;
//		this.pedidos = pedidos;
//		this.itens = itens;
//		this.produto = produto;
//	}
//		
//	public Cliente getCliente() {
//		return cliente;
//	}
//	public void setCliente(Cliente cliente) {
//		this.cliente = cliente;
//	}
//	public List<Pedido> getPedidos() {
//		return pedidos;
//	}
//	public void setPedidos(List<Pedido> pedidos) {
//		this.pedidos = pedidos;
//	}
//	public List<Item> getItens() {
//		return itens;
//	}
//	public void setItens(List<Item> itens) {
//		this.itens = itens;
//	}
//	public Produto getProduto() {
//		return produto;
//	}
//	public void setProduto(Produto produto) {
//		this.produto = produto;
//	}
//	
//	
//}
