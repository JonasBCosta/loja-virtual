package br.com.nomedositeinvertido.dominio.cliente;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.ValidacaoCPF;

public class Pessoa extends EntidadeDominio{

	private String nome;
	private String cpf;
    private Endereco endereco = new Endereco();	
    private String dataNascimento;

	public String getCPF() {
		return cpf;
	}

	public void setCPF(String cpf) { 
		this.cpf = cpf;
//		ICommand validcpf = new ValidacaoCPF();
//		String msg = validcpf.execute(this);
//		System.out.println(msg);
//		if(msg == null) 
//			return true;
//		else
//			return false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}		
}
