package br.com.nomedositeinvertido.dominio.cliente;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.pedido.Pedido;

public class Cliente extends Pessoa{

	private String login;
	private String senha;
	private String statusValida��o; 
	private String telefone;
	private String celular;
	private String email;
	private Pedido pedEmUso;
	private int id;
	private List<Pedido> pedidos;
	
	public Cliente(){
		this.pedEmUso = new Pedido();
		this.pedidos = new ArrayList<Pedido>();
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatusValida��o() {
		return statusValida��o;
	}
	public void setStatusValida��o(String statusValida��o) {
		this.statusValida��o = statusValida��o;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Pedido> getPedidos() {
		return pedidos;
	}
	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	public Pedido getPedEmUso() {
		return pedEmUso;
	}
	public void setPedEmUso(Pedido pedEmUso) {
		this.pedEmUso = pedEmUso;
	}
}
