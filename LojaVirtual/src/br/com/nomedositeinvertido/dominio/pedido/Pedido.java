package br.com.nomedositeinvertido.dominio.pedido;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;

public class Pedido extends EntidadeDominio{

	private int idCliente;
    private java.sql.Timestamp expiracao = new Timestamp(System.currentTimeMillis());
    private String status;
    private List<Item> itens;

    public Pedido(){
    	this.itens = new ArrayList<Item>();
    }
    
	public java.sql.Timestamp getExpiracao() {
		if(status == null || status.equals("Em espera")){
			expiracao.setYear(expiracao.getYear()+100); // Est� com +100 para evitar problemas de testes
			return expiracao;
		}else{
			return expiracao;
		}
	}

	public void setExpiracao(java.sql.Timestamp expiracao) {
		this.expiracao = expiracao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}	
}
