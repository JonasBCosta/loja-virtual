package br.com.nomedositeinvertido.dominio.loja;

import java.awt.Image;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;

public class Categoria extends EntidadeDominio{

	private String categoria;

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}
