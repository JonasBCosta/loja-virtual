package br.com.nomedositeinvertido.dominio.item;

import java.math.BigDecimal;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;

public class Lote extends EntidadeDominio{
	
	private Fornecedor fornecedor;
	private BigDecimal valorCompra;
	private String codigo;
	private int idfornecedor;
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public BigDecimal getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(BigDecimal bigDecimal) {
		this.valorCompra = bigDecimal;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getIdfornecedor() {
		return idfornecedor;
	}
	public void setIdfornecedor(int idfornecedor) {
		this.idfornecedor = idfornecedor;
	}
}
