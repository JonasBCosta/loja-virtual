package br.com.nomedositeinvertido.dominio.fornecedor;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;

public class Fornecedor extends EntidadeDominio{

	private String nomeFornec;

	public String getNomeFornec() {
		return nomeFornec;
	}

	public void setNomeFornec(String nomeFornec) {
		this.nomeFornec = nomeFornec;
	}
}
