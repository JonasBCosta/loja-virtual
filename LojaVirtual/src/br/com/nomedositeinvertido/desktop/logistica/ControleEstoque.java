package br.com.nomedositeinvertido.desktop.logistica;

import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;

public class ControleEstoque extends JInternalFrame{
	
	private static ControleEstoque instance = new ControleEstoque();
	private JPanel contentPane;
	static public JDesktopPane desktopPane;
	static public CadastroFornecedor interFrameCadFornec;
	static public CadastroLote interFrameCadLote;
	static public CadastroItemEstoque interFrameCadItemEstq;
	static public TabelaFornecedores interFrameTabFornec;
	static public TabelaItemEstoque interFrameTabItemEstq;
	static public TabelaLotes interFrameTabLote;
	public static ControleEstoque getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void exibir(){
		setBounds(5, 40, 1200, 645);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(273, 200);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel painelAbsoluto = new JPanel();
		painelAbsoluto.setBounds(0, 0, 1209, 701);
		contentPane.add(painelAbsoluto);
		painelAbsoluto.setLayout(null);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.BLACK);
		desktopPane.setBounds(0, 0, 1213, 701);
		desktopPane.setLayout(null);
		painelAbsoluto.add(desktopPane);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Controle de estoque");
	
		mostrarCadFornec();
		mostrarCadLote();
		mostrarCadItemEstq();
		mostrarTabFornec();
		mostrarTabLotes();
		mostrarTabItemEstq();
		
		
		setVisible(true);
	}// FIM DO METODO CONSTROI TELA
//////////////// Metodos p�blicos ------------------------------------------------------------------	
	
	public static void mostrarCadFornec(){
		if(interFrameCadFornec != null)
			desktopPane.remove(interFrameCadFornec);
		interFrameCadFornec = CadastroFornecedor.getInstance();
		desktopPane.add(interFrameCadFornec);
		interFrameCadFornec.exibir("CADASTRAR");
		interFrameCadFornec.setVisible(true);

	}
	public static void esconderCadFornec(){
		if(interFrameCadFornec != null)
			interFrameCadFornec.setVisible(false);
	}
	
	public static void mostrarEditFornec(){
		if(TabelaFornecedores.rowAux  < 0)
			return;
		if(interFrameCadFornec != null)
			desktopPane.remove(interFrameCadFornec);
		interFrameCadFornec = CadastroFornecedor.getInstance();
		interFrameCadFornec.exibir("EDITAR");
		desktopPane.add(interFrameCadFornec);			
	}
	private void esconderEditFornec(){
		if(interFrameCadFornec != null)
			interFrameCadFornec.setVisible(false);
	}

	public static void mostrarTabFornec(){
		if(interFrameTabFornec != null){
			desktopPane.remove(interFrameTabFornec);
		}
		interFrameTabFornec = TabelaFornecedores.getInstance();
		interFrameTabFornec.exibir();
		desktopPane.add(interFrameTabFornec);
		interFrameTabFornec.setVisible(true);
	}
	private void esconderTabFornec(){
		if(interFrameTabFornec != null){
			interFrameTabFornec.setVisible(false);
			TabelaFornecedores.rowAux = -1;
		}
	}
	
	public static void mostrarCadLote(){
		if(interFrameCadLote != null)
			desktopPane.remove(interFrameCadLote);
		interFrameCadLote = CadastroLote.getInstance();
		desktopPane.add(interFrameCadLote);
		interFrameCadLote.exibir("CADASTRAR");
		interFrameCadLote.setVisible(true);
	}
	public static void esconderCadLote(){
		if(interFrameCadLote != null)
			interFrameCadLote.setVisible(false);
	}

	public static void mostrarEditLote(){
		if(TabelaLotes.rowAux  < 0)
			return;
		if(interFrameCadLote != null)
			desktopPane.remove(interFrameCadLote);
		interFrameCadLote = CadastroLote.getInstance();
		interFrameCadLote.exibir("EDITAR");
		desktopPane.add(interFrameCadLote);			
	}
	private void esconderEditlote(){
		if(interFrameCadLote != null)
			interFrameCadLote.setVisible(false);
	}
	
	public static void mostrarTabLotes(){
		if(interFrameTabLote != null)
			desktopPane.remove(interFrameTabLote);
		interFrameTabLote = TabelaLotes.getInstance();
		interFrameTabLote.exibir();
		desktopPane.add(interFrameTabLote);
		interFrameTabLote.setVisible(true);
	}
	public void esconderTabLotes(){
		if(interFrameTabLote != null){
			interFrameTabLote.setVisible(false);
			TabelaLotes.rowAux = -1;
		}
	}
	
	public static void mostrarCadItemEstq(){
		if(interFrameCadItemEstq != null)
			desktopPane.remove(interFrameCadItemEstq);
		interFrameCadItemEstq = CadastroItemEstoque.getInstance();
		desktopPane.add(interFrameCadItemEstq);
		interFrameCadItemEstq.exibir("CADASTRAR");
		interFrameCadItemEstq.setVisible(true);

	}
	public static void esconderCadItemEstq(){
		if(interFrameCadItemEstq != null)
			interFrameCadItemEstq.setVisible(false);
	}
	
	public static void mostrarEditItemEstq(){
		if(TabelaItemEstoque.rowAux  < 0)
			return;
		if(interFrameCadItemEstq != null)
			desktopPane.remove(interFrameCadItemEstq);
		interFrameCadItemEstq = CadastroItemEstoque.getInstance();
		interFrameCadItemEstq.exibir("EDITAR");
		desktopPane.add(interFrameCadItemEstq);			
	}
	private void esconderEditItemEstq(){
		if(interFrameCadItemEstq != null)
			interFrameCadItemEstq.setVisible(false);
	}
	
	public static void mostrarTabItemEstq(){
		if(interFrameTabItemEstq != null)
			desktopPane.remove(interFrameTabItemEstq);
		interFrameTabItemEstq = TabelaItemEstoque.getInstance();
		interFrameTabItemEstq.exibir();
		desktopPane.add(interFrameTabItemEstq);
		interFrameTabItemEstq.setVisible(true);
	}
	private void esconderTabItemEstq(){
		if(interFrameTabItemEstq != null){
			interFrameTabItemEstq.setVisible(false);
			TabelaFornecedores.rowAux = -1;
		}
	}
	
	public void encerraTela(){
		dispose();		
	}
}

