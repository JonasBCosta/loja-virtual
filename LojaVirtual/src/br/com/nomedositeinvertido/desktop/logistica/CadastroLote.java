package br.com.nomedositeinvertido.desktop.logistica;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.item.Lote;
import br.com.nomedositeinvertido.jdbc.impl.FornecedorDAO;
import br.com.nomedositeinvertido.jdbc.impl.LoteDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class CadastroLote extends JInternalFrame implements Observer{

	private static CadastroLote instance = new CadastroLote();
///////// Atributos da GUI .................
	private JTextField txtFCodigo;
	private JComboBox comboBoxFnc;
	private JTextField txtFValor;
	private JButton btoCadastro;
	private JButton btoAlteracao;
	private JButton btoexclusao;
	
//////// Objeto EntidadeDominio ...............
	private Lote lote;
	private int idLote;
	private FornecedorDAO fornDAO = new FornecedorDAO();
	private HashMap<String, Fornecedor> fornMap = new HashMap<String, Fornecedor>();
	private Fachada fachada = new Fachada();

////////// METODOS PUBLICOS  .........................	
	
	public static CadastroLote getInstance() {
	    return instance;
	}
	public void executaAcao(String acao){
		sincronizarModelComView();
		/// CASO SEJA ALTERA��O, APAGAR A IMAGEM NA PASTA E SALVAR A NOVA IMAGEM ------------
		if(acao.equals("CADASTRAR")){
			fachada.cadastrar(lote, null);
		}else if(acao.equals("ALTERAR")){
			fachada.alterar(lote, null);
		}else if(acao.equals("EXCLUIR")){
			fachada.excluir(lote, null);
			dispose();
		}
	}
    public void exibir(String sOperacao) {
		setBounds(5, 145, 250, 210); //OK
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(5, 145);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		lote = new Lote();
		limpaComponentes();

		if(sOperacao == "CADASTRAR"){
			add(btoCadastro = criaBotao("Cadastrar", "CADASTRAR", "Cadastro de lote", 10, 140, 159, 23));
		}else if(sOperacao == "EDITAR"){
			add(btoAlteracao = criaBotao("Alterar", "ALTERAR", "Edi��o de lote", 10, 130, 159, 23));
			add(btoexclusao = criaBotao("Exclu�r", "EXCLUIR", "Edi��o de lote", 10, 153, 159, 23));
		}

		add(criaLabel("C�digo", 10, 10, 125, 18));
		add(criaLabel("Fornecedor", 10, 50, 125, 18));
		add(criaLabel("Valor do lote", 10, 90, 125, 18));

		add(txtFCodigo = criaTextField(10, 30, 220, 20));
		add(comboBoxFnc = criaCombobox(itensCBoxForn(), 10, 70, 220, 20));
		add(txtFValor = criaTextField(10, 110, 220, 20));

		if(sOperacao == "EDITAR"){
			lote.setId(idLote);
			sincronizarViewComModel((Lote) new LoteDAO().consultar(lote));
		}		
		setVisible(true);		
	}
	public void encerraTela(){
		dispose();
	}
	@Override
	public void update(Observable o, Object arg) {		
		if(arg != null){
			idLote = Integer.parseInt(String.valueOf(arg));
		}else{
			ControleEstoque.mostrarCadLote();
		}
	}
////////// METODOS PRIVADOS ..........................  
    private String[] itensCBoxForn(){
    	List<EntidadeDominio> entDoms = new ArrayList<EntidadeDominio>();
    	entDoms = fornDAO.consultarTodos(null);
    	String fList[] = new String[entDoms.size()]; 
    	for (int i = 0; i < entDoms.size(); i++) {
			fList[i] = (String)((Fornecedor) entDoms.get(i)).getNomeFornec();
			fornMap.put(fList[i], ((Fornecedor) entDoms.get(i)));
		}
		String[] itensComboBox = new String[fList.length+1];
		itensComboBox[0] = "Selecione o fornecedor...";
		for(int i = 0; i < fList.length; i++) 
			itensComboBox[i+1] = fList[i];
		return itensComboBox;
    }
	private ItemListener eventoItemForn(String[] itenscombobox) {
    	ItemListener itemListener = null;
    	itemListener = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lote.setFornecedor(fornMap.get(e.getItem()));
			}
		};
		return itemListener;
    }
    private JComboBox criaCombobox(String[] itenscombobox, int esquerda, int topo, int largura, int altura){
    	JComboBox combobox = new JComboBox(itenscombobox);
    	combobox.setBounds(esquerda, topo, largura, altura); //OK
    	combobox.addItemListener(eventoItemForn(itenscombobox));
		return combobox;
    } 	
    private void sincronizarViewComModel(Lote model){
    	if(model.getCodigo() != null){
    		txtFCodigo.setText(model.getCodigo());
    	}if(model.getFornecedor() != null){
    		comboBoxFnc.setSelectedItem(model.getFornecedor().getNomeFornec());
    	}if(model.getValorCompra() != null){
    		txtFValor.setText(String.valueOf(model.getValorCompra()));
    	}
    }
    private void sincronizarModelComView(){
		if(txtFCodigo.getText() != null)
			lote.setCodigo(txtFCodigo.getText());
		if(comboBoxFnc != null && !comboBoxFnc.getSelectedItem().equals("Selecione o fornecedor...")){
			lote.setFornecedor(fornMap.get(comboBoxFnc.getSelectedItem()));
		}
		if(txtFValor.getText() != null)
			lote.setValorCompra(BigDecimal.valueOf(Double.parseDouble(txtFValor.getText())));
    }    
    private JButton criaBotao(String texto, String chave, String tituloTela, int esquerda, int topo, int largura, int altura){
    	JButton botao = new JButton(texto);
    	botao.setBounds(esquerda, topo, largura, altura);
		OuvinteCadlote ouvinteCadFornec = new OuvinteCadlote(chave); 
		botao.addActionListener(ouvinteCadFornec);
		setTitle(tituloTela);
		return botao;
    }
    private JTextField criaTextField (int esquerda, int topo, int largura, int altura){
		JTextField textField = new JTextField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }	    
    private void limpaComponentes(){
    	if(txtFCodigo != null){
    		remove(txtFCodigo);
    	}if(comboBoxFnc != null){
    		remove(comboBoxFnc);
    	}if(txtFValor != null){
    		remove(txtFValor);
    	}if(btoCadastro != null){
    		remove(btoCadastro);
    	}if(btoAlteracao != null){
    		remove(btoAlteracao);
    	}if(btoexclusao != null){
    		remove(btoexclusao);
    	}if(fornMap.size() > 0){
    		fornMap.clear();
    	}
    }
    private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
    }    
	private class OuvinteCadlote extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteCadlote(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(TabelaLotes.getInstance());
			addObserver(CadastroItemEstoque.getInstance());
			addObserver(CadastroFornecedor.getInstance());
			addObserver(TabelaItemEstoque.getInstance());
			addObserver(TabelaFornecedores.getInstance());
		}
		public void actionPerformed(ActionEvent e) {
		//////////////////// Fluxo do MenuAdministrador ------------------------------------------------		
			if(sOpcao.equals("CADASTRAR")){   
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadLote();   
			}
			else if(sOpcao.equals("ALTERAR")){ 
				executaAcao(sOpcao);
			}
			else if(sOpcao.equals("EXCLUIR")){
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadLote();   
			}
			setChanged();
			notifyObservers();
		}
	}
}

