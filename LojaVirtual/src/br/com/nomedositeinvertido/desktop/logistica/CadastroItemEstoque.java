package br.com.nomedositeinvertido.desktop.logistica;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.dominio.item.Lote;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemEstoqueDAO;
import br.com.nomedositeinvertido.jdbc.impl.LoteDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class CadastroItemEstoque extends JInternalFrame implements Observer{

	private static CadastroItemEstoque instance = new CadastroItemEstoque();
///////// Atributos da GUI .................
	private JTextField txtFLocalizacao;
	private JComboBox comboBoxLote;
	private JComboBox comboBoxProd;
	private JTextField txtFQtdd;
	private JButton btoCadastro;
	private JButton btoAlteracao;
	private JButton btoexclusao;
	
//////// Objeto EntidadeDominio ...............
	private ItemEstoque itemEstq;
	private int idItemEstoque;
	private LoteDAO loteDAO = new LoteDAO();
	private ProdutoDAO prodDAO = new ProdutoDAO();

	private HashMap<String, Integer> idProdMap = new HashMap<String, Integer>();
	private HashMap<String, Lote> loteMap = new HashMap<String, Lote>();
	private Fachada fachada = new Fachada();

////////// METODOS PUBLICOS  .........................	
	
	public static CadastroItemEstoque getInstance() {
	    return instance;
	}
	public void executaAcao(String acao){
		sincronizarModelComView();
		/// CASO SEJA ALTERA��O, APAGAR A IMAGEM NA PASTA E SALVAR A NOVA IMAGEM ------------
		if(acao.equals("CADASTRAR")){
			fachada.cadastrar(itemEstq, null);
		}else if(acao.equals("ALTERAR")){
			fachada.alterar(itemEstq, null);
		}else if(acao.equals("EXCLUIR")){
			fachada.excluir(itemEstq, null);
			dispose();
		}
	}
    public void exibir(String sOperacao) {
		setBounds(5, 355, 250, 250); //OK
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(5, 355);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		itemEstq = new ItemEstoque();
		limpaComponentes();

		if(sOperacao == "CADASTRAR"){
			add(btoCadastro = criaBotao("Cadastrar", "CADASTRAR", "Cadastro de itens", 10, 180, 159, 23));
		}else if(sOperacao == "EDITAR"){
			add(btoAlteracao = criaBotao("Alterar", "ALTERAR", "Edi��o de item", 10, 170, 159, 23));
			add(btoexclusao = criaBotao("Exclu�r", "EXCLUIR", "Edi��o de item", 10, 193, 159, 23));
		}

		add(criaLabel("Localiza��o", 10, 10, 125, 18));
		add(criaLabel("Lote", 10, 50, 125, 18));
		add(criaLabel("Produto", 10, 90, 125, 18));
		add(criaLabel("Quantidade", 10, 130, 125, 18));

		add(txtFLocalizacao = criaTextField(10, 30, 220, 20));
		add(comboBoxLote = criaCombobox(itensCBoxLote(), 10, 70, 220, 20));
		add(comboBoxProd = criaCombobox(itensCBoxProd(), 10, 110, 220, 20));
		add(txtFQtdd = criaTextField(10, 150, 50, 20));

		if(sOperacao == "EDITAR"){
			itemEstq.setId(idItemEstoque);
			sincronizarViewComModel((ItemEstoque) new ItemEstoqueDAO().consultar(itemEstq));
		}		
		setVisible(true);		
	}
	public void encerraTela(){
		dispose();
	}
	@Override
	public void update(Observable o, Object arg) {
		if(arg != null){
			idItemEstoque = Integer.parseInt(String.valueOf(arg));
		}else{		
			ControleEstoque.mostrarCadItemEstq();
		}
	}
////////// METODOS PRIVADOS ..........................  
    private String[] itensCBoxLote(){
    	List<EntidadeDominio> entDoms = new ArrayList<EntidadeDominio>();
    	entDoms = loteDAO.consultarTodos(null);
    	String fList[] = new String[entDoms.size()]; 
    	for (int i = 0; i < entDoms.size(); i++) {
			fList[i] = ((Lote) entDoms.get(i)).getCodigo();
			loteMap.put(fList[i], (Lote)entDoms.get(i));
		}
		String[] itensComboBox = new String[fList.length+1];
		itensComboBox[0] = "Selecione o lote...";
		for(int i = 0; i < fList.length; i++) 
			itensComboBox[i+1] = fList[i];
		return itensComboBox;
    }
    private String[] itensCBoxProd(){
    	List<EntidadeDominio> entDoms = new ArrayList<EntidadeDominio>();
    	entDoms = prodDAO.consultarTodos(null);
    	String fList[] = new String[entDoms.size()]; 
    	for (int i = 0; i < entDoms.size(); i++) {
			fList[i] = (String)((Produto) entDoms.get(i)).getNome();
			idProdMap.put(fList[i], entDoms.get(i).getId());
		}
		String[] itensComboBox = new String[fList.length+1];
		itensComboBox[0] = "Selecione o produto...";
		for(int i = 0; i < fList.length; i++){
			itensComboBox[i+1] = fList[i];
		}
		return itensComboBox;
    }
	private ItemListener eventoItemComboBox(String[] itenscombobox) {
    	ItemListener itemListener = null;
    	if(itenscombobox.length > 0 && itenscombobox[0].equals("Selecione o lote...")){
	    	itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(!e.getItem().equals("Selecione o lote...")){
						itemEstq.setLote(loteMap.get(e.getItem()));
						itemEstq.setIdLote(itemEstq.getLote().getId());
						itemEstq.setCodigo(itemEstq.getLote().getCodigo());
					}
				}
			};
    	}else{
    		itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(!e.getItem().equals("Selecione o produto...")){
						itemEstq.setIdProduto(idProdMap.get(e.getItem()));
					}
				}
			};
    	}
		return itemListener;
    }
    private JComboBox criaCombobox(String[] itenscombobox, int esquerda, int topo, int largura, int altura){
    	JComboBox combobox = new JComboBox(itenscombobox);
    	combobox.setBounds(esquerda, topo, largura, altura); //OK
    	combobox.addItemListener(eventoItemComboBox(itenscombobox));
		return combobox;
    } 	
    
    private void sincronizarViewComModel(ItemEstoque model){
    	if(model.getCodigo() != null){
    		txtFLocalizacao.setText(model.getLocalizacao());
    	}if(model.getLote() != null){
    		comboBoxLote.setSelectedItem(model.getLote().getCodigo());
    	}if(model.getIdProduto() > 0 ){
    		Produto produto = new Produto();
    		produto.setId(model.getIdProduto());
    		produto = (Produto) prodDAO.consultar(produto);
    		comboBoxProd.setSelectedItem(produto.getNome());
    	}if(model.getQuantidade() > 0){
    		txtFQtdd.setText(String.valueOf(model.getQuantidade()));
    	}
    }
    private void sincronizarModelComView(){
    	String string = new String();    	
    	for (Lote lote : loteMap.values()) {
    		string += "\n" + lote.getCodigo();
    	}
    	string = "";
		for(Integer produto: idProdMap.values()){
    		string += "\n" + produto;
    	}
		if(txtFLocalizacao.getText() != null)
			itemEstq.setLocalizacao(txtFLocalizacao.getText());
		if(comboBoxLote != null && !comboBoxLote.getSelectedItem().equals("Selecione o lote...")){
			itemEstq.setLote(loteMap.get(comboBoxLote.getSelectedItem()));
			itemEstq.setIdLote(itemEstq.getLote().getId());
			itemEstq.setCodigo(itemEstq.getLote().getCodigo());
		}
		if(comboBoxProd != null && !comboBoxProd.getSelectedItem().equals("Selecione o produto...")){
			itemEstq.setIdProduto(idProdMap.get(comboBoxProd.getSelectedItem()));
		}
		if(txtFQtdd.getText() != null)
			itemEstq.setQuantidade(Integer.parseInt(txtFQtdd.getText()));
    }    
    private JButton criaBotao(String texto, String chave, String tituloTela, int esquerda, int topo, int largura, int altura){
    	JButton botao = new JButton(texto);
    	botao.setBounds(esquerda, topo, largura, altura);
		OuvinteCadItemEstq ouvinteCadItemEstq = new OuvinteCadItemEstq(chave); 
		botao.addActionListener(ouvinteCadItemEstq);
		setTitle(tituloTela);
		return botao;
    }
    private JTextField criaTextField (int esquerda, int topo, int largura, int altura){
		JTextField textField = new JTextField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }	    
    private void limpaComponentes(){
    	if(txtFLocalizacao != null){
    		remove(txtFLocalizacao);
    	}if(comboBoxLote != null){
    		remove(comboBoxLote);
    	}if(comboBoxProd != null){
    		remove(comboBoxProd);
    	}if(txtFQtdd != null){
    		remove(txtFQtdd);
    	}if(btoCadastro != null){
    		remove(btoCadastro);
    	}if(btoAlteracao != null){
    		remove(btoAlteracao);
    	}if(btoexclusao != null){
    		remove(btoexclusao);
    	}if(idProdMap.size() > 0){
    		idProdMap.clear();
    	}if(loteMap.size() > 0){
    		loteMap.clear();
    	}
    }
    private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
    }    
	private class OuvinteCadItemEstq extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteCadItemEstq(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(TabelaItemEstoque.getInstance());
			addObserver(TabelaLotes.getInstance());
			addObserver(TabelaFornecedores.getInstance());
			addObserver(CadastroFornecedor.getInstance());
			addObserver(CadastroLote.getInstance());
		}
		public void actionPerformed(ActionEvent e) {
		//////////////////// Fluxo do MenuAdministrador ------------------------------------------------		
			if(sOpcao.equals("CADASTRAR")){   
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadItemEstq();   
			}
			else if(sOpcao.equals("ALTERAR")){ 
				executaAcao(sOpcao);
			}
			else if(sOpcao.equals("EXCLUIR")){
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadItemEstq();   
			}
			setChanged();
			notifyObservers();
		}
	}
}

