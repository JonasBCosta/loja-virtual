package br.com.nomedositeinvertido.desktop.logistica;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.FornecedorDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class CadastroFornecedor extends JInternalFrame implements Observer{

	private static CadastroFornecedor instance = new CadastroFornecedor();
///////// Atributos da GUI .................
	private JTextField txtFNome;
	private JButton btoCadastro;
	private JButton btoAlteracao;
	private JButton btoexclusao;
	
//////// Objeto EntidadeDominio ...............
	private Fornecedor fornecedor;
	private int idFornecedor;
	private Fachada fachada = new Fachada();

////////// METODOS PUBLICOS  .........................	
	
	public static CadastroFornecedor getInstance() {
	    return instance;
	}
	public void executaAcao(String acao){
		sincronizarModelComView();
		/// CASO SEJA ALTERA��O, APAGAR A IMAGEM NA PASTA E SALVAR A NOVA IMAGEM ------------
		if(acao.equals("CADASTRAR")){
			fachada.cadastrar(fornecedor, null);
		}else if(acao.equals("ALTERAR")){
			fachada.alterar(fornecedor, null);
		}else if(acao.equals("EXCLUIR")){
			fachada.excluir(fornecedor, null);
			dispose();
		}
	}
    public void exibir(String sOperacao) {
		setBounds(5, 5, 250, 140); //OK
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(5, 5);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		fornecedor = new Fornecedor();
		limpaComponentes();

		if(sOperacao == "CADASTRAR"){
			add(btoCadastro = criaBotao("Cadastrar", "CADASTRAR", "Cadastro de Fornecedor", 10, 60, 159, 23));
		}else if(sOperacao == "EDITAR"){
			add(btoAlteracao = criaBotao("Alterar", "ALTERAR", "Edi��o de Fornecedor", 10, 50, 159, 23));
			add(btoexclusao = criaBotao("Exclu�r", "EXCLUIR", "Edi��o de Fornecedor", 10, 73, 159, 23));
		}

		add(criaLabel("Nome do fornecedor", 10, 10, 190, 18));
		add(txtFNome = criaTextField(10, 30, 220, 20));

		if(sOperacao == "EDITAR"){
			fornecedor.setId(idFornecedor);
			sincronizarViewComModel((Fornecedor) new FornecedorDAO().consultar(fornecedor));
		}		
		setVisible(true);		
	}
	public void encerraTela(){
		dispose();
	}
	@Override
	public void update(Observable o, Object arg) {		
		if(arg != null){
			idFornecedor = Integer.parseInt(String.valueOf(arg));
		}else{
			ControleEstoque.mostrarCadFornec();
		}
	}
////////// METODOS PRIVADOS ..........................
  
    private void sincronizarViewComModel(Fornecedor model){
    	if(model.getNomeFornec() != null){
    		txtFNome.setText(model.getNomeFornec());
    	}
    }
    private void sincronizarModelComView(){
		if(txtFNome.getText() != null)
			fornecedor.setNomeFornec(txtFNome.getText());
    }    
    private JButton criaBotao(String texto, String chave, String tituloTela, int esquerda, int topo, int largura, int altura){
    	JButton botao = new JButton(texto);
    	botao.setBounds(esquerda, topo, largura, altura);
		OuvinteCadFornecedor ouvinteCadFornec = new OuvinteCadFornecedor(chave); 
		botao.addActionListener(ouvinteCadFornec);
		setTitle(tituloTela);
		return botao;
    }
    private JTextField criaTextField (int esquerda, int topo, int largura, int altura){
		JTextField textField = new JTextField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }	    
    private void limpaComponentes(){
    	if(txtFNome != null){
    		remove(txtFNome);
    	}if(btoCadastro != null){
    		remove(btoCadastro);
    	}if(btoAlteracao != null){
    		remove(btoAlteracao);
    	}if(btoexclusao != null){
    		remove(btoexclusao);
    	}
    }
    private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
    }    
	private class OuvinteCadFornecedor extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteCadFornecedor(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(TabelaFornecedores.getInstance());
			addObserver(TabelaLotes.getInstance());
			addObserver(TabelaItemEstoque.getInstance());
			addObserver(CadastroLote.getInstance());
			addObserver(CadastroItemEstoque.getInstance());
		}
		public void actionPerformed(ActionEvent e) {
		//////////////////// Fluxo do MenuAdministrador ------------------------------------------------		
			if(sOpcao.equals("CADASTRAR")){   
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadFornec();   
			}
			else if(sOpcao.equals("ALTERAR")){ 
				executaAcao(sOpcao);
			}
			else if(sOpcao.equals("EXCLUIR")){
				executaAcao(sOpcao);
				ControleEstoque.mostrarCadFornec();   
			}
			setChanged();
			notifyObservers();
		}
	}
}

