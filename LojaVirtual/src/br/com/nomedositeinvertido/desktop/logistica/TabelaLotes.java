package br.com.nomedositeinvertido.desktop.logistica;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.item.Lote;
import br.com.nomedositeinvertido.jdbc.impl.FornecedorDAO;
import br.com.nomedositeinvertido.jdbc.impl.LoteDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class TabelaLotes extends JInternalFrame implements Observer{
	
	private static TabelaLotes instance = new TabelaLotes();
	private Lote lote;
	private JButton btoEditar;
    private OuvinteTabelaLote ouvinteTabelaLote;
    private javax.swing.JTable table;
	public static int rowAux = -1;
	public static DefaultTableModel dados;
	public static HashMap<Integer, Integer> idMap;
	private JScrollPane scrollPane;
	public static TabelaLotes getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void exibir(){
		setBounds(255, 145, 930, 210);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(255, 145);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Tabela de Lotes");
		
		limpaComponentes();
		
		lote = new Lote();
		idMap = new HashMap<Integer, Integer>();
	    rowAux = -1;
	    		
		////////////// Bot�es-coluna da tabela ------------------------------------------------
		add(criaBotao("C�DIGO", 10, 10, 180, 20));
		add(criaBotao("FORNECEDOR", 190, 10, 180, 20));
		add(criaBotao("VALOR", 370, 10, 180, 20));
		add(criaBotao("DATAALTERA��O", 550, 10, 180, 20));

		add(btoEditar = criaBotao("EDITAR", 750, 10, 160, 40));
		btoEditar.setEnabled(false);
		add(criaBotao("CADASTRAR", 750, 50, 160, 40));

		////////////// Preparando os dados da tabela ---------------------------------------------
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("C�DIGO");
		dados.addColumn("FORNECEDOR");
		dados.addColumn("VALOR");
		dados.addColumn("DATAALTERA��O");
		sincronizaViewComModel(new LoteDAO().consultarTodos(null));
		add(scrollPane = criaTabela());
		dados.fireTableStructureChanged();		
		setVisible(true);
	}// FIM DO METODO CONSTROI TELA
//////////////// Metodos p�blicos ------------------------------------------------------------------	
	private JButton criaBotao(String texto, int esquerda, int topo, int largura, int altura){
		JButton botao = new JButton(texto); // IMPLEMENTADO OK    BOT�O PRODUTO
		ouvinteTabelaLote = new OuvinteTabelaLote(texto);
		botao.setBounds(esquerda, topo, largura, altura);
		botao.addActionListener(ouvinteTabelaLote);
		return botao;
	}	
	public void encerraTela(){
		dispose();		
	}
	public void sincronizaViewComModel(List<EntidadeDominio> model){ 	
		Fornecedor fornecedor = new Fornecedor();
		if(model != null){
			for (int linha = 0; linha < model.size(); linha++) {
				lote = (Lote)model.get(linha);
				Vector vetor = new Vector();
				vetor.add(lote.getCodigo());
				fornecedor.setId(lote.getIdfornecedor());
				fornecedor = (Fornecedor) new FornecedorDAO().consultar(fornecedor);
				vetor.add(fornecedor.getNomeFornec());
				vetor.add(lote.getValorCompra());
				vetor.add(lote.getData());
				dados.addRow(vetor);
				// Cria os mapas para a imagem e o id relacionando-os com sua posi��o na matriz
				idMap.put(dados.getDataVector().get(linha).hashCode(), lote.getId());
			}		
		}
	}//////////////////////////////////////////////////////////////////////////////////
	private JScrollPane criaTabela(){
		table = new JTable(dados);
		table.setAutoCreateColumnsFromModel(false);
		table.setPreferredScrollableViewportSize(new Dimension(720, 142));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				dados.fireTableStructureChanged();
				rowAux = table.rowAtPoint(e.getPoint());
				if(rowAux >= 0){  // Verifica se � uma linha v�lida
					mudaImagem(dados.getDataVector().get(rowAux).hashCode());
				}
			}
		});
		table.doLayout();
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 722, 162);
		return scrollPane;
	}
//---   //// MUDA IMAGEM SEGUNDO O HASHCODE DO PRODUTO DA LINHA ----
	private void mudaImagem(int hashcode){ // non-static
		btoEditar.setEnabled(true);
	}
	private void limpaComponentes(){
	    if(scrollPane != null){
	    	remove(scrollPane);
	    }if(btoEditar != null){
	    	remove(btoEditar);
	    }
	}
//---   //// ORDENA A TABELA PELO INDICE DA COLUNA ------
	static boolean flgCresDecr = false;
	public static void ordenaPorColuna(int colIndex){
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
	public class OuvinteTabelaLote extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteTabelaLote(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(CadastroLote.getInstance());
		}	
		public void actionPerformed(ActionEvent e) {
			if(sOpcao.equals("C�DIGO")){
				TabelaLotes.ordenaPorColuna(0);
			}else if(sOpcao.equals("FORNECEDOR")){
				TabelaLotes.ordenaPorColuna(1);
			}else if(sOpcao.equals("VALOR")){
				TabelaLotes.ordenaPorColuna(2);
			}else if(sOpcao.equals("DATAALTERA��O")){
				TabelaLotes.ordenaPorColuna(3);
			}else if(sOpcao.equals("EDITAR")){
				setChanged();
				notifyObservers(idMap.get(dados.getDataVector().get(rowAux).hashCode()));
				ControleEstoque.mostrarEditLote();
			}else if(sOpcao.equals("CADASTRAR")){
				ControleEstoque.mostrarCadLote();
			}
		}
	}
	@Override
	public void update(Observable o, Object arg) {
		ControleEstoque.mostrarTabLotes();
	}
}


