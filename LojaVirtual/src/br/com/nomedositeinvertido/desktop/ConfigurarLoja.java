package br.com.nomedositeinvertido.desktop;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.loja.Categoria;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.CategoriaDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class ConfigurarLoja extends JInternalFrame implements Observer{

	private static ConfigurarLoja instance = new ConfigurarLoja();
///////// Atributos da GUI ....................
	private JTextField txtFCategoria;
	private JComboBox comboBoxImg;
	private JButton btoCadastro;
	private JButton btoAlteracao;
	private JButton btoexclusao;

//////// Atributos da imagem gerada ...........
	private Image imagemCateg;
	private String srcWebCont;
	private String nomeImagem = null;
	private ImageIcon imagem = null;
	private JLabel lblImagem = null;
	private RedimensionaImagem redim = new RedimensionaImagem();
	
//////// Objeto EntidadeDominio ...............
	private Categoria categoria = new Categoria();
	private CategoriaDAO categDAO = new CategoriaDAO();
	private Fachada fachada = new Fachada();
	private int idCategoria = 0;

////////// METODOS PUBLICOS  .........................	
	
	public static ConfigurarLoja getInstance() {
	    return instance;
	}
	
	@Override
	public void update(Observable o, Object arg) {		
		idCategoria = Integer.parseInt(String.valueOf(arg));
	}
	
	public boolean executaAcao(String acao){
		categoria.setId(idCategoria);
		sincronizarModelComView();
		
		/// CASO SEJA ALTERA��O, APAGAR A IMAGEM NA PASTA E SALVAR A NOVA IMAGEM ------------
		if(imagemCateg != null){
			File f = new File("ImagensGUI/LogoHeader.png");  
			f.delete();
			redim.gravaImagemNaPasta(1146, 290, (BufferedImage) imagemCateg, "ImagensGUI/LogoHeader.png");
		}
		if(!txtFCategoria.getText().equals("")){
			if(acao.equals("CADASTRAR")){
				fachada.cadastrar(categoria, null);
			}else if(acao.equals("ALTERAR")){
				fachada.alterar(categoria, null);
			}else if(acao.equals("EXCLUIR")){
				fachada.excluir(categoria, null);
				dispose();
			}
		}
		return true;
	}
    public void exibir(String sOperacao) {
		setBounds(10, 80, 250, 600); //OK
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		categoria = new Categoria();
		limpaComponentes();

		if(sOperacao == "CADASTRAR"){
			add(btoCadastro = criaBotao("Cadastrar", "CADASTRAR", "Cadastro de categoria", 10, 520, 159, 23));
		}else if(sOperacao == "EDITAR"){
			add(btoAlteracao = criaBotao("Alterar", "ALTERAR", "Edi��o de categoria", 10, 510, 159, 23));
			add(btoexclusao = criaBotao("Exclu�r", "EXCLUIR", "Edi��o de categoria", 10, 533, 159, 23));
		}

		add(criaLabel("Insira a imagem", 10, 10, 119, 18));
		add(criaLabel("Nome da Categoria", 10, 265, 190, 18));

		add(txtFCategoria = criaTextField(10, 285, 220, 20));
		add(comboBoxImg = criaCombobox(itensCBoxImag(), 10, 30, 220, 20));
		
		if(sOperacao == "EDITAR"){
			categoria.setId(idCategoria);
			sincronizarViewComModel((Categoria) new CategoriaDAO().consultar(categoria));
			lblImagem.setBounds(10, 50, 221, 176);
			add(lblImagem);
		}		
		setVisible(true);		
	}
	public void encerraTela(){
		dispose();
	}
	
////////// METODOS PRIVADOS ..........................
    private String[] itensCBoxImag(){
		String dir = "Images"; // Pasta de onde as imagens ser�o lidas
		File diretorio = new File(dir);
		File fList[] = diretorio.listFiles();
		String[] itensComboBox = new String[fList.length+1];
		itensComboBox[0] = "Selecione a foto...";
		for(int i = 0; i < fList.length; i++) 
			itensComboBox[i+1] = fList[i].getName();
    	return itensComboBox;
    } 

    private ItemListener eventoItemImag(String[] itenscombobox) {
    	ItemListener itemListener = null;
    	if(itenscombobox.length > 0 && itenscombobox[0].equals("Selecione a foto...")){ // Evento do ComboBox de imagem
	    	itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
			    	if(e.getStateChange() == ItemEvent.SELECTED){						
						nomeImagem = (String) e.getItem();
						if(lblImagem != null){ // Para n�o dar null pointer
							remove(lblImagem);
						}
						try {
							if(!nomeImagem.equals("Selecione a foto...")){
								imagemCateg = ImageIO.read(new File("Images/" + nomeImagem));
								imagem = new ImageIcon(imagemCateg);
								imagem = redim.resizeProporcional(0,0,new ImageIcon(imagemCateg)); // Redimensiona sempre pra 221/176
							}
						} catch (IOException e1) {  e1.printStackTrace();	}
						lblImagem = new JLabel(imagem, JLabel.CENTER);
						lblImagem.setBounds(10, 50, 221, 176); //OK
						add(lblImagem);
						SwingUtilities.updateComponentTreeUI(lblImagem);
					}
				}
			};
    	}else{ // Evento do ComboBox de categoria
    		itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					categoria.setCategoria((String) e.getItem());
				}
			};
    	}
		return itemListener;
    }
    private JComboBox criaCombobox(String[] itenscombobox, int esquerda, int topo, int largura, int altura){
    	JComboBox combobox = new JComboBox(itenscombobox);
    	combobox.setBounds(esquerda, topo, largura, altura); //OK
    	combobox.addItemListener(eventoItemImag(itenscombobox));
		return combobox;
    }   
    private void sincronizarViewComModel(Categoria model){
    	if(model.getCategoria() != null){
    		txtFCategoria.setText(model.getCategoria());
    	}if(imagem != null){
    		lblImagem = new JLabel(imagem, JLabel.CENTER);
    	}
    }
    private boolean sincronizarModelComView(){
    	categoria = (Categoria) categDAO.consultar(categoria);
		if(!txtFCategoria.getText().equals("")){
			categoria.setCategoria(txtFCategoria.getText());
		}else{
			return false;
		}if(comboBoxImg != null && !comboBoxImg.getSelectedItem().equals("Selecione a foto...")){
		}else{
			return false;
		}
		return true;
    }    
    private JButton criaBotao(String texto, String chave, String tituloTela, int esquerda, int topo, int largura, int altura){
    	JButton botao = new JButton(texto);
    	botao.setBounds(esquerda, topo, largura, altura);
		OuvinteCadLoja ouvinteCadProduto = new OuvinteCadLoja(chave); 
		botao.addActionListener(ouvinteCadProduto);
		setTitle(tituloTela);
		return botao;
    }
    private JTextField criaTextField (int esquerda, int topo, int largura, int altura){
		JTextField textField = new JTextField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }
    private void limpaComponentes(){
    	if(txtFCategoria != null){
    		remove(txtFCategoria);
    	}if(comboBoxImg != null){
        	comboBoxImg.setSelectedIndex(0);
    	}if(lblImagem != null){
        	remove(lblImagem);
    	}if(btoCadastro != null){
    		remove(btoCadastro);
    	}if(btoAlteracao != null){
    		remove(btoAlteracao);
    	}if(btoexclusao != null){
    		remove(btoexclusao);
    	}
    }
    private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
    }    

	private class OuvinteCadLoja extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteCadLoja(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(TabelaCategorias.getInstance());
		}
		public void actionPerformed(ActionEvent e) {
		//////////////////// Fluxo do MenuAdministrador ------------------------------------------------		
			if(sOpcao.equals("CADASTRAR")){   
				if(executaAcao(sOpcao) == false){
					return;
				}
				Prototipo1Teste.mostrarConfLoja();   
			}
			else if(sOpcao.equals("ALTERAR")){ 
				if(executaAcao(sOpcao) == false){
					return;
				}
			}
			else if(sOpcao.equals("EXCLUIR")){
				executaAcao(sOpcao);
				Prototipo1Teste.mostrarConfLoja();   
			}
			setChanged();
			notifyObservers();
		}
	}
}
