package br.com.nomedositeinvertido.desktop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidosAguardPag;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidosFinalizados;

public class TelaHistorico extends JInternalFrame{
	
	private static DefaultTableModel dados;
	private List<Cliente> clientes;
	
//	public TelaHistorico (final ControladorVisao controlador){
//		this.controlador = controlador;
//	}
	
	public void exibir(){		
		setLayout(new FlowLayout()); // adiciona o flowlayout
		dados = new DefaultTableModel();

		dados.addColumn("SITUA��O");
		dados.addColumn("ESTADO");  // adiciona cada coluna a matriz
		dados.addColumn("CIDADE");
		dados.addColumn("CATEGORIA");
		dados.addColumn("PRODUTO");
		dados.addColumn("QUANTIDADE");
		dados.addColumn("DATA");
		
		sincronizarViewComModel(new PedidosAguardPag().factoryPedidoCliente(null));
		sincronizarViewComModel(new PedidosFinalizados().factoryPedidoCliente(null));
		add(criaTabela(5, 10, 1173, 500));

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(1193, 600);
		setVisible(true);
		setTitle("Hist�rico de vendas");
	}
	
	private void sincronizarViewComModel(List<Cliente> model){
		if(model != null){
			clientes = model;
		}	
		for (int i = 0; i < clientes.size(); i++) { // itera entre todas os clientes
			for(int j = 0; j < clientes.get(i).getPedidos().size(); j++){				
				for(int k = 0; k < clientes.get(i).getPedidos().get(j).getItens().size(); k++){
					Vector vetor = new Vector();					   // cria uma matriz local
					vetor.add(clientes.get(i).getPedidos().get(j).getStatus());	
					vetor.add(clientes.get(i).getEndereco().getEstado());
					vetor.add(clientes.get(i).getEndereco().getCidade());
					vetor.add(clientes.get(i).getPedidos().get(j).getItens().get(k).getProduto().getCategoria());
					vetor.add(clientes.get(i).getPedidos().get(j).getItens().get(k).getProduto().getNome());
					vetor.add(clientes.get(i).getPedidos().get(j).getItens().get(k).getQuantidade());					
					vetor.add(clientes.get(i).getPedidos().get(j).getData());			
					dados.addRow(vetor);								// adiciona a linha no vetor
				}				
			}
		}
	}
	
	private JScrollPane criaTabela(int esquerda, int topo, int largura, int altura){
		JTable tabelaFinal = new JTable(dados);
		tabelaFinal.setAutoCreateColumnsFromModel(false);
		tabelaFinal.setPreferredScrollableViewportSize(new Dimension(1150,500));  // seta o tamanho da tela
		tabelaFinal.setSelectionBackground(new Color(255, 255, 255));
		tabelaFinal.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela
		
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(tabelaFinal);
		scrollPane.setBounds(esquerda, topo, largura, altura);
		return scrollPane;
	}	
	
	public static void ordenaPorColuna(int colIndex){ 				 // pega o indice da coluna
		Vector data = dados.getDataVector();       					 // cria objeto vector(Matriz), com colunas e linhas preenchidas
		Collections.sort(data, new OrdenadorColuna(colIndex, false));   // Ordena a matriz usando a .sort, que usa implicitamente o compare do colections
		dados.fireTableStructureChanged(); 							 // Detecta mudan�as e aplica-as
	}
	
	private class OuvinteTabelaHist implements ActionListener {

		private String sOpcao;
		public OuvinteTabelaHist(String sOpcao){
			this.sOpcao = sOpcao;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(sOpcao.equals("SITUACAO")){ 
				TelaHistorico.ordenaPorColuna(6);
			}else if(sOpcao.equals("ESTADO")){		
				TelaHistorico.ordenaPorColuna(0);
			}else if(sOpcao.equals("CIDADE")){ 
				TelaHistorico.ordenaPorColuna(1);
			}else if(sOpcao.equals("CATEGORIA")){ 
				TelaHistorico.ordenaPorColuna(2);
			}else if(sOpcao.equals("PRODUTO")){ 
				TelaHistorico.ordenaPorColuna(3);
			}else if(sOpcao.equals("QUANTIDADE")){ 
				TelaHistorico.ordenaPorColuna(4);
			}else if(sOpcao.equals("DATA")){ 
				TelaHistorico.ordenaPorColuna(5);
			}
		}
	}
}

