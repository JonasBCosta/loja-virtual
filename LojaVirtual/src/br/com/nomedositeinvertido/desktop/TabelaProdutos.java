package br.com.nomedositeinvertido.desktop;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class TabelaProdutos extends JInternalFrame implements Observer{
	
	private static TabelaProdutos instance = new TabelaProdutos();
	private Produto produto;
	private JButton btoEditar;
	private RedimensionaImagem redim;
	private HashMap<Integer, Image> imgMap;
	private HashMap<Integer, String> descMap;
    private JLabel lblImagem;
    private TextArea txtDescri;
    private OuvinteTabelaProd ouvinteTabelaProd;
    private javax.swing.JTable table;
	public static int rowAux = -1;
	public static DefaultTableModel dados;
	public static HashMap<Integer, Integer> idMap;
	private JScrollPane scrollPane;
	public static TabelaProdutos getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void constroiTela(){
		setBounds(273, 80, 930, 600);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(273, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Tabela de Produtos");

		limpaComponentes();
		
		produto = new Produto();
		redim = new RedimensionaImagem();
		imgMap = new HashMap<Integer, Image>();
		idMap = new HashMap<Integer, Integer>();
		descMap = new HashMap<Integer, String>();
	    rowAux = -1;
	    		
		////////////// Bot�es-coluna da tabela ------------------------------------------------
		add(criaBotao("NOME", 10, 10, 128, 20));
		add(criaBotao("PRE�O", 138, 10, 128, 20));
		add(criaBotao("QUANTIDADE", 266, 10, 128, 20));
		add(criaBotao("Q. M�NIMA", 394, 10, 128, 20));
		add(criaBotao("CATEGORIA", 522, 10, 128, 20));
		add(btoEditar = criaBotao("EDITAR", 700, 470, 160, 40));
		btoEditar.setEnabled(false);
		
		////////////// Preparando os dados da tabela ---------------------------------------------
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("NOME");
		dados.addColumn("PRE�O");
		dados.addColumn("QUANTIDADE");
		dados.addColumn("Q. M�NIMA");
		dados.addColumn("CATEGORIA");
		sincronizaViewComModel(new ProdutoDAO().consultarTodos(null));
		add(scrollPane = criaTabela());
		dados.fireTableStructureChanged();		
		setVisible(true);
	}// FIM DO METODO CONSTROI TELA
//////////////// Metodos p�blicos ------------------------------------------------------------------	
	private JButton criaBotao(String texto, int esquerda, int topo, int largura, int altura){
		JButton botao = new JButton(texto); // IMPLEMENTADO OK    BOT�O PRODUTO
		ouvinteTabelaProd = new OuvinteTabelaProd(texto);
		botao.setBounds(esquerda, topo, largura, altura);
		botao.addActionListener(ouvinteTabelaProd);
		return botao;
	}	
	public void encerraTela(){
		dispose();		
	}
	public void sincronizaViewComModel(List<EntidadeDominio> model){ 		
		if(model != null){
			for (int linha = 0; linha < model.size(); linha++) {
				produto = (Produto)model.get(linha);
				Vector vetor = new Vector();
				vetor.add(produto.getNome());
				vetor.add(produto.getPreco());
				vetor.add(produto.getQuantidade());
				vetor.add(produto.getEstqMinimo());
				vetor.add(produto.getCategoria());
				dados.addRow(vetor);
				// Cria os mapas para a imagem e o id relacionando-os com sua posi��o na matriz
				imgMap.put(dados.getDataVector().get(linha).hashCode(), produto.getImagem()); // guarda um mapa com a imagem do produto associada ao hashcode do vetor do mesmo produto
				idMap.put(dados.getDataVector().get(linha).hashCode(), produto.getId());
				descMap.put(dados.getDataVector().get(linha).hashCode(), produto.getDescricao());
			}		
		}
	}//////////////////////////////////////////////////////////////////////////////////
	private JScrollPane criaTabela(){
		table = new JTable(dados);
		table.getColumnModel().getColumn(2).setCellRenderer(new StatusColumnCellRenderer());
		table.setAutoCreateColumnsFromModel(false);
		table.setPreferredScrollableViewportSize(new Dimension(640, 100));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				dados.fireTableStructureChanged();
				rowAux = table.rowAtPoint(e.getPoint());
				if(rowAux >= 0){  // Verifica se � uma linha v�lida
					mudaImagem(dados.getDataVector().get(rowAux).hashCode());
				}
			}
		});
		table.doLayout();
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 642, 500);
		return scrollPane;
	}
//---   //// MUDA IMAGEM SEGUNDO O HASHCODE DO PRODUTO DA LINHA ----
	private void mudaImagem(int hashcode){ // non-static
		if(lblImagem != null || txtDescri != null){ // Para n�o dar null pointer
			remove(lblImagem);
			remove(txtDescri);
		}
		lblImagem = new JLabel(redim.resize(221, 176, 
				new ImageIcon(imgMap.get(dados.getDataVector().get(rowAux).hashCode()))), JLabel.CENTER); 
		lblImagem.setBounds(670, 10, 221, 176);
		txtDescri = new TextArea();
		txtDescri.setVisible(false);
		txtDescri.setBounds(670, 190, 230, 280);
		txtDescri.setText(descMap.get(dados.getDataVector().get(rowAux).hashCode()));
		txtDescri.setEditable(false);
		
		add(lblImagem);
		add(txtDescri);
		btoEditar.setEnabled(true);
		SwingUtilities.updateComponentTreeUI(lblImagem);
		SwingUtilities.updateComponentTreeUI(txtDescri);
		txtDescri.setVisible(true);
	}
	private void limpaComponentes(){
	    if(scrollPane != null){
	    	remove(scrollPane);
	    }if(lblImagem != null){
	    	remove(lblImagem);
	    }if(txtDescri != null){
	    	remove(txtDescri);
	    }if(btoEditar != null){
	    	remove(btoEditar);
	    }
	}
//---   //// ORDENA A TABELA PELO INDICE DA COLUNA ------
	static boolean flgCresDecr = false;
	public static void ordenaPorColuna(int colIndex){
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
	public class OuvinteTabelaProd extends Observable implements ActionListener{
	
		private String sOpcao;
		public OuvinteTabelaProd(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(CadastroProduto.getInstance());
		}	
		public void actionPerformed(ActionEvent e) {
			
			if(sOpcao.equals("NOME")){
				TabelaProdutos.ordenaPorColuna(0);
			}
			else if(sOpcao.equals("PRE�O")){
				TabelaProdutos.ordenaPorColuna(1);
			}
			else if(sOpcao.equals("QUANTIDADE")){
				TabelaProdutos.ordenaPorColuna(2);
			}
			else if(sOpcao.equals("CATEGORIA")){
				TabelaProdutos.ordenaPorColuna(3);
			}else if(sOpcao.equals("EDITAR")){
				setChanged();
				notifyObservers(idMap.get(dados.getDataVector().get(rowAux).hashCode()));
				Prototipo1Teste.mostrarEditProd();
			}
		}
	}
	
	public class StatusColumnCellRenderer extends DefaultTableCellRenderer {
		  @Override
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

		    //Cells are by default rendered as a JLabel.
		    JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

		    //Get the status for the current row.
		    TableModel tableModel = (TableModel) table.getModel();
		    if (Integer.parseInt(String.valueOf(tableModel.getValueAt(row, 2)))
		    		<= Integer.parseInt(String.valueOf(tableModel.getValueAt(row, 3)))) {
		      label.setBackground(new Color(255, 120, 80));		      
		    } else {
		      label.setBackground(Color.WHITE);
		    }

		  //Return the JLabel which renders the cell.
		  return label;

		}
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		Prototipo1Teste.mostrarTabProduto();
	}
}

