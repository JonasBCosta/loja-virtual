package br.com.nomedositeinvertido.desktop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.negocio.factory.impl.PesquisarClientes;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteTabelaProd;

public class TabelaClientes extends JInternalFrame{

//	private ClienteDAO cliDAO;
//	private List<EntidadeDominio> entdoms;
//	private Cliente cliente;
//	private boolean flgPrimPass;
    private static int colAux = -1;
//    public static OuvinteProduto ouvintProd;
    private static DefaultTableModel dados;
//    OuvinteTabelaProd ouvinteTabelaProd;
    private static javax.swing.JTable table;
	
	public void constroiTela(){
//		cliDAO = new ClienteDAO();
//		entdoms = new ArrayList<EntidadeDominio>();
//		cliente = new Cliente();
//		flgPrimPass = true;
	    colAux = -1;
	
/////////////// Preparando os dados da tabela ---------------------------------------------		
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("ID");
		dados.addColumn("LOGIN");
		dados.addColumn("SENHA");
		dados.addColumn("NOME");
		dados.addColumn("CPF");
		dados.addColumn("DATNASC");
		dados.addColumn("CARRINHO");
		dados.addColumn("TELEFONE");
		dados.addColumn("CELULAR");
		dados.addColumn("EMAIL");
		dados.addColumn("CEP");
		dados.addColumn("ESTADO");
		dados.addColumn("CIDADE");
		dados.addColumn("BAIRRO");
		dados.addColumn("RUA");
		dados.addColumn("NUMERO");
		dados.addColumn("PONTREF");
		
		sincronizarViewComModel(new PesquisarClientes().factoryPedidoCliente(null));		
		add(criaTabela());
		dados.fireTableStructureChanged();
	
		setLayout(new FlowLayout()); // adiciona o flowlayout
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setSize(1193, 600);
		setVisible(true);
		setTitle("Tabela de clientes");
		SwingUtilities.updateComponentTreeUI(this);
		
	}// FIM DO METODO CONSTROI TELA
	
	private void sincronizarViewComModel(List<Cliente> model){
		for (int linha = 0; linha < model.size(); linha++) {
			Cliente cliente = (Cliente)model.get(linha);
			Vector vetor = new Vector();
			vetor.add(cliente.getId());
			vetor.add(cliente.getLogin());
			vetor.add(cliente.getSenha());
			vetor.add(cliente.getNome());
			vetor.add(cliente.getCPF()); 
			vetor.add(cliente.getDataNascimento());
			vetor.add(cliente.getPedEmUso().getId());
			vetor.add(cliente.getTelefone());
			vetor.add(cliente.getCelular());
			vetor.add(cliente.getEmail());
			vetor.add(cliente.getEndereco().getCep());
			vetor.add(cliente.getEndereco().getEstado());
			vetor.add(cliente.getEndereco().getCidade());
			vetor.add(cliente.getEndereco().getBairro());
			vetor.add(cliente.getEndereco().getRua());
			vetor.add(cliente.getEndereco().getNumero());
			vetor.add(cliente.getEndereco().getComplemento());
			dados.addRow(vetor);
		}
	}
	private JScrollPane criaTabela(){
		table = new JTable(dados);
		table.setAutoCreateColumnsFromModel(false);
		table.setPreferredScrollableViewportSize(new Dimension(1150,500));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.setSelectionBackground(new Color(255, 255, 255));
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(table.rowAtPoint(e.getPoint()) >= 0)
					ordenaPorColuna(table.columnAtPoint(e.getPoint()));
			}
		});
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(5, 10, 1173, 500);
		return scrollPane;
	}
	
	//---   //// ORDENA A TABELA PELO INDICE DA COLUNA ------
	static boolean flgCresDecr = false;
	public static void ordenaPorColuna(int colIndex){
		colAux = -1;
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
}
