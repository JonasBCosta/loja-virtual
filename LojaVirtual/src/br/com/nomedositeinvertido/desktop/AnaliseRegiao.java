package br.com.nomedositeinvertido.desktop;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;
import br.com.nomedositeinvertido.negocio.decorator.PreparaListaAnalise;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarCategoria;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarPeriodo;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarProduto;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarRegiao;
import br.com.nomedositeinvertido.negocio.decorator.impl.ConjuntoAnalise;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Graficos;

public class AnaliseRegiao extends JInternalFrame {
	
	private static AnaliseRegiao instance = new AnaliseRegiao();
	// Dominio
	private Produto produto;
	private RedimensionaImagem redim;
	private HashMap<Integer, Image> imgMap;
	private HashMap<Integer, Integer> idMap;
	private HashMap<Integer, String> descMap;
	private ProdutoDAO prodDAO = new ProdutoDAO();
	// l�gica
	private static boolean flgCresDecr = false;
    private int rowAux = -1;
    // Listeners
    private ListenerAnlsReg ouvinteAnlsReg;
    // Tabela
    private static DefaultTableModel dados;
    private JLabel lblImagem;
    private TextArea txtDescri;
    private javax.swing.JTable table;
    // Janela
	private JInternalFrame interFrameGrafico;	
	public static AnaliseRegiao getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void exibir(){
		redim = new RedimensionaImagem();
		imgMap = new HashMap<Integer, Image>();
		idMap = new HashMap<Integer, Integer>();
		descMap = new HashMap<Integer, String>();
	    rowAux = -1;
	    produto = null;
	    
		setLayout(null); // adiciona o flowlayout
		setBounds(10, 80, 1193, 600);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setTitle("Gr�fico de vendas por regi�o");
		
		////////////// Bot�es-coluna da tabela ------------------------------------------------
		
		add(criaBotao("NOME", 10, 10, 129, 20));
		add(criaBotao("PRE�O", 138, 10, 129, 20));
		add(criaBotao("QUANTIDADE", 266, 10, 129, 20));
		add(criaBotao("CATEGORIA", 394, 10, 129, 20));
		add(criaBotao("VENDAS", 522, 10, 129, 20));
		
		////////////// Preparando os dados da tabela ---------------------------------------------
		constroiGrafico();
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("NOME");
		dados.addColumn("PRE�O");
		dados.addColumn("QUANTIDADE");
		dados.addColumn("CATEGORIA");
		dados.addColumn("VENDAS");

		sincronizarViewComModel(new ProdutoDAO().consultarTodos(null));
		add(criaTabela());
		dados.fireTableStructureChanged();

		setVisible(true);
		SwingUtilities.updateComponentTreeUI(this);
		
	}// FIM DO METODO CONSTROI TELA

	private JButton criaBotao(String texto, int esquerda, int topo, int largura, int altura){
		JButton botao = new JButton(texto);
		ouvinteAnlsReg = new ListenerAnlsReg(texto);
		botao.setBounds(esquerda, topo, largura, altura);
		botao.addActionListener(ouvinteAnlsReg);
		return botao;
	}
	
	public void encerraTela(){
		dispose();		
	}

	private void sincronizarViewComModel(List<EntidadeDominio> model){ 
		if(model != null){
			for (int linha = 0; linha < model.size(); linha++) {
				Produto produto = (Produto)model.get(linha);
				Vector vetor = new Vector();
				vetor.add(produto.getNome());
				vetor.add(produto.getPreco());
				vetor.add(produto.getQuantidade());
				vetor.add(produto.getCategoria());
				vetor.add(produto.getVendidos());
				dados.addRow(vetor);
				// Cria os mapas para a imagem e o id relacionando-os com sua posi��o na matriz
				imgMap.put(dados.getDataVector().get(linha).hashCode(), produto.getImagem()); // guarda um mapa com a imagem do produto associada ao hashcode do vetor do mesmo produto
				idMap.put(dados.getDataVector().get(linha).hashCode(), produto.getId());
				descMap.put(dados.getDataVector().get(linha).hashCode(), produto.getDescricao());
			}
		}
	}//////////////////////////////////////////////////////////////////////////////////
	
	private JScrollPane criaTabela(){
		table = new JTable(dados);
		table.setAutoCreateColumnsFromModel(false);
		table.setPreferredScrollableViewportSize(new Dimension(640, 100));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				dados.fireTableStructureChanged();
				rowAux = table.rowAtPoint(e.getPoint());
				if(rowAux >= 0){  // Verifica se � uma linha v�lida
					mudaImagem(dados.getDataVector().get(rowAux).hashCode());
				}
			}
		});
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 642, 550);
		return scrollPane;
//		add(scrollPane);
//		dados.fireTableStructureChanged();
	}
	
	private void mudaImagem(int hashcode){
		if(lblImagem != null || txtDescri != null){ // Para n�o dar null pointer
			remove(lblImagem);
			remove(txtDescri);
		}
		lblImagem = new JLabel(redim.resize(221, 176, 
				new ImageIcon(imgMap.get(hashcode))), JLabel.CENTER); 
		lblImagem.setBounds(670, 10, 221, 176);
		txtDescri = new TextArea();
		txtDescri.setBounds(900, 10, 230, 176);
		txtDescri.setText(descMap.get(hashcode));
		txtDescri.setEditable(false);
		// No futuro mude os 3 Maps para um mapa de produto
		produto = new Produto();
		produto.setId(idMap.get(hashcode));
		produto = (Produto) prodDAO.consultar(produto);
		add(lblImagem);
		add(txtDescri);
//		ouvintProd = new OuvinteProduto("altera��o", idMap.get(hashcode));
		constroiGrafico();
	}
	
	private void constroiGrafico(){
		if(interFrameGrafico != null){ // Para n�o dar null pointer
			remove(interFrameGrafico);
		}
		if(produto == null){
			interFrameGrafico = new Graficos("An�lise", "Vendas de todos os produtos" , dadosGrafico());
		}else{
			interFrameGrafico = new Graficos("An�lise", "Vendas de " + produto.getNome() , dadosGrafico());
		}
		interFrameGrafico.setBounds(660, 230, 300, 600);
		interFrameGrafico.addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				interFrameGrafico.setLocation(660, 230);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		add(interFrameGrafico);
		interFrameGrafico.pack();
		interFrameGrafico.setVisible(true);
	}
	
	private static void ordenaPorColuna(int colIndex){
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
	
	private List<EntidadeDominio> dadosGrafico(){
		IDecorator idecorator = new ConjuntoAnalise();
		idecorator = new AdicionarRegiao(idecorator, null);
		idecorator = new AdicionarProduto(idecorator, produto);
		PreparaListaAnalise listaAnalise = new PreparaListaAnalise(idecorator, 27, new Cliente());
		return listaAnalise.ordenaMaisVendidos();
	}
	
	private class ListenerAnlsReg implements ActionListener{
		private String sOpcao;
		public ListenerAnlsReg(String sOpcao){
			this.sOpcao = sOpcao;
		}
		public void actionPerformed(ActionEvent e) {	
			if(sOpcao.equals("nome")){
				ordenaPorColuna(0);
			}else if(sOpcao.equals("pre�o")){
				ordenaPorColuna(1);
			}else if(sOpcao.equals("quantidade")){
				ordenaPorColuna(2);
			}else if(sOpcao.equals("categoria")){
				ordenaPorColuna(3);
			}else if(sOpcao.equals("vendas")){
				ordenaPorColuna(4);
			}
		}
	}

}
