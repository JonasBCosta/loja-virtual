package br.com.nomedositeinvertido.desktop;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;
import br.com.nomedositeinvertido.negocio.decorator.PreparaListaAnalise;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarCategoria;
import br.com.nomedositeinvertido.negocio.decorator.impl.AdicionarPeriodo;
import br.com.nomedositeinvertido.negocio.decorator.impl.ConjuntoAnalise;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Graficos;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;
import br.com.nomedositeinvertido.teste.TesteSysoutClienteCompleto;

import com.toedter.calendar.JCalendar;

public class AnalisePeriodo extends JInternalFrame{

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AnalisePeriodo frame = new AnalisePeriodo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private static AnalisePeriodo instance = new AnalisePeriodo();
	// Dominio
	private RedimensionaImagem redim = new RedimensionaImagem();
	private HashMap<Integer, Image> imgMap;
	private HashMap<Integer, Integer> idMap;
	private HashMap<Integer, String> descMap;
	// l�gica
    public int rowAux = -1;
    public Timestamp dataInic;
    public Timestamp dataTerm;
    // Listeners
    private OuvinteTabela ouvinteTabela;
    // Tabela
    private static DefaultTableModel dados;
    private JLabel lblImagem;
    private TextArea txtDescri;
    private javax.swing.JTable table;
    // Janela
	private JInternalFrame interFrameGrafico;
	private JComboBox comboBoxCtg;
	private String categoria = null;	
	private String fList[] = {"Eletrodomesticos", "Telefones", "Computadores", "Moveis", "Livros", "Utensilios"};
    private String[] itensComboBox = new String[fList.length+1];
	private static boolean flgCresDecr = false;
	public static AnalisePeriodo getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void exibir(){
		imgMap = new HashMap<Integer, Image>();
		idMap = new HashMap<Integer, Integer>();
		descMap = new HashMap<Integer, String>();
	    rowAux = -1;
	    
		setLayout(null);
		setBounds(10, 80, 1193, 600);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setTitle("Gr�fico de vendas por produto");
				
		////////////// Bot�es-coluna da tabela ------------------------------------------------
		add(criaBotao("NOME", 10, 10, 129, 20));
		add(criaBotao("PRE�O", 138, 10, 129, 20));
		add(criaBotao("QUANTIDADE", 266, 10, 129, 20));
		add(criaBotao("CATEGORIA", 394, 10, 129, 20));
		add(criaBotao("VENDAS", 522, 10, 129, 20));
		add(criaBotao("ENTER", 525, 535, 120, 20));
		add(criaBotao("RESET", 410, 535, 120, 20));
		/////// ---------------------------------------------------------------------------------------------------------
		
		/////// COMBOBOX - -----------------------------------------------------------------------------------------------
		preparaComboBox();
//		| | | | | | | | |
//		V V V V V V V V V 
		comboBoxCtg = new JComboBox(itensComboBox);
		comboBoxCtg.setBounds(410, 420, 240, 20); //OK
		add(comboBoxCtg);
		comboBoxCtg.addItemListener(new OuvinteEntradaAnls(null));				
		/////// ---------------------------------------------------------------------------------------------------------
		JCalendar calendarioInic = new JCalendar();
		calendarioInic.setBounds(10, 420, 191, 153);
		calendarioInic.addPropertyChangeListener("calendar", new OuvinteEntradaAnls("dataInic"));	
		add(calendarioInic);
		JCalendar calendarioTerm = new JCalendar();
		calendarioTerm.setBounds(210, 420, 191, 153);
		calendarioTerm.addPropertyChangeListener("calendar", new OuvinteEntradaAnls("dataTerm"));
		add(calendarioTerm);		
		constroiGrafico();
		////// LABELS ----------------------------------------------------------------------------
		add(criaLabel("Inicio do periodo", 10, 390, 191, 23));
		add(criaLabel("Termino do periodo", 210, 390, 191, 23));		
		add(criaLabel("Categoria do produto", 410, 390, 240, 23));
		////// -----------------------------------------------------------------------------------------------------------
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("NOME");
		dados.addColumn("PRE�O");
		dados.addColumn("QUANTIDADE");
		dados.addColumn("CATEGORIA");
		dados.addColumn("VENDAS");
		sincronizarViewComModel(new ProdutoDAO().consultarTodos(null));
		add(criaTabela());
		dados.fireTableStructureChanged();
		// ---------------------------------------------
		SwingUtilities.updateComponentTreeUI(this);
		setVisible(true);
//////// ----------------------------------------------------------------------------------	
	}// FIM DO METODO CONSTROI TELA
	public void encerraTela(){
		dispose();		
	}
	////////PREPARANDO O COMBOBOX DE CATEGORIA - VAI SER MODIFICADO, AS CATEGORIAS SER�O INSERIDAS PELO USU�RIO -------
	private void preparaComboBox(){   
		itensComboBox[0] = "Selecione a categoria...";
		for(int i = 0; i < fList.length; i++) 
			itensComboBox[i+1] = fList[i];
	}
	private JButton criaBotao(String texto, int esquerda, int topo, int largura, int altura){
		JButton botao = new JButton(texto); // IMPLEMENTADO OK    BOT�O PRODUTO
		ouvinteTabela = new OuvinteTabela(texto);
		botao.setBounds(esquerda, topo, largura, altura);
		botao.addActionListener(ouvinteTabela);
		return botao;
	}
	private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 16));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
	}
	private void constroiGrafico(){
		if(interFrameGrafico != null){ // Para n�o dar null pointer
			remove(interFrameGrafico);
		}
		interFrameGrafico = new Graficos("Gr�fico", "10 mais vendidos", dadosGrafico());
		interFrameGrafico.setBounds(660, 255, 300, 600);
		interFrameGrafico.addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				interFrameGrafico.setLocation(660, 230);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		add(interFrameGrafico);
		interFrameGrafico.pack();
		interFrameGrafico.setVisible(true);
	}
	private void sincronizarViewComModel(List<EntidadeDominio> produtos){ 
		if(produtos != null){		
			for (int linha = 0; linha < produtos.size(); linha++) {
				Vector vetor = new Vector();
				Produto produto = (Produto) produtos.get(linha);
				vetor.add(produto.getNome());
				vetor.add(produto.getPreco());
				vetor.add(produto.getQuantidade());
				vetor.add(produto.getCategoria());
				vetor.add(produto.getVendidos());
				dados.addRow(vetor);
				// Cria os mapas para a imagem e o id relacionando-os com sua posi��o na matriz
				imgMap.put(dados.getDataVector().get(linha).hashCode(), produto.getImagem()); // guarda um mapa com a imagem do produto associada ao hashcode do vetor do mesmo produto
				idMap.put(dados.getDataVector().get(linha).hashCode(), produto.getId());
				descMap.put(dados.getDataVector().get(linha).hashCode(), produto.getDescricao());
			}
		}
	}
////////////// Constru�ndo a tabela --------------------------------------------------
	private JScrollPane criaTabela(){ 
		table = new JTable(dados);
		table.setAutoCreateColumnsFromModel(false);
		table.setPreferredScrollableViewportSize(new Dimension(640, 100));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				dados.fireTableStructureChanged();
				rowAux = table.rowAtPoint(e.getPoint());
				if(rowAux >= 0){  // Verifica se � uma linha v�lida
					mudaImagem(dados.getDataVector().get(rowAux).hashCode());
				}
			}
		});
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 642, 374); // wid - hei / 642 - 550
		
		return scrollPane;
	}//////////////////////////////////////////////////////////////////////////////////
//---   //// MUDA IMAGEM SEGUNDO O HASHCODE DO PRODUTO DA LINHA ----
	private void mudaImagem(int hashcode){ // non-static
		if(lblImagem != null || txtDescri != null){ // Para n�o dar null pointer
			remove(lblImagem);
			remove(txtDescri);
		}
		lblImagem = new JLabel(redim.resize(221, 176, new ImageIcon(imgMap.get(hashcode))), JLabel.CENTER); 
		lblImagem.setBounds(670, 10, 221, 176); // 670, 10, 221, 176
		txtDescri = new TextArea();
		txtDescri.setBounds(900, 10, 265, 176);	// 900, 10, 230, 176
		txtDescri.setText(descMap.get(hashcode));
		txtDescri.setEditable(false);		
		add(lblImagem);
		add(txtDescri);		
//		ouvintProd = new OuvinteProduto("altera��o", idMap.get(hashcode));
		SwingUtilities.updateComponentTreeUI(lblImagem);
		SwingUtilities.updateComponentTreeUI(txtDescri);
	}
//---   //// ORDENA A TABELA PELO INDICE DA COLUNA ------
	public static void ordenaPorColuna(int colIndex){
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
	private List<EntidadeDominio> dadosGrafico(){

		IDecorator idecorator = new ConjuntoAnalise();
		idecorator = new AdicionarPeriodo(idecorator, dataInic, dataTerm);
		idecorator = new AdicionarCategoria(idecorator, categoria); 
//		TesteSysoutClienteCompleto tscc = new TesteSysoutClienteCompleto();
//		tscc.testeClienteAtribs(cliente);
		PreparaListaAnalise listaAnalise = new PreparaListaAnalise(idecorator, 10, new Produto());
		
		return listaAnalise.ordenaMaisVendidos();
	}	
	// Classe Listener
	private class OuvinteTabela implements ActionListener{
		private String sOpcao;
		public OuvinteTabela(String sOpcao){
			this.sOpcao = sOpcao;
		}
		public void actionPerformed(ActionEvent e) {
			if(sOpcao.equals("NOME")){
				ordenaPorColuna(0);
			}else if(sOpcao.equals("PRE�O")){
				ordenaPorColuna(1);
			}else if(sOpcao.equals("QUANTIDADE")){
				ordenaPorColuna(2);
			}else if(sOpcao.equals("CATEGORIA")){
				ordenaPorColuna(3);
			}else if(sOpcao.equals("VENDAS")){
				ordenaPorColuna(4);
			}else if(sOpcao.equals("ENTER")){			
				constroiGrafico();
			}else if(sOpcao.equals("RESET")){
				categoria = null;
				dataInic = null;
				dataTerm = null;
				constroiGrafico();
			}
		}
	}
	private class OuvinteEntradaAnls implements ItemListener, PropertyChangeListener{

		private String sOpcao;
		public OuvinteEntradaAnls(String sOpcao){
			this.sOpcao = sOpcao;
		}
		
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED){						
				categoria = ((String) e.getItem());
			}				
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if(sOpcao.equals("dataTerm")){
		        final Calendar c = (Calendar) evt.getNewValue();   
		        if(dataTerm == null){
		        	dataTerm = new Timestamp(0);
		        }
		        dataTerm.setDate(c.get(Calendar.DATE));
		        dataTerm.setMonth(c.get(Calendar.MONTH));
		        dataTerm.setYear(c.get(Calendar.YEAR)-1900);
			}if(sOpcao.equals("dataInic")){
		        final Calendar c = (Calendar) evt.getNewValue(); 
		        if(dataInic == null){
		        	dataInic = new Timestamp(0);
		        }
		        dataInic.setDate(c.get(Calendar.DATE));
		        dataInic.setMonth(c.get(Calendar.MONTH));
		        dataInic.setYear(c.get(Calendar.YEAR)-1900);
			}
		}		
	}
}
