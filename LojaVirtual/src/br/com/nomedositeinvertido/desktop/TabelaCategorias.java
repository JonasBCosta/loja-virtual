package br.com.nomedositeinvertido.desktop;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import br.com.nomedositeinvertido.comparador.impl.OrdenadorColuna;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.loja.Categoria;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.CategoriaDAO;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class TabelaCategorias extends JInternalFrame implements Observer{
	
	private static TabelaCategorias instance = new TabelaCategorias();
	private Categoria categoria;
	private JButton btoEditar;
    private OuvinteTabelaProd ouvinteTabelaProd;
    private javax.swing.JTable table;
	public static int rowAux = -1;
	public static DefaultTableModel dados;
	public static HashMap<Integer, Integer> idMap;
	private JScrollPane scrollPane;
	public static TabelaCategorias getInstance() {
	    return instance;
	}
//////// Constroi Tela ----------------------------------------------
	public void constroiTela(){
		setBounds(273, 80, 930, 600);
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(273, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setTitle("Tabela de Categorias");

		limpaComponentes();
		
		categoria = new Categoria();
		idMap = new HashMap<Integer, Integer>();
	    rowAux = -1;
	    		
		////////////// Bot�es-coluna da tabela ------------------------------------------------
		add(criaBotao("CATEGORIA", 10, 10, 320, 20));
		add(criaBotao("DATA", 330, 10, 320, 20));
		add(btoEditar = criaBotao("EDITAR", 700, 470, 160, 40));
		btoEditar.setEnabled(false);
		
		////////////// Preparando os dados da tabela ---------------------------------------------
		dados = new DefaultTableModel() { // RENOVA ARRAY DADOS
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		dados.addColumn("CATEGORIA");
		dados.addColumn("DATA");
		sincronizaViewComModel(new CategoriaDAO().consultarTodos(null));
		add(scrollPane = criaTabela());
		dados.fireTableStructureChanged();		
		setVisible(true);
	}// FIM DO METODO CONSTROI TELA
//////////////// Metodos p�blicos ------------------------------------------------------------------	
	private JButton criaBotao(String texto, int esquerda, int topo, int largura, int altura){
		JButton botao = new JButton(texto); // IMPLEMENTADO OK    BOT�O PRODUTO
		ouvinteTabelaProd = new OuvinteTabelaProd(texto);
		botao.setBounds(esquerda, topo, largura, altura);
		botao.addActionListener(ouvinteTabelaProd);
		return botao;
	}	
	public void encerraTela(){
		dispose();		
	}
	public void sincronizaViewComModel(List<EntidadeDominio> model){ 		
		if(model != null){
			for (int linha = 0; linha < model.size(); linha++) {
				categoria = (Categoria)model.get(linha);
				Vector vetor = new Vector();
				vetor.add(categoria.getCategoria());
				vetor.add(categoria.getData());
				dados.addRow(vetor);
				// Cria os mapas para a imagem e o id relacionando-os com sua posi��o na matriz
				idMap.put(dados.getDataVector().get(linha).hashCode(), categoria.getId());
			}		
		}
	}//////////////////////////////////////////////////////////////////////////////////
	private JScrollPane criaTabela(){
		table = new JTable(dados);
		table.setPreferredScrollableViewportSize(new Dimension(640, 100));  // seta o tamanho da tela
		table.setFillsViewportHeight(true); // alarga a tela conforme o tamanho da tabela	
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				dados.fireTableStructureChanged();
				rowAux = table.rowAtPoint(e.getPoint());
			}
		});
		table.doLayout();
		// Cria um painel de rolamento e coloca a tabela nele
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 642, 500);
		return scrollPane;
	}

	private void limpaComponentes(){
	    if(scrollPane != null){
	    	remove(scrollPane);
	    }if(btoEditar != null){
	    	remove(btoEditar);
	    }
	}
//---   //// ORDENA A TABELA PELO INDICE DA COLUNA ------
	static boolean flgCresDecr = false;
	public static void ordenaPorColuna(int colIndex){
		Vector data = dados.getDataVector();
		if(flgCresDecr == false)
			flgCresDecr = true;
		else
			flgCresDecr = false;
		Collections.sort(data, new OrdenadorColuna(colIndex, flgCresDecr));
		dados.fireTableStructureChanged();
	}
	public class OuvinteTabelaProd extends Observable implements ActionListener{
	
		private String sOpcao;
		public OuvinteTabelaProd(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(ConfigurarLoja.getInstance());
		}	
		public void actionPerformed(ActionEvent e) {			
			if(sOpcao.equals("CATEGORIA")){
				TabelaProdutos.ordenaPorColuna(0);
			}
			else if(sOpcao.equals("DATA")){
				TabelaProdutos.ordenaPorColuna(1);
			}else if(sOpcao.equals("EDITAR")){
				setChanged();
				notifyObservers(idMap.get(dados.getDataVector().get(rowAux).hashCode()));
				Prototipo1Teste.mostrarEditProd();
			}
		}
	}
	@Override
	public void update(Observable o, Object arg) {
		Prototipo1Teste.mostrarTabCategorias();
	}
}

