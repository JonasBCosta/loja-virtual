package br.com.nomedositeinvertido.desktop;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
//import br.com.nomedositeinvertido.telas.controle.controladores.OuvinteProduto;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class CadastroProduto extends JInternalFrame implements Observer{

	private static CadastroProduto instance = new CadastroProduto();
///////// Atributos da GUI ....................
	private TextArea txtAreaDesc;
	private JTextField txtFNome;
	private JTextField txtFPreco;
	private JTextField txtFEstqMinimo;
	private JComboBox comboBoxCtg;
	private JComboBox comboBoxImg;
	private JButton btoCadastro;
	private JButton btoAlteracao;
	private JButton btoexclusao;

//////// Atributos da imagem gerada ...........
	private String nomeImagem = null;
	private ImageIcon imagem = null;
	private JLabel lblImagem = null;
	private RedimensionaImagem redim = new RedimensionaImagem();
	
//////// Objeto EntidadeDominio ...............
	private Produto produto = new Produto();
	private ProdutoDAO prodDAO = new ProdutoDAO();
	private String nomeTratado;
	private Fachada fachada = new Fachada();
	private int idProduto = 0;

////////// METODOS PUBLICOS  .........................	
	
	public static CadastroProduto getInstance() {
	    return instance;
	}
	public boolean executaAcao(String acao){
		produto.setId(idProduto);
		trataNomeImg();                  
		sincronizarModelComView();
		
		if(acao.equals("CADASTRAR") && sincronizarModelComView() == false){
			JOptionPane.showMessageDialog(null, "Certifique-se de que todos os campos do cadastro foram preenchidos.");
			return false;
		}
		/// CASO SEJA ALTERA��O, APAGAR A IMAGEM NA PASTA E SALVAR A NOVA IMAGEM ------------
		
		if(acao.equals("CADASTRAR") || acao.equals("ALTERAR")){
			redim.gravaImagemNaPasta(221, 176, (BufferedImage) produto.getImagem(), produto.getSrcWebCont());
		}
		if(acao.equals("CADASTRAR")){
			fachada.cadastrar(produto, null);
		}else if(acao.equals("ALTERAR")){
			fachada.alterar(produto, null);
		}else if(acao.equals("EXCLUIR")){
			fachada.excluir(produto, null);
			dispose();
		}
		return true;
	}
    public void constroiTela(String sOperacao) {
		setBounds(10, 80, 250, 600); //OK
		addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		produto = new Produto();
		limpaComponentes();

		if(sOperacao == "CADASTRAR"){
			add(btoCadastro = criaBotao("Cadastrar", "CADASTRAR", "Cadastro de Produto", 10, 520, 159, 23));
		}else if(sOperacao == "EDITAR"){
			add(btoAlteracao = criaBotao("Alterar", "ALTERAR", "Edi��o de Produto", 10, 510, 159, 23));
			add(btoexclusao = criaBotao("Exclu�r", "EXCLUIR", "Edi��o de Produto", 10, 533, 159, 23));
		}
		txtAreaDesc = new TextArea();
		txtAreaDesc.setBounds(10, 422, 220, 90);
		add(txtAreaDesc);
		add(criaLabel("Insira a imagem", 10, 10, 119, 18));
		add(criaLabel("Descri��o do produto", 10, 402, 196, 18));
		add(criaLabel("Nome do produto", 10, 265, 125, 18));
		add(criaLabel("Pre�o do Produto", 10, 310, 125, 18));
		add(criaLabel("N�vel de estoque cr�tico", 10, 357, 200, 18));
		add(txtFNome = criaTextField(10, 285, 220, 20));
		add(txtFPreco = criaMoneyTextField(10, 330, 220, 20));		
		add(comboBoxCtg = criaCombobox(itensCBoxCatg(), 10, 240, 220, 20));
		add(comboBoxImg = criaCombobox(itensCBoxImag(), 10, 30, 220, 20));
		add(txtFEstqMinimo = criaNumberTextField(10, 377, 50, 18));
		
		if(sOperacao == "EDITAR"){
			produto.setId(idProduto);
			sincronizarViewComModel((Produto) new ProdutoDAO().consultar(produto));
			lblImagem.setBounds(10, 50, 221, 176);
			add(lblImagem);
		}		
		setVisible(true);		
	}
	public void encerraTela(){
		dispose();
	}
	@Override
	public void update(Observable o, Object arg) {		
		idProduto = Integer.parseInt(String.valueOf(arg));
	}
////////// METODOS PRIVADOS ..........................
    private String[] itensCBoxImag(){
		String dir = "Images"; // Pasta de onde as imagens ser�o lidas
		File diretorio = new File(dir);
		File fList[] = diretorio.listFiles();
		String[] itensComboBox = new String[fList.length+1];
		itensComboBox[0] = "Selecione a foto...";
		for(int i = 0; i < fList.length; i++) 
			itensComboBox[i+1] = fList[i].getName();
    	return itensComboBox;
    } 
    private String[] itensCBoxCatg(){
//    	for (EntidadeDominio entDom : categDAO.consultarTodos(null)) {
//			categList.add("Eletrodom�sticos");
//			categList.add("");
//			categList.add((Categoria) entDom);
//			categList.add((Categoria) entDom);
//			categList.add((Categoria) entDom);
//			categList.add((Categoria) entDom);
//
////		}
//		String fList[] = new String[categList.size()];
//		for (int i = 0; i < categList.size(); i++) {
//			fList[i] = categList.get(i).getCategoria();
//		}
		String[] itensComboBox = new String[7]; 
		itensComboBox[0] = "Selecione a categoria...";
//		for(int i = 0; i < 7; i++) 
		itensComboBox[1] = "Eletrodom�sticos";
		itensComboBox[2] = "Telefones";
		itensComboBox[3] = "Computadores";
		itensComboBox[4] = "M�veis";
		itensComboBox[5] = "Livros";
		itensComboBox[6] = "Utens�lios";
		return itensComboBox;
    }
    private ItemListener eventoItemImag(String[] itenscombobox) {
    	ItemListener itemListener = null;
    	if(itenscombobox.length > 0 && itenscombobox[0].equals("Selecione a foto...")){ // Evento do ComboBox de imagem
	    	itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
			    	if(e.getStateChange() == ItemEvent.SELECTED){						
						nomeImagem = (String) e.getItem();
						if(lblImagem != null){ // Para n�o dar null pointer
							remove(lblImagem);
						}
						try {
							if(!nomeImagem.equals("Selecione a foto...")){
								produto.setImagem(ImageIO.read(new File("Images/" + nomeImagem)));
								imagem = new ImageIcon(produto.getImagem());
								imagem = redim.resizeProporcional(0,0,new ImageIcon(produto.getImagem())); // Redimensiona sempre pra 221/176
								produto.setEnderecoImagem("Images/" + nomeImagem);
							}
						} catch (IOException e1) {  e1.printStackTrace();	}
						lblImagem = new JLabel(imagem, JLabel.CENTER);
						lblImagem.setBounds(10, 50, 221, 176); //OK
						add(lblImagem);
						SwingUtilities.updateComponentTreeUI(lblImagem);
					}
				}
			};
    	}else{ // Evento do ComboBox de categoria
    		itemListener = new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					produto.setCategoria((String) e.getItem());
				}
			};
    	}
		return itemListener;
    }
    private JComboBox criaCombobox(String[] itenscombobox, int esquerda, int topo, int largura, int altura){
    	JComboBox combobox = new JComboBox(itenscombobox);
    	combobox.setBounds(esquerda, topo, largura, altura); //OK
    	combobox.addItemListener(eventoItemImag(itenscombobox));
		return combobox;
    }   
    private void sincronizarViewComModel(Produto model){
    	if(model.getNome() != null){
    		txtFNome.setText(model.getNome());
    	}if(model.getDescricao() != null){
    	    txtAreaDesc.setText(model.getDescricao());
    	}if(model.getPreco() != null){
    		txtFPreco.setText(String.valueOf(model.getPreco()));
    	}if(model.getCategoria() != null){
    		comboBoxCtg.setSelectedItem(String.valueOf(model.getCategoria()));
    	}if(model.getImagem() != null){
    		imagem = new ImageIcon(model.getImagem());
    		imagem = redim.resizeProporcional(0,0,new ImageIcon(model.getImagem()));
    	}if(imagem != null){
    		lblImagem = new JLabel(imagem, JLabel.CENTER);
    	}if(model.getEstqMinimo() >= 0){
    		txtFEstqMinimo.setText(String.valueOf(model.getEstqMinimo()));;
    	}
    }
    private boolean sincronizarModelComView(){
    	Produto produto2 = (Produto) prodDAO.consultar(produto);
    	
		if(!txtFNome.getText().equals("")){
			produto.setNome(txtFNome.getText());
		}else{
			return false;
		}if(nomeTratado != null){			
			produto.setSrcWebCont("ImagensGUI/" + nomeTratado + ".png");
//		}if(nomeTratado != null){			
//			produto.setEnderecoImagem(nomeTratado + ".png");
		}else{
			return false;
		}if(!txtFPreco.getText().equals("0,00")){
			produto.setPreco(BigDecimal.valueOf(Double.parseDouble(txtFPreco.getText().replace(",", "."))));
		}else{
			return false;
		}if(!txtAreaDesc.getText().equals("")){
			produto.setDescricao(txtAreaDesc.getText());
		}else{
			return false;
		}if(comboBoxCtg != null && !comboBoxCtg.getSelectedItem().equals("Selecione a categoria...")){
			produto.setCategoria(String.valueOf(comboBoxCtg.getSelectedItem()));
		}else{
			return false;
		}if(comboBoxImg != null && !comboBoxImg.getSelectedItem().equals("Selecione a foto...")){
		}else{
//			return false;
		}if(!txtFEstqMinimo.getText().equals("")){
			produto.setEstqMinimo(Integer.parseInt(txtFEstqMinimo.getText()));
		}else{
			return false;
		}
		return true;
    }    
    private JButton criaBotao(String texto, String chave, String tituloTela, int esquerda, int topo, int largura, int altura){
    	JButton botao = new JButton(texto);
    	botao.setBounds(esquerda, topo, largura, altura);
		OuvinteCadProduto ouvinteCadProduto = new OuvinteCadProduto(chave); 
		botao.addActionListener(ouvinteCadProduto);
		setTitle(tituloTela);
		return botao;
    }
    private JTextField criaTextField (int esquerda, int topo, int largura, int altura){
		JTextField textField = new JTextField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }
    private JTextField criaNumberTextField (int esquerda, int topo, int largura, int altura){
		NumberField textField = new NumberField();
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }
    private JTextField criaMoneyTextField (int esquerda, int topo, int largura, int altura){
    	JNumberFormatField textField = new JNumberFormatField(new DecimalFormat("0.00"));
		textField.setBounds(esquerda, topo, largura, altura);
		textField.setColumns(10);
		return textField;
    }
    private void limpaComponentes(){
    	if(txtFNome != null){
    		remove(txtFNome);
    	}if(txtAreaDesc != null){
    		remove(txtAreaDesc);
    	}if(txtFPreco != null){
    		remove(txtFPreco);
    	}if(comboBoxCtg != null){
        	remove(comboBoxCtg);
    	}if(comboBoxImg != null){
        	comboBoxImg.setSelectedIndex(0);
    	}if(lblImagem != null){
        	remove(lblImagem);
    	}if(btoCadastro != null){
    		remove(btoCadastro);
    	}if(btoAlteracao != null){
    		remove(btoAlteracao);
    	}if(btoexclusao != null){
    		remove(btoexclusao);
    	}if(txtFEstqMinimo != null){
    		remove(txtFEstqMinimo);
    	}
    }
    private JLabel criaLabel(String texto, int esquerda, int topo, int largura, int altura){
		JLabel label = new JLabel(texto);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label.setBounds(esquerda, topo, largura, altura);
		return label;
    }    
	private void trataNomeImg(){
		nomeTratado = txtFNome.getText().replaceAll(" ", "_"); 
		nomeTratado = nomeTratado.replaceAll("�", "c"); // substitui as cedilhas por c
		nomeTratado = nomeTratado.replaceAll("�", "C"); // �
		nomeTratado = nomeTratado.replaceAll("�", "a"); // � {
		nomeTratado = nomeTratado.replaceAll("�", "A"); // �
		nomeTratado = nomeTratado.replaceAll("�", "e"); // �
		nomeTratado = nomeTratado.replaceAll("�", "E"); // �
		nomeTratado = nomeTratado.replaceAll("�", "i"); // �   acento agudo
		nomeTratado = nomeTratado.replaceAll("�", "I"); // �
		nomeTratado = nomeTratado.replaceAll("�", "o"); // �
		nomeTratado = nomeTratado.replaceAll("�", "O"); // �
		nomeTratado = nomeTratado.replaceAll("�", "u"); // �
		nomeTratado = nomeTratado.replaceAll("�", "U"); // � }
		nomeTratado = nomeTratado.replaceAll("�", "n"); // � {
		nomeTratado = nomeTratado.replaceAll("�", "N"); // �  Tio
		nomeTratado = nomeTratado.replaceAll("�", "a"); // �
		nomeTratado = nomeTratado.replaceAll("�", "A"); // �
		nomeTratado = nomeTratado.replaceAll("�", "o"); // �
		nomeTratado = nomeTratado.replaceAll("�", "O"); // � }
		nomeTratado = nomeTratado.replaceAll("�", "a"); // � {
		nomeTratado = nomeTratado.replaceAll("�", "A"); // �
		nomeTratado = nomeTratado.replaceAll("�", "e"); // �
		nomeTratado = nomeTratado.replaceAll("�", "E"); // �
		nomeTratado = nomeTratado.replaceAll("�", "i"); // �
		nomeTratado = nomeTratado.replaceAll("�", "I"); // �  acento circunflexo
		nomeTratado = nomeTratado.replaceAll("�", "o"); // �
		nomeTratado = nomeTratado.replaceAll("�", "O"); // �
		nomeTratado = nomeTratado.replaceAll("�", "u"); // �
		nomeTratado = nomeTratado.replaceAll("�", "U"); // � }
		nomeTratado = nomeTratado.replaceAll("�", "a"); // � {
		nomeTratado = nomeTratado.replaceAll("�", "A"); // �
		nomeTratado = nomeTratado.replaceAll("�", "e"); // �
		nomeTratado = nomeTratado.replaceAll("�", "E"); // �
		nomeTratado = nomeTratado.replaceAll("�", "i"); // �  cr�ze
		nomeTratado = nomeTratado.replaceAll("�", "I"); // �
		nomeTratado = nomeTratado.replaceAll("�", "o"); // �
		nomeTratado = nomeTratado.replaceAll("�", "O"); // �
		nomeTratado = nomeTratado.replaceAll("�", "u"); // �
		nomeTratado = nomeTratado.replaceAll("�", "U"); // � }
	}
	private class OuvinteCadProduto extends Observable implements ActionListener{
		private String sOpcao;
		public OuvinteCadProduto(String sOpcao){
			this.sOpcao = sOpcao;
			addObserver(TabelaProdutos.getInstance());
		}
		public void actionPerformed(ActionEvent e) {
		//////////////////// Fluxo do MenuAdministrador ------------------------------------------------		
			if(sOpcao.equals("CADASTRAR")){   
				if(executaAcao(sOpcao) == false){
					return;
				}
				Prototipo1Teste.mostrarCadProduto();   
			}
			else if(sOpcao.equals("ALTERAR")){ 
				if(executaAcao(sOpcao) == false){
					return;
				}
				Prototipo1Teste.mostrarCadProduto();   
			}
			else if(sOpcao.equals("EXCLUIR")){
				executaAcao(sOpcao);
				Prototipo1Teste.mostrarCadProduto();   
			}
			setChanged();
			notifyObservers();
		}
	}
	public class NumberField extends JTextField {
	    public NumberField() {
	        this( null );
	    }
	    public NumberField( String text ) {
	        super( text );
	        setDocument( new PlainDocument() {
		    @Override
	    	    public void insertString( int offs, String str, AttributeSet a ) throws BadLocationException {
	                //normalmente apenas uma letra � inserida por vez,
	                //mas fazendo assim tamb�m previne caaso o usu�rio
	                //cole algum texto
	                for( int i = 0; i < str.length(); i++ )
	                    if( Character.isDigit( str.charAt( i ) ) == false )
	                        return;
	                super.insertString( offs, str, a );
	            }
	        } );
	    }
	}
	public static class JNumberFormatField extends JTextField {
	    private static final long serialVersionUID = -7506506392528621022L;
	    private final static NumberFormat MONETARY_FORMAT = new DecimalFormat("R$ #,##0.00");
	    private NumberFormat numberFormat;
	    private int limit = -1;
	    public JNumberFormatField(int casasDecimais) {
	        this(new DecimalFormat((casasDecimais == 0 ? "#,##0" : "#,##0.") + makeZeros(casasDecimais)));
	    }
	    public JNumberFormatField() {
	        this(MONETARY_FORMAT);
	    }
	    public JNumberFormatField(NumberFormat format) {// define o formato do
	        // n�mero
	        numberFormat = format;// alinhamento horizontal para o texto
	        setHorizontalAlignment(RIGHT);// documento respons�vel pela formata��o
	        // do campo
	        setDocument(new PlainDocument() {
	            private static final long serialVersionUID = 1L;
	            @Override
	            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
	                String text = new StringBuilder(JNumberFormatField.this.getText().replaceAll("[^0-9]", "")).append(str.replaceAll("[^0-9]", "")).toString();
	                super.remove(0, getLength());
	                if (text.isEmpty()) {
	                    text = "0";
	                } else {
	                    text = new BigInteger(text).toString();
	                }
	                super.insertString(0, numberFormat.format(new BigDecimal(getLimit() > 0 == text.length() > getLimit() ? text.substring(0, getLimit()) : text).divide(new BigDecimal(Math.pow(10, numberFormat.getMaximumFractionDigits())))), a);
	            }
	            @Override
	            public void remove(int offs, int len) throws BadLocationException {
	                super.remove(offs, len);
	                if (len != getLength()) {
	                    insertString(0, "", null);
	                }
	            }
	        });// mantem o cursor no final
	        // do campo
	        addCaretListener(new CaretListener() {
	            boolean update = false;
	            @Override
	            public void caretUpdate(CaretEvent e) {
	                if (!update) {
	                    update = true;
	                    setCaretPosition(getText().length());
	                    update = false;
	                }
	            }
	        });// limpa o campo se
	        // apertar DELETE
	        addKeyListener(new KeyAdapter() {
	            @Override
	            public void keyPressed(KeyEvent e) {
	                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
	                    setText("");
	                }
	            }
	        });// formato
	        // inicial
	        setText("0");
	        setCaretPosition(getText().length());
	    }
	    /***
	     * Define um valor BigDecimal ao campo**
	     *
	     * @param value
	     */
	    public void setValue(BigDecimal value) {
	        super.setText(numberFormat.format(value));
	    }
	    /***
	     * Recupera um valor BigDecimal do campo**
	     *
	     * @return
	     */
	    public final BigDecimal getValue() {
	        return new BigDecimal(getText().replaceAll("[^0-9]", "")).divide(new BigDecimal(Math.pow(10, numberFormat.getMaximumFractionDigits())));
	    }
	    /***
	     * Recupera o formatador atual do campo**
	     *
	     * @return
	     */
	    public NumberFormat getNumberFormat() {
	        return numberFormat;
	    }
	    /***
	     * Define o formatador do campo** @param numberFormat
	     */
	    public void setNumberFormat(NumberFormat numberFormat) {
	        this.numberFormat = numberFormat;
	    }
	    /***
	     * Preenche um StringBuilder com zeros** @param zeros*
	     *
	     * @return
	     */
	    private static final String makeZeros(int zeros) {
	        if (zeros >= 0) {
	            StringBuilder builder = new StringBuilder();
	            for (int i = 0; i < zeros; i++) {
	                builder.append('0');
	            }
	            return builder.toString();
	        } else {
	            throw new RuntimeException("N�mero de casas decimais inv�lida (" + zeros + ")");
	        }
	    }
	    /***
	     * Recupera o limite do campo.** @return
	     */
	    public int getLimit() {
	        return limit;
	    }
	    /***
	     * Define o limite do campo, limit < 0 para deixar livre (default) Ignora os
	     * pontos e virgulas do formato, conta* somente com os n�meros** @param
	     * limit
	     */
	    public void setLimit(int limit) {
	        this.limit = limit;
	    }
	}
	

}
