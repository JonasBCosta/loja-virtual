package br.com.nomedositeinvertido.negocio.command.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class CommandSalvar extends AbstractAcoesCRUD{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {	
//		
//		if(entidade instanceof Historico){
//			
//			Historico hist = (Historico)entidade; 	  // cria a objeto hist�rico com o parametro
//			PedidoDAO pedDAO = new PedidoDAO();		  // cria o objeto pedidoDAO
//			Pedido pedido = new Pedido();		  	  // cria o objeto pedido
//			ItemDAO itemDAO = new ItemDAO();		  // cria o objeto itemDAO 
//			Item item = new Item();					  // cria o objeto item
//			ProdutoDAO proDAO = new ProdutoDAO();	  // cria o objeto do produtoDAO 
//			Produto produto = new Produto();		  // cria o objeto do produto
//			ClienteDAO cliDAO = new ClienteDAO();
//			
//			List<EntidadeDominio> entdoms = new ArrayList<EntidadeDominio>(); // cria array de entidade dominio
//
//			pedido.setIdCliente(hist.getIdCliente());   // grava o idcliente do pedido igual o do historico		
//			pedido = (Pedido) pedDAO.consultar(pedido); // consulta o pedido referente ao passado pelo historico		
//			
//			item.setIdPedido(pedido.getId());		  // seta o item com o id do pedido do cliente do historico
//			entdoms = itemDAO.consultarTodos(item);   // consulta todos os itens do pedido do cliente do historico
//			
//			for (EntidadeDominio entdom : entdoms) {
//				item = (Item) entdom;
//				produto.setId(item.getIdProduto());
//				produto = (Produto) proDAO.consultar(produto);
//								
//				hist.setCategoria(produto.getCategoria());
//				hist.setCidade(LoginClienteVHWeb.cliente.getEndereco().getCidade());
//				hist.setEstado(LoginClienteVHWeb.cliente.getEndereco().getEstado());
//				hist.setNomeProd(produto.getNome());
//				hist.setPre�o(produto.getPreco());
//				hist.setQuantidade(item.getQuantidade());
//				hist.setIdProduto(produto.getId());
//				
//				// acrescenta na quantidade de vendas do produto a quantidade desta venda
//				produto.setVendidos(produto.getVendidos() + item.getQuantidade());
//				proDAO.alterar(produto);
//				
//				fachada.cadastrar(hist);
//			}			
//			
//			pedDAO.excluir(pedido);    // exclui o pedido atual (Exclui os itens em cascata)
//			pedDAO.cadastrar(pedido);  // cria novo pedido com id novo
//			pedido = (Pedido) pedDAO.consultar(pedido); // consulta pedido novo
//			LoginClienteVHWeb.cliente.setPedido(pedido.getId()); // seta o numero do pedido no cliente			
//			cliDAO.alterar(LoginClienteVHWeb.cliente);
//			
//		} else {
			return fachada.cadastrar(entidade, cliente); // Retorna mensagem, caso haja erro, sen�o retorna null
//		}
//		return null;
	}	
}
