package br.com.nomedositeinvertido.negocio.command.impl;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class CommandExcluir extends AbstractAcoesCRUD{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {	
		return fachada.excluir(entidade, cliente); // retorna sempre null
	}
	
}
