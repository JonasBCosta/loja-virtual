package br.com.nomedositeinvertido.negocio.command.impl;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class CommandConsultar extends AbstractAcoesCRUD{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {	
		return fachada.consultar(entidade); // retorna objeto (entidade).
	}
	
}
