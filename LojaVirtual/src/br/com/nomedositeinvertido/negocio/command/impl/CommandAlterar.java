package br.com.nomedositeinvertido.negocio.command.impl;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class CommandAlterar extends AbstractAcoesCRUD{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {	
		return fachada.alterar(entidade, cliente); // retorna mensagem caso haja erro, sen�o retorna null
	}
}
