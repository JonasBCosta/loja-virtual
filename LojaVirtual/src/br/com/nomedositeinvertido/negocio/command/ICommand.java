package br.com.nomedositeinvertido.negocio.command;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public interface ICommand {

	public Object execute(EntidadeDominio entidade, Cliente cliente);

}
