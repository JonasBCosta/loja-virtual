package br.com.nomedositeinvertido.negocio.factory.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.negocio.factory.IFactory;

public abstract class AbstractFactory implements IFactory{
	
	/**
	 * Este metodo monta todos os hist�ricos a partir 
	 * dos clientes, seus pedidos e itens
	 * O primeiro parametro n�o deve ser especificado
	 * Se o segundo parametro for especificado, este
	 * metodo retornar� somente o hist�rico do cliente
	 * especificado.
	 * @param EntidadeDominio
	 * @param Cliente
	 * @return List<Pedido>
	 * @see List<Pedido>
	 */
	
	protected List<Cliente> pesquisarTodos(Cliente c) {

		Pedido pedido = new Pedido();		  	  // cria o objeto pedido
		Item item = new Item();					  // cria o objeto item
		Produto produto = new Produto();		  // cria o objeto produto		
		Cliente cliente = new Cliente();
		
		List<Cliente> clientes = new ArrayList<Cliente>();
		List<Item> itens = new ArrayList<Item>();
		List<Pedido> pedidos = new ArrayList<Pedido>();
		List<Cliente> auxClientes = new ArrayList<Cliente>();
		
		PedidoDAO pedDAO = new PedidoDAO();		  // cria o objeto pedidoDAO
		ItemDAO itemDAO = new ItemDAO();		  // cria o objeto itemDAO 
		ProdutoDAO proDAO = new ProdutoDAO();	  // cria o objeto produtoDAO 		
		ClienteDAO cliDAO = new ClienteDAO();
		
		List<EntidadeDominio> entClis = new ArrayList<EntidadeDominio>(); // cria array de entidade dominio
		List<EntidadeDominio> entPeds = new ArrayList<EntidadeDominio>(); // cria array de entidade dominio
		List<EntidadeDominio> entItens = new ArrayList<EntidadeDominio>(); // cria array de entidade dominio

		entClis = cliDAO.consultarTodos(cliente);
		
		/**
		 * Nota: os atributos do tipo EntidadeDominio (getCliente(), getPedidos(), getItens())das classes,
		 * n�o vem na consulta DAO, elas devem passar pela consulta de sua classe correspondente e ser
		 * adicionada depois na sua classe de composi��o. 
		 */
		for (EntidadeDominio entCli : entClis) { //Itera entre todos os clientes
			cliente = (Cliente) entCli;		// Cliente s� n�o tem a lista de pedidos
			pedido.setIdCliente(cliente.getId());		
			entPeds = pedDAO.consultarTodos(pedido); 
			for (EntidadeDominio entPed : entPeds) { //Itera entre todos os pedidos do cliente
				pedido = (Pedido) entPed;
				item.setIdPedido(pedido.getId());
				entItens = itemDAO.consultarTodos(item);
				for (EntidadeDominio entItem : entItens) { //Itera entre todos os itens do pedido
					item = (Item) entItem;
					produto = new Produto();			   // Faz new para criar outro objeto (Receber todas as informa��es novas (Mantinha o id do primeiro produto consultado))
					produto.setId(item.getProduto().getId()); //Item mant�m (rastreabilidade) o idProduto e idPedido no BD
					produto = (Produto) proDAO.consultar(produto);					
					item.setIdPedido(pedido.getId());
					item.setProduto(produto);
					itens.add(item);							//Lista Itens: Recebe item
				}
				pedido.setItens(itens);
				itens = new ArrayList<Item>();
				pedidos.add(pedido);							//Lista Pedidos: Recebe pedido
			}
			cliente.setPedidos(pedidos);
			pedidos = new ArrayList<Pedido>();
			clientes.add(cliente);						
		} // For Clientes
		if(c == null){
			return clientes;
		}
		else{ // Se for buscar um pedido de um cliente espec�fico...
			for (Cliente listCli : clientes) {	// Itera entre os arrays de hist�rico (varre todos os pedidos)
				if(listCli.getId() == c.getId()){		// Compara o id do cliente do primeiro item do hist�rico com o id do cliente passado
					// Se for pedido do cliente...
					auxClientes.add(listCli);					
				}
			}
			return auxClientes;
		}
	}
	public abstract List<Cliente> factoryPedidoCliente(Cliente c);
}
