package br.com.nomedositeinvertido.negocio.factory.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;

public class PesquisarClientes extends AbstractFactory{

	@Override
	public List<Cliente> factoryPedidoCliente(Cliente c) {		
		return pesquisarTodos(c);
	}

}
