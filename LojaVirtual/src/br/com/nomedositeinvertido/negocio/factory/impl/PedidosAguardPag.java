package br.com.nomedositeinvertido.negocio.factory.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;

public class PedidosAguardPag extends AbstractFactory{

	@Override
	public List<Cliente> factoryPedidoCliente(Cliente c) {
		List<Cliente> listaClientes = pesquisarTodos(c); 
		List<Pedido> listaPedidos = new ArrayList<Pedido>();
		
		for (Cliente listaPedCli : listaClientes) { // Est� pesquisando todos ou um s� cliente
			for (Pedido pedido : listaPedCli.getPedidos()) { // Esta pesquisando os pedidos do(dos) cliente(s)
				if(pedido.getStatus().equals("Aguardando pagamento")){
					listaPedidos.add(pedido);
				}
			}
			listaPedCli.setPedidos(listaPedidos);
		}
		return listaClientes;
	}	

}
