package br.com.nomedositeinvertido.negocio.factory.impl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.AbstractPesquisar;
import br.com.nomedositeinvertido.teste.TesteSysoutClienteCompleto;

/**
 * 
 * @author Jonas
 *	@see Retorno: Historico
 *	@see Esta classe retorna o pedido atual com status 'Aberto' do cliente
 */
public class PedidoAbertoCliente extends AbstractFactory {
	@Override
	public List<Cliente> factoryPedidoCliente(Cliente c) {
		List<Cliente> listaClientes = pesquisarTodos(c); 	
		List<Pedido> listaPedidos = new ArrayList<Pedido>();
		
		for (Cliente listaPedCli : listaClientes) { // Est� pesquisando todos ou um s� cliente
			for (Pedido pedido : listaPedCli.getPedidos()) { // Esta pesquisando os pedidos do(dos) cliente(s)
				if(pedido.getStatus().equals("Aberto") || pedido.getStatus().equals("Em espera")){
					listaPedidos.add(pedido);
				}
			}
			listaPedCli.setPedidos(listaPedidos);
		}
		return listaClientes;
	}
}
