package br.com.nomedositeinvertido.negocio.factory;

import java.util.List;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public interface IFactory {

	public List<Cliente> factoryPedidoCliente(Cliente c);
	
}
