package br.com.nomedositeinvertido.negocio.decorator.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;

public class AdicionarRegiao extends AdicionarParametro{

	private String regiao;
	
	public AdicionarRegiao(IDecorator idecorator, String regiao) {
		super(idecorator);
		this.regiao = regiao;
	}

	@Override
	public List<Cliente> getConjunto() {
		
		List<Cliente> listClientes = new ArrayList<Cliente>();
		
		for (EntidadeDominio entCliente : this.idecorator.getConjunto()) { // Itera Clientes
			Cliente cliente = (Cliente) entCliente;
			if(regiao == null){
				listClientes.add(cliente);
			}else if(cliente.getEndereco().getEstado().equals(regiao)){
				listClientes.add(cliente);
			}
		}
		return listClientes;
	}

}
