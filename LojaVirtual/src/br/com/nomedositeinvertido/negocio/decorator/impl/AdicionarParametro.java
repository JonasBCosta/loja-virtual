package br.com.nomedositeinvertido.negocio.decorator.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;

public abstract class AdicionarParametro implements IDecorator{

	protected List<Cliente> entdoms = new ArrayList<Cliente>();
	protected IDecorator idecorator;
	
	public AdicionarParametro(IDecorator idecorator){
		this.idecorator = idecorator;
	}
	
	@Override
	public abstract List<Cliente> getConjunto();	
}
