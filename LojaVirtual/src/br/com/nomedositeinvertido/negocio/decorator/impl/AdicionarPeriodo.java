package br.com.nomedositeinvertido.negocio.decorator.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;
import br.com.nomedositeinvertido.teste.TesteSysoutClienteCompleto;

/**
 * Filtrar� somente para os pedidos que foram finalizados dentro do periodo passado
 */
public class AdicionarPeriodo extends AdicionarParametro{

    private Timestamp dataInic, dataTerm;

    public AdicionarPeriodo(IDecorator idecorator, Timestamp dataInic, Timestamp dataTerm) {
		super(idecorator);
		this.dataInic = dataInic;
		this.dataTerm = dataTerm;
	}
    
	@Override
	public List<Cliente> getConjunto() {
		List<Pedido> listPedidos = new ArrayList<Pedido>();
		List<Cliente> listClientes = new ArrayList<Cliente>();		
		for (Cliente entCliente : this.idecorator.getConjunto()) {
			for (EntidadeDominio entPedido : entCliente.getPedidos()) {
				Pedido pedido = (Pedido) entPedido;
				if(pedido.getStatus().equals("Finalizado")){ // Importante: Somente pedidos finalizados
					if(dataInic != null){
						if(pedido.getExpiracao().before(dataInic)){
							continue;
						}
					}if(dataTerm != null){
						if(pedido.getExpiracao().after(dataTerm)){
							continue;
						}
					}
					listPedidos.add(pedido);
				}
			}
			entCliente.getPedidos().clear();
			entCliente.setPedidos(listPedidos);
			listPedidos = new ArrayList<Pedido>();
			listClientes.add(entCliente);
		}
		return listClientes;
	}
}
