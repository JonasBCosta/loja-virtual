package br.com.nomedositeinvertido.negocio.decorator.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PesquisarClientes;
import br.com.nomedositeinvertido.teste.TesteSysoutClienteCompleto;

public class ConjuntoAnalise implements IDecorator{

	private IFactory factoryCliente = new PesquisarClientes();
	private List<Cliente> clientes = new ArrayList<Cliente>();
	private List<Cliente> entdoms = new ArrayList<Cliente>(); 
	
	public ConjuntoAnalise(){
		clientes = factoryCliente.factoryPedidoCliente(null);
		for (Cliente cliente : clientes) {
			entdoms.add(cliente);
		}		
	}
	
	@Override
	public List<Cliente> getConjunto() {
		return entdoms;
	}	
}
