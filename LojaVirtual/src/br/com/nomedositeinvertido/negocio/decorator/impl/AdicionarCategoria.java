package br.com.nomedositeinvertido.negocio.decorator.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.negocio.decorator.IDecorator;

public class AdicionarCategoria extends AdicionarParametro{

	private String categoria;
	
	public AdicionarCategoria(IDecorator idecorator, String categoria) {
		super(idecorator);
		this.categoria = categoria;
	}

	@Override
	public List<Cliente> getConjunto() {
		
		List<Item> listItens = new ArrayList<Item>();		
		List<Pedido> listPedidos = new ArrayList<Pedido>();
		List<Cliente> listClientes = new ArrayList<Cliente>();
		
		for (EntidadeDominio entCliente : this.idecorator.getConjunto()) { // Itera Clientes
			Cliente cliente = (Cliente) entCliente;
			for (EntidadeDominio entPedido : cliente.getPedidos()) { 
				Pedido pedido = (Pedido) entPedido;
				if(pedido.getStatus().equals("Finalizado")){ // Importante: Somente pedidos finalizados
					for (EntidadeDominio entItem : pedido.getItens()) {
						Item item = (Item) entItem;
						if(categoria == null){
							listItens.add(item);
						}else if(item.getProduto().getCategoria().equals(categoria)){ // Regra da classe: Somente itens com produto da categoria do parametro
							listItens.add(item);
						}
					}// Item
					pedido.getItens().clear();
					pedido.setItens(listItens);
					listItens = new ArrayList<Item>();	
					listPedidos.add(pedido);
				}
			}
			cliente.getPedidos().clear();
			cliente.setPedidos(listPedidos);
			listPedidos = new ArrayList<Pedido>();
			listClientes.add(cliente);
		}
		
		return listClientes;
	}

}
