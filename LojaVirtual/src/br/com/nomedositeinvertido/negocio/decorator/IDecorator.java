package br.com.nomedositeinvertido.negocio.decorator;

import java.util.List;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public interface IDecorator {

	public List<Cliente> getConjunto();
}
