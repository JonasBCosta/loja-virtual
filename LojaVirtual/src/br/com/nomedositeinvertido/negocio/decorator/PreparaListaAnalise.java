package br.com.nomedositeinvertido.negocio.decorator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.teste.TesteSysoutClienteCompleto;

public class PreparaListaAnalise {

	private int quantidade;
	private IDecorator idecorator;
	private EntidadeDominio entDom;	
	
	public PreparaListaAnalise(IDecorator idecorator, int quantidade, EntidadeDominio entDom){
		this.quantidade = quantidade;
		this.idecorator = idecorator;
		this.entDom = entDom;
	}
	
	public List<EntidadeDominio> ordenaMaisVendidos(){
		List<EntidadeDominio> entDoms = getLista();
		List<EntidadeDominio> auxEntDoms = new ArrayList<EntidadeDominio>();
		EntidadeDominio entDomMaior = new EntidadeDominio();
			
//		int cont = -1;
		int maior = 0;
		int contador = 0;
		int aux = 0;
		if(!entDoms.isEmpty() && entDoms.get(0) instanceof Produto){
			while(!entDoms.isEmpty()) {
				if(((Produto)entDoms.get(contador)).getVendidos() > maior){
					maior = ((Produto)entDoms.get(contador)).getVendidos();
					entDomMaior = entDoms.get(contador);
				}
				if(contador == (entDoms.size()-1)){    // Se o corrente � o ultimo da lista...
					auxEntDoms.add(entDomMaior); // O mais vendido vai para a lista
					entDoms.remove(entDomMaior); // Ele � removido, para o proximo loop encontrar o pr�ximo numero
					if(aux == entDoms.size()){
						break;
					}
					aux = entDoms.size();
					maior=0;				// o maior � zerado para a recontagem e encontrar o pr�ximo numero
					contador= -1;					// O loop ser� recontado.
				}
				contador++;
			}
			entDoms.clear();
			contador = 0;
			while(contador < quantidade && contador < auxEntDoms.size()){
				if(auxEntDoms.get(contador) != null){
					entDoms.add(auxEntDoms.get(contador));
				}
				contador++;
			}		
		}
		return entDoms;
	}
	
	private List<EntidadeDominio> getLista(){
		
		List<EntidadeDominio> listItens = new ArrayList<EntidadeDominio>();		
		List<EntidadeDominio> listPedidos = new ArrayList<EntidadeDominio>();
		List<EntidadeDominio> listProdutos = new ArrayList<EntidadeDominio>();
		List<EntidadeDominio> listClientes = new ArrayList<EntidadeDominio>();
		
		for (Cliente entCliente : this.idecorator.getConjunto()) { // Itera Clientes
			listClientes.add(entCliente);
			for (EntidadeDominio entPedido : entCliente.getPedidos()) {
				Pedido pedido = (Pedido) entPedido;
				for (EntidadeDominio entItem : pedido.getItens()) {
					Item item = (Item) entItem;
					listItens.add(item);
					listProdutos.add(item.getProduto());					
				}				
				listPedidos.add(pedido);
			}

		}
		if(entDom instanceof Cliente){
			return listClientes;
		}
		if(entDom instanceof Pedido){
			for (EntidadeDominio entDom : listPedidos) {				
			}
			return listPedidos;
		}else if(entDom instanceof Item){
			for (EntidadeDominio entDom : listItens) {
			}
			return listItens;
		}else if(entDom instanceof Produto){
			ArrayList<EntidadeDominio> produto2 = new ArrayList<EntidadeDominio>();
			ProdutoDAO prodDAO = new ProdutoDAO();
			produto2 = (ArrayList<EntidadeDominio>) prodDAO.consultarTodos(null);
			
			for(int i = 0; i < produto2.size(); i++){
				Produto produto = (Produto) produto2.get(i);
				for(int j = 0; j < produto2.size(); j++){
					if(i != j && produto.getId() == produto2.get(j).getId()){
						produto2.remove(i);
						i--;
					}
				}
			}
			return produto2;
		}
		return null;
	}	
}
