package br.com.nomedositeinvertido.negocio.strategy;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public interface IStrategy {

	public Object execute(EntidadeDominio entidade, Cliente cliente); // Assinatura de metodo mais gen�rico poss�vel, atendendo a todas necessidades de todas as classes Strategy
}
