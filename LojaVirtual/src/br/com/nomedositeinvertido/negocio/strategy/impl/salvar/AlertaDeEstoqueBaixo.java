package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.desktop.AnalisePeriodo;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.teste.Prototipo1Teste;

public class AlertaDeEstoqueBaixo extends AbstractSalvar{
	
	private List<Produto> listProds = new ArrayList<Produto>();
	private static AlertaDeEstoqueBaixo instance = new AlertaDeEstoqueBaixo();

	public static AlertaDeEstoqueBaixo getInstance(){
		return instance;
	}
	
	@Override
	public String salvar(EntidadeDominio entidade, Cliente cliente) {

		Produto produto = new Produto();
		produto = (Produto) entidade;
		ProdutoDAO prodDAO = new ProdutoDAO();
		List<EntidadeDominio> listProdusts = new ArrayList<EntidadeDominio>();
		listProdusts = prodDAO.consultarTodos(null);
		
		for (EntidadeDominio entidadeDominio : listProdusts) {
		produto = (Produto) entidadeDominio;
		
		
		if(produto.getQuantidade() <= produto.getEstqMinimo()){
			listProds.add(produto);
		}
		}
		Prototipo1Teste.alertaEstoqueBaixo(listProds);		
		
		return null;
	}
	public List<Produto> getListProds() {
		return listProds;
	}
	public void removeValorLista(Produto produto){
		listProds.remove(produto);
		return;
	}
}
