package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import java.util.HashMap;
import java.util.Map;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.IDAO;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.FornecedorDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class VerificacaoDuplicidade extends AbstractPesquisar{

	private Map<String, IDAO>   daos  = new HashMap<String, IDAO>();
	private Map<String, String> termo = new HashMap<String, String>();

	public VerificacaoDuplicidade (){			
		daos.put(Cliente.class.getName(), new ClienteDAO());
		termo.put(Cliente.class.getName(), "apelido");
		daos.put(Produto.class.getName(), new ProdutoDAO());
		termo.put(Produto.class.getName(), "nome");
		daos.put(Fornecedor.class.getName(), new FornecedorDAO());
		termo.put(Fornecedor.class.getName(), "nome");
	}
	
	public String pesquisar(EntidadeDominio entidade, Cliente cliente) {
		String nmClasse = entidade.getClass().getName();
		IDAO dao = daos.get(nmClasse);
		String nome = termo.get(nmClasse); 
		
		if(dao.verifDuplicidade(entidade)){ 
			return "Este " + nome + " ja est� sendo usado, escolha outro ou tente mudar alguma letra";
		}
		return null;
	}
}
