package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;

public abstract class AbstractPesquisar implements IStrategy{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {
		return pesquisar(entidade, cliente);
	}
	
	public abstract Object pesquisar(EntidadeDominio entidade, Cliente cliente);

}
