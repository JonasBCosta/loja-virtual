package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;

public abstract class AbstractSalvar  implements IStrategy{

	public Object execute(EntidadeDominio entidade, Cliente cliente) {
		return salvar(entidade, cliente);
	}
	
	public abstract String salvar(EntidadeDominio entidade, Cliente cliente);

}
