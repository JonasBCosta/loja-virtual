package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;

public class ValidarPedido extends AbstractSalvar{

	// s� cadastra um pedido
	public String salvar(EntidadeDominio entidade, Cliente cliente) {
		PedidoDAO pedDAO = new PedidoDAO();
		Pedido pedido = new Pedido();
		pedido.setIdCliente(cliente.getId());
		pedDAO.cadastrar(pedido);     // A rastreabilidade n�o pode ser feita vindo do cliente, deve ser feita desta maneira
		pedido = (Pedido) pedDAO.consultar(pedido); // consulta o pedido ja cadastrado
		
		return null;
	}
}
