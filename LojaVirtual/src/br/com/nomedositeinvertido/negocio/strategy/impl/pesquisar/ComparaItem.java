package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class ComparaItem extends AbstractPesquisar{

	public String pesquisar(EntidadeDominio entidade, Cliente cliente) {
		
		PedidoDAO pedDAO = new PedidoDAO();
		ItemDAO itemDAO = new ItemDAO();
		Pedido pedido = new Pedido();
		List<EntidadeDominio> itens = new ArrayList<EntidadeDominio>();
		Item item = new Item();
		Item itemAtualizado = (Item) entidade;
		pedido.setIdCliente(cliente.getId());
		pedido = (Pedido) pedDAO.consultar(pedido);
		item.setIdPedido(pedido.getId());
		
		itens = itemDAO.consultarTodos(item);
				
		for (EntidadeDominio iterItem : itens) {
			System.out.println("Teste 1");
			item = (Item) iterItem;
			System.out.println("Teste 2");
			if(item.getProduto().getId() == itemAtualizado.getProduto().getId()){
				System.out.println("Teste 3");
				item.setQuantidade(itemAtualizado.getQuantidade());
				System.out.println("Teste 4");
				itemDAO.alterar(item);
				System.out.println("Teste 5");
				return "A quantidade do produto foi atualizada";
			}
		}	
		return null;
	}

}
