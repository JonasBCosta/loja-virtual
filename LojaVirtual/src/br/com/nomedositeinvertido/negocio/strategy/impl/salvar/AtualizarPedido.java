package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import java.sql.Timestamp;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidoAbertoCliente;
import br.com.nomedositeinvertido.negocio.factory.impl.PesquisarClientes;
/**
 * 
 * @author Jonas
 * Metodo execute:
 * Parametros(entidade, cliente)
 * 1�- Recebe um cliente, � feito uma busca entre todos os pedidos para ver se
 * 		este cliente j� possui um pedido "Aberto", neste caso, o pedido ser�
 * 		passado para "Aguardando Pagamento", deduzindo que foi esta classe 
 * 		foi usada uma segunda vez quando o cliente se propos a pagar no PagSeguro.
 * 2�- Se o pedido estava como "Aguardando Pagamento", deduz-se de que esta classe
 * 		esta sendo usada ap�s o pagamento ter sido conclu�do.
 * 3�- Por fim, em qualquer situa��o, (cliente n�o possui pedido) (O pedido aberto
 * 		foi enviado ao PagSeguro) (O pedido aguardando pagamento foi conclu�do),
 * 		um novo pedido ser� criado para o cliente. 
 */

public class AtualizarPedido extends AbstractSalvar{
	
	public String salvar(EntidadeDominio entidade, Cliente cliente) {
		IFactory factoryCliente = new PesquisarClientes(); // Pesquisa geral 
		
		Pedido pedido = new Pedido();
		PedidoDAO pedDAO = new PedidoDAO();
		ClienteDAO cliDAO = new ClienteDAO();
	    
		java.sql.Timestamp dataExpira = new Timestamp(System.currentTimeMillis());
	    dataExpira.setDate(dataExpira.getDate() + 2);
	    
		// Testa para ver se o cliente j� pussui um pedido aberto
		// Este for passa o status do pedido (parametro) para o pr�ximo nivel 
		if(entidade instanceof Pedido){
			Cliente clienteEnt = factoryCliente.factoryPedidoCliente(cliente).get(0); // Passou cliente como par�metro, vai vir s�mente este cliente
			Pedido pedidoEnt = (Pedido) entidade;
			for (Pedido pedCli : clienteEnt.getPedidos()){
				if(pedCli.getId() == pedidoEnt.getId()){
					if(pedCli.getStatus().equals("Em espera")){
						pedCli.setStatus("Aberto");
						pedDAO.alterar(pedCli);
					}else if(pedCli.getStatus().equals("Aberto")){
						pedCli.setStatus("Aguardando pagamento");
						// C�DIGO: CADASTRAR NOVO PEDIDO!!
						pedCli.setIdCliente(pedidoEnt.getIdCliente()); // O cadastro exige o id do cliente
						pedCli.setExpiracao(dataExpira);			   // O pedido rece
						pedDAO.alterar(pedCli);		// Altera o pedido com novo Status (Mant�m composi��o no BD com os itens do pedido)
						abrirPedido(pedDAO, pedCli, clienteEnt, cliDAO);
						return "Novo pedido criado.";
					}else if(pedCli.getStatus().equals("Aguardando pagamento")){	
						pedCli.setStatus("Finalizado");
						pedDAO.alterar(pedCli);
					}
				}
			}
		}else if(entidade instanceof Cliente){
			Cliente clienteEnt = (Cliente) cliDAO.consultar(entidade);
			pedido.setIdCliente(clienteEnt.getId()); // O cadastro exige o id do cliente
//			cliente = cliDAO.consultar(entidade);
			abrirPedido(pedDAO, pedido, clienteEnt, cliDAO);
		}		
		return null;
	}
	
	private void abrirPedido(PedidoDAO pedDAO, Pedido pedido, Cliente cliente, ClienteDAO cliDAO){
		pedDAO.cadastrar(pedido);	// Cadastra novo pedido (Vai receber id novo e Status "Em espera")		
		pedido = (Pedido) pedDAO.consultar(pedido);
		// Atualiza o campo do pedido aberto(atual) do cliente
		cliente.setPedEmUso(pedido);
		cliDAO.alterar(cliente);
	}
}
