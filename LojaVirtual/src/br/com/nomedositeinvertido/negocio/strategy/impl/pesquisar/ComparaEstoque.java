package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import javax.servlet.http.HttpServletRequest;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class ComparaEstoque extends AbstractPesquisar{

	public String pesquisar(EntidadeDominio entidade, Cliente cliente){

		ItemDAO itemDAO = new ItemDAO(); 					// Cria o dao item
		ProdutoDAO prodDAO = new ProdutoDAO(); 				// Cria o dao produto
		Produto produto = new Produto();	   				// Cria o produto
		Item item, itemAux = new Item();		
		item = (Item) entidade;           	  			// Este � o item atualizado
		itemAux.setId(item.getId());
		itemAux = (Item) itemDAO.consultar(itemAux);  	// Este � o item antigo
		produto.setId(item.getProduto().getId());   			// salva o id do produto no objeto produto
		
		produto = (Produto) prodDAO.consultar(produto);		
		
		try{
			if((item.getQuantidade()-itemAux.getQuantidade()) > produto.getQuantidade()){ // verifica se tem a quantidade pedida no estoque
				return "Estoque insuficiente para o pedido";
			}
			if(item.getQuantidade() < 1){
				return "Insira um valor v�lido";
			}
		}catch(NumberFormatException e){
			return "Insira um n�mero";
		}
		return null;
	}
	
}
