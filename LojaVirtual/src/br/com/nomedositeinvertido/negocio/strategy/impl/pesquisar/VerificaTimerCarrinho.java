package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.AtualizarPedido;

/**
 * 
 * @author Jonas Barros da Costa
 *	Esta classe trabalha com o dois primeiros est�gios do pedido (Em espera e Aberto)
 *	Esta classe julga o pedido segundo sua data de Abertura e de expira��o
 *	As regras de neg�cio estabelecidas para esta classe s�o de expira��o do pedido
 *	ap�s 48 horas mantendo itens no pedido.
 *	Se o pedido ainda n�o foi aberto, ele ser� aberto e receber� a data de abertura e expira��o
 *	Se o pedido n�o possuir itens, ele ter� seu cronometro reiniciado.
 *	Se o pedido mantiver itens durante 48 horas, ele ser� exclu�do. 
 */
public class VerificaTimerCarrinho extends AbstractPesquisar {
    
	// Vai entrar um item como parametro
    public String pesquisar(EntidadeDominio entidade, Cliente cliente) {
    	PedidoDAO pedDAO = new PedidoDAO();	
    	ItemDAO itemDAO = new ItemDAO();
    	Item item = new Item();
    	List<EntidadeDominio> entList = new ArrayList<EntidadeDominio>();
    	Pedido pedido = new Pedido();
    	
    	pedido.setIdCliente(cliente.getId());
    	pedido = (Pedido) pedDAO.consultar(pedido);
		
    	IStrategy cmd = new AtualizarPedido();
    	    	
        java.sql.Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
        java.sql.Timestamp dataExpira = new Timestamp(System.currentTimeMillis());
        dataExpira.setDate(dataExpira.getDate() + 2);
        
        // Vai abrir o pedido e setar data de abertura e expira��o
        if(pedido.getStatus().equals("Em espera") && entidade != null){
        	pedido.setData(dataAtual); // Hor�rio de agora
        	pedido.setExpiracao(dataExpira); // Hor�rio de agora +2 dias
        	pedido.setStatus("Aberto");
        	pedDAO.alterar(pedido);
        // --------------------------------------------------------
        // Vai excluir o pedido que atingiu a data de expira��o
        } else if(pedido.getStatus().equals("Aberto") && dataAtual.after(pedido.getExpiracao())){
        	pedDAO.excluir(pedido);    	    		
        	cmd.execute(pedido, cliente);
        	return "O pedido expirou, um novo pedido foi criado";
        // --------------------------------------------------------
        // Vai reiniciar a data de expira��o e abertura se o pedido estiver sem itens
        } else if(pedido.getStatus().equals("Aberto")){ // O cliente j� abriu o pedido. Vai verificar se o pedido n�o possui itens
        	item.setIdPedido(pedido.getId());
        	entList = itemDAO.consultarTodos(item); // Busca todos os itens deste pedido
        	if(entList.isEmpty()){ // O pedido n�o possui itens
            	pedido.setData(dataAtual); // Hor�rio de agora
            	pedido.setExpiracao(dataExpira); // Hor�rio de agora +2 dias
            	pedDAO.alterar(pedido);
        	}
        } 
		return null;
	}
    
}
