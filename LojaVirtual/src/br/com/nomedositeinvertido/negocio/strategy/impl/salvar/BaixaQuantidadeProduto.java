package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.dominio.item.Lote;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemEstoqueDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.uol.pagseguro.domain.checkout.Checkout;

public class BaixaQuantidadeProduto extends AbstractSalvar{

	@Override
	public String salvar(EntidadeDominio entidade, Cliente cliente) {

		
		Checkout checkout = new Checkout();
		
		ProdutoDAO prodDAO = new ProdutoDAO();
		Produto produto = new Produto();
		ItemEstoqueDAO itemEstqDAO = new ItemEstoqueDAO();
		ItemEstoque itemEstq = new ItemEstoque();
		Lote lote = new Lote();
		List<EntidadeDominio> itensEstq = new ArrayList<EntidadeDominio>();
		int quantidade = 0;
		
		lote = (Lote) entidade;		
		itensEstq = itemEstqDAO.consultarTodos(null);
		for (EntidadeDominio entDomItemEstq : itensEstq) {
			if(((ItemEstoque)entDomItemEstq).getIdLote() == lote.getId()){
				quantidade++;	
				itemEstq = (ItemEstoque) entDomItemEstq;
			}
		}
		
		produto.setId(itemEstq.getIdProduto());
		produto = (Produto) prodDAO.consultar(produto);
		produto.setQuantidade(produto.getQuantidade() - quantidade);
		prodDAO.alterar(produto);
		
		return null;
	}

}
