package br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.cliente.Pessoa;

public class ValidacaoCPF extends AbstractPesquisar{
	
	Pessoa pes = new Pessoa();
	
    public String pesquisar(EntidadeDominio entidade, Cliente cliente) {
        Pessoa pes = (Pessoa)entidade;
        VerificacaoCPF c = new VerificacaoCPF(null);
        do{            
            if(pes.getCPF().length() == 11){ 
                for(int i=0; i<11; i++){ 
                	if(Character.isDigit(pes.getCPF().charAt(i))) 
                    	c.cpf[i] = Integer.parseInt(String.valueOf(pes.getCPF().charAt(i)));
                	else{
                		return "Use somente n�meros no CPF";  
                	}
                }               
                break;
            }
            else{
            	return "CPF deve conter 11 n�meros";	
            }
        }while(true);
        c.CalculaNumero();
        try {
            c.AutenticarCPF();
        } catch (VerificacaoCPF e){
        	return e.getMessage();
        }
        return null; 
    }
    
    private class VerificacaoCPF extends Exception {
        
        int cpf[] = new int[11];
        int aux , aux2;
        
            public void CalculaNumero(){
                           
                for(int i=0; i< 9; i++){
                    aux += cpf[i]*(10-i);}
                
                if(aux%11 < 2)  {  aux = 0;       }
                else            {  aux = aux%11;
                                   aux = 11-aux;}
                
                for(int i=0; i< 10; i++){
                    aux2 += cpf[i]*(11-i);}
                
                
                if(aux2%11 < 2){  aux2 = 0;        }
                else           {  aux2 = aux2%11;
                                  aux2 = 11-aux2;  }
            }
            
            public VerificacaoCPF (String validacao){
                super(validacao);
            }
            
            public int AutenticarCPF() throws VerificacaoCPF{
            	if(aux != cpf[9] || aux2 != cpf[10]){
                    // CPF Inválido                
                    throw new VerificacaoCPF("CPF inv�lido"); // envia a mensagem para o construtor passar a mensagem para a superclasse. N�o est� sendo utilizado!          
                }
                else{
                    // CPF Valido
                    throw new VerificacaoCPF(null); // envia a mensagem para o construtor passar a mensagem para a superclasse; N�o est� sendo utilizado!                
                }
            }
    }

}
