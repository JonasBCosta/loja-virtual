package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.jdbc.impl.ItemEstoqueDAO;

public class GravarItensEstoque extends AbstractSalvar{

	@Override
	public String salvar(EntidadeDominio entidade, Cliente cliente) {

		ItemEstoque itemEstq = (ItemEstoque) entidade;
		ItemEstoqueDAO itemEstqDAO = new ItemEstoqueDAO();
		String codigoProd = itemEstq.getCodigo();
		for(int i = 0; i < itemEstq.getQuantidade() ; i++){
			itemEstq.setCodigo(codigoProd + "-" + String.valueOf(i));
			itemEstqDAO.cadastrar(itemEstq);
		}
		
		return "gravado";
	}

}
