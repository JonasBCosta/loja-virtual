package br.com.nomedositeinvertido.negocio.strategy.impl.salvar;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemEstoqueDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class GravarQuantidadeProduto extends AbstractSalvar{

	@Override
	public String salvar(EntidadeDominio entidade, Cliente cliente) {
		
		ProdutoDAO prodDAO = new ProdutoDAO();
		Produto produto = new Produto();
		ItemEstoqueDAO itemEstqDAO = new ItemEstoqueDAO();
		ItemEstoque itemEstq = new ItemEstoque();
		List<EntidadeDominio> itensEstq = new ArrayList<EntidadeDominio>();
		int quantidade = 0;
		
		itemEstq = (ItemEstoque) entidade;
		itensEstq = itemEstqDAO.consultarTodos(null);
		quantidade = itemEstq.getQuantidade();
		produto.setId(itemEstq.getIdProduto());
		produto = (Produto) prodDAO.consultar(produto);
		
//		for (EntidadeDominio entDomItemEstq : itensEstq) {
//			if(itemEstq.getIdProduto() == ((ItemEstoque)entDomItemEstq).getIdProduto()){ // � altera��o de produto, logo � uma venda, 
//				produto.setQuantidade(produto.getQuantidade() + quantidade);
//				prodDAO.alterar(produto);
//			}
//		}
		
		produto.setQuantidade(produto.getQuantidade() + quantidade);
		prodDAO.alterar(produto);
		
		return null;
	}

}
