package br.com.nomedositeinvertido.teste.testeDecorator;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.teste.testeDecorator.impl.CoberturaExtraDupla;
import br.com.nomedositeinvertido.teste.testeDecorator.impl.CoberturaSemCusto;
import br.com.nomedositeinvertido.teste.testeDecorator.impl.Pizza;
import br.com.nomedositeinvertido.teste.testeDecorator.impl.UmaCoberturaExtra;

public class Principal {

	public static void main(String[] args) {

		  Pedido pizzaDaCasa = new Pizza("Pizza da Casa", 10);
		  System.out.println("SAIDA PRINC" + "pizzaDaCasa");
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getPreco());
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getLabel());
		  pizzaDaCasa = new UmaCoberturaExtra("Pepperoni", 4, pizzaDaCasa);
		  System.out.println("SAIDA PRINC" + "pizzaDaCasa");
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getPreco());
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getLabel());
		  pizzaDaCasa = new CoberturaExtraDupla("Mozzarella", 2, pizzaDaCasa);
		  System.out.println("SAIDA PRINC" + "pizzaDaCasa");
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getPreco());
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getLabel());
		  pizzaDaCasa = new CoberturaSemCusto("Pimenta", 2, pizzaDaCasa);
		  System.out.println("SAIDA PRINC" + "pizzaDaCasa");
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getPreco());
		  System.out.println("SAIDA PRINC" + pizzaDaCasa.getLabel());

	}

}
