package br.com.nomedositeinvertido.teste.testeDecorator.impl;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.teste.testeDecorator.Pedido;

public class Pizza implements Pedido{
	private String label;
	  private double preco;
	   
	  public Pizza(String label, double preco) {
	  this.label=label;
	  this.preco=preco;
	  }
	   
	  public double getPreco(){
		  System.out.println("getPre�o, Pizza");
	  return this.preco;
	  }
	   
	  public String getLabel(){
		  System.out.println("getLabel, Pizza");
	  return this.label;
	  }

}
