package br.com.nomedositeinvertido.teste.testeDecorator.impl;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.teste.testeDecorator.Pedido;

public abstract class Extra implements Pedido{

    protected Pedido pedido;
    protected String label;
    protected double preco;

    public Extra(String label, double preco, Pedido pedido) {
    	JOptionPane.showMessageDialog(null, "Extra, Construtor, Pedido\nLabel: " 
    						+ pedido.getLabel() + "\nPreco: " + pedido.getPreco());
    	this.label=label;
        this.preco=preco;
        this.pedido=pedido;
    }

    // O pre�o � delegado para a implementa��o concreta
    public abstract double getPreco();

    // Label default � fornecido
    public String getLabel() {
    	System.out.println("getLabel, Extra");
    	return pedido.getLabel()+", "+this.label;
    }


}
