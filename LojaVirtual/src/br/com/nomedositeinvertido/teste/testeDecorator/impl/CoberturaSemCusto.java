package br.com.nomedositeinvertido.teste.testeDecorator.impl;

import br.com.nomedositeinvertido.teste.testeDecorator.Pedido;

public class CoberturaSemCusto extends Extra{
	   
    public CoberturaSemCusto(String label, double preco, Pedido pedido) {
              super(label, preco, pedido);
    }

    public double getPreco() {
    	System.out.println("getPre�o, CobSCus");
              return pedido.getPreco();
    }

}
