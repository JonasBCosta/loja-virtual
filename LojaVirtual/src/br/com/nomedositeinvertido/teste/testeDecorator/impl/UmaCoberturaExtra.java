package br.com.nomedositeinvertido.teste.testeDecorator.impl;

import br.com.nomedositeinvertido.teste.testeDecorator.Pedido;

public class UmaCoberturaExtra extends Extra{
	   
    public UmaCoberturaExtra(String label, double preco, Pedido pedido) {
              super(label, preco, pedido);
    }

    public double getPreco() {
    	System.out.println("getPre�o, UCobExt");
              return this.preco+pedido.getPreco();
    }

}
