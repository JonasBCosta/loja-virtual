package br.com.nomedositeinvertido.teste.testeDecorator.impl;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.teste.testeDecorator.Pedido;

public class CoberturaExtraDupla extends Extra{
	   
    public CoberturaExtraDupla(String label, double preco, Pedido pedido) {
              super(label, preco, pedido);
    }

    public double getPreco() {
    	System.out.println("getPre�o, CobExt");
              return (this.preco*2)+pedido.getPreco();
    }

    public String getLabel() {
    	System.out.println("getLabel, CobExt");
              return pedido.getLabel()+ ", Dupla " + this.label;
    }

}
