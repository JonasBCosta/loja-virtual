package br.com.nomedositeinvertido.teste.testeDecorator;

public interface Pedido {
    public double getPreco();
    public String getLabel();

}
