package br.com.nomedositeinvertido.teste;
import java.awt.HeadlessException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.JInternalFrame;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
 
public class Graficos extends JInternalFrame implements Observer {
 
	private static final long serialVersionUID = 1L;
 
	public Graficos(String applicationTitle, String chartTitle, List<EntidadeDominio> entdoms) {
		super(applicationTitle);										// Seta o titulo da janela
		PieDataset origemDados = carregarDados(entdoms);				// Busca os Dados para o gr�fico
		JFreeChart grafico = criarGrafico(origemDados, chartTitle);		// Cria o modelo do gr�fico
		ChartPanel chartPanel = new ChartPanel(grafico);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		setContentPane(chartPanel);
	}
 
	/**
	 * Carregamento dos dados
	 */
	private PieDataset carregarDados(List<EntidadeDominio> entdoms) {
		// Gera��o de dados aleat�rios

		DefaultPieDataset result = new DefaultPieDataset();
		int total = 0;
		List<Double> porcentagem = new ArrayList<Double>();
		
		if(!entdoms.isEmpty()){
			///// Gr�fico AnaliseProduto ---------------------------------------------------------------------------
			if(entdoms.get(0) instanceof Produto){
				for (EntidadeDominio entProduto : entdoms) {
					// Gera o total de produtos vendidos
					total += ((Produto) entProduto).getVendidos();
				}
				// Gera um vetor contendo a participa��o (porcentagem) de cada produto
				for (int i = 0; i < entdoms.size(); i++) {
					if(((Produto) entdoms.get(i)).getVendidos() != 0 && total != 0){	
						porcentagem.add(i, ((Double.parseDouble(String.valueOf(((Produto) entdoms.get(i)).getVendidos()))) / (Double.parseDouble(String.valueOf(total)))) * 100);
					}else{
						porcentagem.add(i, 0.0);
					}
				}					
				// monta o dataset result com o nome e a participa��o (porcentagem) de cada produto
				for (int i = 0; i < entdoms.size(); i++) {	
					result.setValue(((Produto) entdoms.get(i)).getNome() + " " + new BigDecimal(porcentagem.get(i)).setScale(1, RoundingMode.HALF_EVEN) + "%" , porcentagem.get(i));
				}
			}
		
			///// Gr�fico AnaliseRegi�o ----------------------------------------------------------------------------
			if(entdoms.get(0) instanceof Cliente){				
				HashMap<String, HashMap<Produto, Integer>> hashRegProd = preparaHashMap(entdoms);
				Set<String> regioes = hashRegProd.keySet();
				Set<Produto> produtos = null;
				int indice = 0;
				int totalReg = 0;
				if(!hashRegProd.isEmpty() && !regioes.isEmpty()){
					for (String regiao: regioes) {
						produtos = hashRegProd.get(regiao).keySet();
						StringBuilder sb = new StringBuilder();
						for (Produto produto : produtos) {
							total += hashRegProd.get(regiao).get(produto);
						}
					}
					for (String regiao: regioes) {
						totalReg = 0;
						produtos = hashRegProd.get(regiao).keySet();
						for (Produto produto : produtos) {
							totalReg += Double.parseDouble(String.valueOf(hashRegProd.get(regiao).get(produto)));
						}
						if(totalReg != 0 && total != 0){
							porcentagem.add(indice, (totalReg / Double.parseDouble(String.valueOf(total))) * 100);
						}else{
							porcentagem.add(indice, 0.0);
						}
						indice++;
					}	
					indice = 0;
					for (String regiao: regioes) {
						result.setValue(regiao + " " + new BigDecimal(porcentagem.get(indice)).setScale(1, RoundingMode.HALF_EVEN) + "%" , porcentagem.get(indice));
						indice++;
					}
				}
			}
			return result;
		}// Fim if entdoms n�o vazio
		return null;
	}
 
	/**
	 * Cria��o do gr�fico
	 * */

	private JFreeChart criarGrafico(PieDataset origemDados, String titulo) {
		// Vai escolher o design de grafico do tipo pizza
		JFreeChart chart = ChartFactory.createPieChart3D(titulo, origemDados, true, true, false);
		// Vai aprimorar o desing de grafico pizza para grafico pizza 3D
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}
	
	private HashMap<String, HashMap<Produto, Integer>> preparaHashMap(List<EntidadeDominio> listCli){
		HashMap<Produto, Integer> hashProdQtdd = new HashMap<Produto, Integer>();
		HashMap<String, HashMap<Produto, Integer>> hashRegProd = new HashMap<String, HashMap<Produto, Integer>>();
		ProdutoDAO prodDAO = new ProdutoDAO();
		List<Cliente> clientes = new ArrayList<Cliente>();
		List<Produto> produtos = new ArrayList<Produto>();
		List<EntidadeDominio> entProds = prodDAO.consultarTodos(null);
		Produto produto;
		Produto prodAux = new Produto();
		Item item;
		int indiceItem = 0;
		for (EntidadeDominio entidadeDominio : entProds) {
			produtos.add((Produto)entidadeDominio);
		}		
		for(int i = 0; i < 27; i++){
			for (int indCli = 0; indCli < listCli.size();) {
				Cliente cliente = (Cliente) listCli.get(indCli);
				if(clientes.isEmpty()){
					clientes.add(cliente);
					listCli.remove(cliente);
					indCli--;
				// Vai comparar o estado com o primeiro da lista clientes 
				//(PEGA UM CLIENTE, OS PROXIMOS SER�O COLOCADOS JUNTO COM 
				// ELE CASO PERTEN�A AO MESMO ESTADO, POR FIM, ESSES CLIENTES 
				// S�O REMOVIDOS DA LISTA PARA O PROCESSO SER FEITO NOVAMENTE 
				// COM OUTRO CLIENTE DE OUTRO ESTADO)
				}else if(clientes.get(0).getEndereco().getEstado().equals(cliente.getEndereco().getEstado())){
					clientes.add(cliente);
					listCli.remove(cliente);
					indCli--;
				}
				indCli++;
			}
			/** Temos a lista de clientes de uma regi�o... */
			for (Cliente regCli : clientes) { // Itera na lista de clientes de uma regi�o
				while(!regCli.getPedidos().isEmpty()){ // Itera na lista pedidos enquanto n�o ficar vazia(cada pedido especifico encontrado � removido)
					indiceItem = 0;			
					while(!regCli.getPedidos().get(0).getItens().isEmpty()){ // Itera na lista itens enquanto n�o ficar vazia(cada item especifico encontrado � removido)
						item = regCli.getPedidos().get(0).getItens().get(indiceItem);
						produto = item.getProduto();						
						// PROBLEMA: O hashMap s� compara os hashCodes dos objetos. Objetos iguais mas de instancias diferentes possuem hashCode diferente. SOLU��O...
						// Itera entre as keys do hashProdQtdd para verificar se j� existe o produto no hashMap
						Iterator<Produto> keyProdIterator = hashProdQtdd.keySet().iterator();						
						while(keyProdIterator.hasNext()){
							prodAux = keyProdIterator.next();
							if(prodAux.getId() == produto.getId()){ // Se o HashMap ja possui o item corrente...
								for (Produto prod : produtos) {
									if(produto.getId() == prod.getId()){
										hashProdQtdd.replace(prod, hashProdQtdd.get(prod)+item.getQuantidade()); // Apenas acrecenta a quantidade(item) ao produto
										regCli.getPedidos().get(0).getItens().remove(indiceItem); // Remove o item corrente da lista.
										indiceItem--; // Quando se remove um item da lista, os objetos subsequentes tomam a posi��o do antecedente
									}
								}
							}
							if(!keyProdIterator.hasNext() && regCli.getPedidos().get(0).getItens().size() == 0){
								produto = null;
							}
						}
						if(!keyProdIterator.hasNext() && produto != null){
							for (Produto prod : produtos) {
								if(produto.getId() == prod.getId()){
									hashProdQtdd.put(prod, item.getQuantidade()); // Guarda o produto no hashMap e sua quantidade(Item)
									regCli.getPedidos().get(0).getItens().remove(indiceItem);
									indiceItem--;
								}
							}
						}
						indiceItem++;
					}// while lista Itens
					regCli.getPedidos().remove(0);
				}// while lista Produtos
			}// Fim do la�o regCLi
			/** Temos os produtos e sua somat�ria total de vendas (de todos pedidos de todos os clientes) de uma regi�o... */
			if(!clientes.isEmpty()){
				hashRegProd.put(clientes.get(0).getEndereco().getEstado(), hashProdQtdd);
				hashProdQtdd = new HashMap<Produto, Integer>();
				clientes = new ArrayList<Cliente>();
			}
		}// Fim do la�o principal
		return hashRegProd;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
	
}
