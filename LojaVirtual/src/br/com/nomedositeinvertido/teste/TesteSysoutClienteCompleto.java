package br.com.nomedositeinvertido.teste;

import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;

public class TesteSysoutClienteCompleto {

	public void testeClienteAtribs(Cliente cliente){
		System.out.println("CLIENTE");
		System.out.println(" getCelular " + cliente.getCelular()); 
		System.out.println(" getCPF " + cliente.getCPF()); 
		System.out.println(" getDataNascimento " + cliente.getDataNascimento()); 
		System.out.println(" getId " + cliente.getId()); 
		System.out.println(" getLogin " + cliente.getLogin()); 
		System.out.println(" getNome " + cliente.getNome()); 
		System.out.println(" getSenha " + cliente.getSenha()); 
		System.out.println(" getStatusValida��o " + cliente.getStatusValida��o()); 
		System.out.println(" getTelefone " + cliente.getTelefone()); 
		System.out.println(" getData " + cliente.getData()); 
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getId());
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getIdCliente());
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getStatus());
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getData());
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getExpiracao());
		System.out.println(" getPedEmUso " + cliente.getPedEmUso().getItens());				
		System.out.println(" getBairro " + cliente.getEndereco().getBairro()); 
		System.out.println(" getCep " + cliente.getEndereco().getCep()); 
		System.out.println(" getCidade " + cliente.getEndereco().getCidade()); 
		System.out.println(" getComplemento " + cliente.getEndereco().getComplemento()); 
		System.out.println(" getEstado " + cliente.getEndereco().getEstado()); 
		System.out.println(" getNumero " + cliente.getEndereco().getNumero()); 
		System.out.println(" getRua " + cliente.getEndereco().getRua());
		for (Pedido pedido : cliente.getPedidos()) {
			System.out.println("PEDIDO");
			System.out.println(" getId " + pedido.getId());
			System.out.println(" getIdCliente " + pedido.getIdCliente());
			System.out.println(" getStatus " + pedido.getStatus());
			System.out.println(" getData " + pedido.getData());
			System.out.println(" getExpiracao " + pedido.getExpiracao());
			for (Item item : pedido.getItens()) {
				System.out.println("ITEM");
				System.out.println(" getId " + item.getId());
				System.out.println(" getIdPedido " + item.getIdPedido());
				System.out.println(" getQuantidade " + item.getQuantidade());
				System.out.println(" getData " + item.getData());
				System.out.println("PRODUTO");
				System.out.println(" getCategoria " + item.getProduto().getCategoria());
				System.out.println(" getDescricao " + item.getProduto().getDescricao());
				System.out.println(" getEnderecoImagem " + item.getProduto().getEnderecoImagem());
				System.out.println(" getId " + item.getProduto().getId());
				System.out.println(" getNome " + item.getProduto().getNome());
				System.out.println(" getQuantidade " + item.getProduto().getQuantidade());
				System.out.println(" getSrcWebCont " + item.getProduto().getSrcWebCont());
				System.out.println(" getVendidos " + item.getProduto().getVendidos());
				System.out.println(" getData " + item.getProduto().getData());
				System.out.println(" getImagem " + item.getProduto().getImagem());
				System.out.println(" getPreco " + item.getProduto().getPreco());
			}	
		}
	}
}
