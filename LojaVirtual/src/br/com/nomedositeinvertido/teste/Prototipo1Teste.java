package br.com.nomedositeinvertido.teste;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.nomedositeinvertido.desktop.AnalisePeriodo;
import br.com.nomedositeinvertido.desktop.AnaliseRegiao;
import br.com.nomedositeinvertido.desktop.CadastroProduto;
import br.com.nomedositeinvertido.desktop.ConfigurarLoja;
import br.com.nomedositeinvertido.desktop.TabelaCategorias;
import br.com.nomedositeinvertido.desktop.TabelaClientes;
import br.com.nomedositeinvertido.desktop.TabelaProdutos;
import br.com.nomedositeinvertido.desktop.TelaHistorico;
import br.com.nomedositeinvertido.desktop.logistica.ControleEstoque;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.dominio.produto.RedimensionaImagem;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.AlertaDeEstoqueBaixo;

public class Prototipo1Teste extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {		
//		data();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Prototipo1Teste frame = new Prototipo1Teste();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static public ConfigurarLoja interFrameConfLoja;
	static public TabelaCategorias interFrameTabCateg;
	static public CadastroProduto interFrameCadProd;
	static public TabelaProdutos interFrameTabProd;
	static public TabelaClientes interFrameTabCli;
	static public TelaHistorico  interFrameTabVen;	
	static public JInternalFrame interFrameBemVindo;
	static public JDesktopPane desktopPane;
	static public AnalisePeriodo interFrameAnlsProd;
	static public AnaliseRegiao interFrameAnlsReg;
	static public ControleEstoque interFrameAmbLog;
	private JPanelComBackground painelBackground;
	private JMenuItem subMnEditProd; // Est� aqui somente por causa do listener e o setEnabled
	private static Icon imagem = null;
	private static JLabel lblImagem = null;
	private static RedimensionaImagem redim = new RedimensionaImagem();

	
	public Prototipo1Teste() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1225, 730);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel painelAbsoluto = new JPanel();
		painelAbsoluto.setBounds(0, 0, 1209, 701);
		contentPane.add(painelAbsoluto);
		painelAbsoluto.setLayout(null);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.BLACK);
		desktopPane.setBounds(0, 0, 1213, 701);
		desktopPane.setLayout(null);
		painelAbsoluto.add(desktopPane);
		
// ------------ Barra de Menu ---------------------------------------------------------------
		JMenuBar barraMenu = new JMenuBar();
		barraMenu.setBounds(0, 0, 1213, 37);
		desktopPane.add(barraMenu);
		
		JMenu abaGest�o = new JMenu("Gest�o");
		barraMenu.add(abaGest�o);
		
		JMenu abaAnalise = new JMenu("An�lise");
		barraMenu.add(abaAnalise);				

		JMenu abaLogistica = new JMenu("Log�stica");
		barraMenu.add(abaLogistica);
		
//		JMenu abaConfiguracao = new JMenu("Configura��o");
//		barraMenu.add(abaConfiguracao);		
		// -- An�lise Vendas ------------------------------------
		JMenu menuVerHist = new JMenu("Verificar Historico");
		abaAnalise.add(menuVerHist);
		
		JMenuItem subMnVenRea = new JMenuItem("Vendas realizadas");
		subMnVenRea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarTabHistorico();
			}
		});
		menuVerHist.add(subMnVenRea);
		
		JMenuItem subMnAnlsProd = new JMenuItem("Vendas por periodo");
		subMnAnlsProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarAnalsProd();
			}
		});
		menuVerHist.add(subMnAnlsProd);
	
		JMenuItem subMnAnlsReg = new JMenuItem("Vendas por regi�o");
		subMnAnlsReg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarAnalsReg();
			}
		});
		menuVerHist.add(subMnAnlsReg);
		
		// -- Gerenciar Produtos ----------------------------------

		JMenu menuGerProd = new JMenu("Gerenciar Produtos");
		abaGest�o.add(menuGerProd);
		
		subMnEditProd = new JMenuItem("Editar produto");
		subMnEditProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarEditProd();
				mostrarTabProduto();
			}
		});
		menuGerProd.add(subMnEditProd);
		subMnEditProd.setEnabled(false);
		
		JMenuItem subMnCadNov = new JMenuItem("Cadastrar novo");
		subMnCadNov.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarCadProduto();
				mostrarTabProduto();
			}
		});
		menuGerProd.add(subMnCadNov);
		
		JMenuItem subMnTabProd = new JMenuItem("Tabela de produtos");
		subMnTabProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarTabProduto();
			}
		});
		menuGerProd.add(subMnTabProd);
		
		// -- Gerenciar Clientes ---------------------------------
		JMenu menuGerCli = new JMenu("Gerenciar Clientes");
		abaGest�o.add(menuGerCli);
		
		JMenuItem subMnTabCli = new JMenuItem("Tabela de clientes");
		subMnTabCli.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarTabClientes();
			}
		});
		menuGerCli.add(subMnTabCli);
		
		// -- Log�stica -----------------------------------------
		JMenu menuGerFor = new JMenu("Log�stica");
		abaLogistica.add(menuGerFor);
		
		JMenuItem subMnAmbLog = new JMenuItem("Ambiente de log�stica");
		subMnAmbLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esconderJanelas();
				mostrarAmbLog();
			}
		});
		menuGerFor.add(subMnAmbLog);
		// -- Configura��o -----------------------------------------
//		JMenu menuConfig = new JMenu("Configura��o");
//		abaConfiguracao.add(menuConfig);
			
//		JMenuItem subMnConfLoja = new JMenuItem("Ambiente de configura��o");
//		subMnConfLoja.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				esconderJanelas();
//				mostrarConfLoja();
//				mostrarTabCategorias();
//			}
//		});
//		menuConfig.add(subMnConfLoja);
		// ------ Listener Menu ---------------------------------
		abaGest�o.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(TabelaProdutos.rowAux >= 0){
					subMnEditProd.setEnabled(true);
				}
				else{
					subMnEditProd.setEnabled(false);
				}
			}
		});
// -------------------------------------------------------------------------------------------------------
		
// /////////// Internal Frames - Tela Inicial ///////////////////////////////////////////////////////////////////////////
		// IMPLEMENTAR LOGIN DE INICIO!!! /////////////////////////////////////////
		interFrameBemVindo = new JInternalFrame("New JInternalFrame");
		interFrameBemVindo.setBounds(-10, -30, 1224, 733);
		desktopPane.add(interFrameBemVindo);
		interFrameBemVindo.getContentPane().setLayout(null);		
		painelBackground = new JPanelComBackground("ImagensGUI/LogoMenuAdm.png");
		painelBackground.setSize(1224,733);
		interFrameBemVindo.getContentPane().add(painelBackground);
		
		interFrameBemVindo.addMouseListener( new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				interFrameBemVindo.dispose();
				AlertaDeEstoqueBaixo.getInstance().salvar(null, null);
				alertaEstoqueBaixo(AlertaDeEstoqueBaixo.getInstance().getListProds());
			}
		});
		interFrameBemVindo.setVisible(true);
	}
	
///////////// Internal Frames - A��o Menu ///////////////////////////////////////////////////////////////////////////
	
	public static void mostrarTabProduto(){
		if(interFrameTabProd != null)
			desktopPane.remove(interFrameTabProd);
		interFrameTabProd = TabelaProdutos.getInstance();
		interFrameTabProd.constroiTela();
		desktopPane.add(interFrameTabProd);
		interFrameTabProd.setVisible(true);
	}
	private static void esconderTabProduto(){
		if(interFrameTabProd != null){
			interFrameTabProd.setVisible(false);
			TabelaProdutos.rowAux = -1;
		}
	}
	
	public static void mostrarCadProduto(){
		if(interFrameCadProd != null)
			desktopPane.remove(interFrameCadProd);
		interFrameCadProd = CadastroProduto.getInstance();
		interFrameCadProd.constroiTela("CADASTRAR");
		desktopPane.add(interFrameCadProd);
		interFrameCadProd.setVisible(true);
	}
	private static void esconderCadProduto(){
		if(interFrameCadProd != null)
			interFrameCadProd.setVisible(false);
	}
		
	public static void mostrarEditProd(){
		if(TabelaProdutos.rowAux  < 0)
			return;
		if(interFrameCadProd != null)
			desktopPane.remove(interFrameCadProd);
		interFrameCadProd = CadastroProduto.getInstance();
		interFrameCadProd.constroiTela("EDITAR");
		desktopPane.add(interFrameCadProd);			
	}
	private static void esconderEditProd(){
		if(interFrameCadProd != null)
			interFrameCadProd.setVisible(false);
	}
	
	private static void mostrarTabClientes(){
		interFrameTabCli = new TabelaClientes();
		interFrameTabCli.setBounds(10, 80, 1193, 600);
		interFrameTabCli.addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				interFrameTabCli.setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		desktopPane.add(interFrameTabCli);
		interFrameTabCli.constroiTela();
		interFrameTabCli.setVisible(true);
	}
	private static void esconderTabClientes(){
		if(interFrameTabCli != null)
			interFrameTabCli.setVisible(false);
	}
	
	private static void mostrarTabHistorico(){
		interFrameTabVen = new TelaHistorico();
		interFrameTabVen.setBounds(10, 80, 1193, 600);
		interFrameTabVen.addComponentListener( new ComponentListener() {			
			public void componentMoved(ComponentEvent e) {
				interFrameTabVen.setLocation(10, 80);
			}
			public void componentHidden(ComponentEvent e) {}
			public void componentResized(ComponentEvent e) {}
			public void componentShown(ComponentEvent e) {}
		} );
		desktopPane.add(interFrameTabVen);
		interFrameTabVen.exibir();
		interFrameTabVen.setVisible(true);
	}
	private static void esconderTabHistorico(){
		if(interFrameTabVen != null)
			interFrameTabVen.setVisible(false);
	}
	
	private static void mostrarAnalsProd(){
		if(interFrameAnlsProd != null)
			desktopPane.remove(interFrameAnlsProd);
		interFrameAnlsProd = AnalisePeriodo.getInstance();
		desktopPane.add(interFrameAnlsProd);
		interFrameAnlsProd.exibir();
	}
	private static void esconderAnalsProd(){
		if(interFrameAnlsProd != null)
			interFrameAnlsProd.setVisible(false);
	}
	
	private static void mostrarAnalsReg(){
		if(interFrameAnlsReg != null)
			desktopPane.remove(interFrameAnlsReg);
		interFrameAnlsReg = AnaliseRegiao.getInstance();
		desktopPane.add(interFrameAnlsReg);
		interFrameAnlsReg.exibir();
	}
	private static void esconderAnalsReg(){
		if(interFrameAnlsReg != null)
			interFrameAnlsReg.setVisible(false);
	}
	
	private static void mostrarAmbLog(){
		if(interFrameAmbLog != null)
			desktopPane.remove(interFrameAmbLog);
		interFrameAmbLog = ControleEstoque.getInstance();
		desktopPane.add(interFrameAmbLog);
		interFrameAmbLog.exibir();
		interFrameAmbLog.setVisible(true);
	}
	private static void esconderAmbLog(){
		if(interFrameAmbLog != null)
			interFrameAmbLog.setVisible(false);
	}

	public static void mostrarConfLoja(){
		if(interFrameConfLoja != null)
			desktopPane.remove(interFrameConfLoja);
		interFrameConfLoja = ConfigurarLoja.getInstance();
		desktopPane.add(interFrameConfLoja);
		interFrameConfLoja.exibir("CADASTRAR");
		interFrameConfLoja.setVisible(true);
	}
	public static void esconderConfLoja(){
		if(interFrameConfLoja != null)
			interFrameConfLoja.setVisible(false);
	}
	public static void mostrarTabCategorias(){
		if(interFrameTabCateg != null)
			desktopPane.remove(interFrameTabCateg);
		interFrameTabCateg = TabelaCategorias.getInstance();
		interFrameTabCateg.constroiTela();
		desktopPane.add(interFrameTabCateg);
		interFrameTabCateg.setVisible(true);
	}
	private static void esconderTabCategorias(){
		if(interFrameTabCateg != null){
			interFrameTabCateg.setVisible(false);
			TabelaCategorias.rowAux = -1;
		}
	}
	
	private static void esconderJanelas(){
		esconderTabClientes();
		esconderTabHistorico();
		esconderTabProduto();
		esconderEditProd();
		esconderCadProduto();
		esconderAnalsReg();
		esconderAmbLog();
		esconderAnalsProd();
		esconderConfLoja();
		esconderTabCategorias();
	}
	
	public static void alertaEstoqueBaixo(List<Produto> produtos){
		AlertaDeEstoqueBaixo alertaEstqB = AlertaDeEstoqueBaixo.getInstance();		
		for (int i = 0; i < produtos.size(); i++) {
			imagem = new ImageIcon(produtos.get(i).getImagem());
			imagem = redim.resizeProporcional(0,0,new ImageIcon(produtos.get(i).getImagem())); // Redimensiona sempre pra 221/176
			lblImagem = new JLabel(imagem, JLabel.CENTER);
			if(JOptionPane.showConfirmDialog(null, "O produto: " + produtos.get(i).getNome() + "\nest� com o estoque em \nn�vel cr�tico."
					+ "\nDeseja parar de receber este \naviso para este produto?", 
					"titulo da caixa", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, imagem) == JOptionPane.YES_OPTION){
				alertaEstqB.removeValorLista(produtos.get(i));
				i--;
			};
		}
	}
//    public static void data() {
//        java.sql.Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
//        java.sql.Date date = new java.sql.Date(timeStamp.getTime()); 
//        java.sql.Time time = new java.sql.Time(timeStamp.getTime());
//    }
}

