//package br.com.nomedositeinvertido.teste;
//
//import java.awt.BorderLayout;
//import java.awt.EventQueue;
//
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
//import javax.swing.JInternalFrame;
//import javax.swing.JButton;
//import javax.swing.JLabel;
//import java.awt.Font;
//import javax.swing.SwingConstants;
//import java.awt.Color;
//import javax.swing.JDesktopPane;
//import javax.swing.JMenuBar;
//import javax.swing.JMenu;
//import javax.swing.JMenuItem;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
//
//public class Prototipo1 extends JFrame {
//
//	private JPanel contentPane;
//
//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Prototipo1 frame = new Prototipo1();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * Create the frame.
//	 */
//	public Prototipo1() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 1225, 740);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//		
//		JPanel painelAbsoluto = new JPanel();
//		painelAbsoluto.setBounds(0, 0, 1209, 701);
//		contentPane.add(painelAbsoluto);
//		painelAbsoluto.setLayout(null);
//		
//		JDesktopPane desktopPane = new JDesktopPane();
//		desktopPane.setBackground(Color.BLACK);
//		desktopPane.setBounds(0, 0, 1213, 701);
//		painelAbsoluto.add(desktopPane);
//		
//		JInternalFrame interFrameBemVindo = new JInternalFrame("New JInternalFrame");
//		interFrameBemVindo.setBounds(-11, -32, 1224, 733);
//		desktopPane.add(interFrameBemVindo);
//		interFrameBemVindo.getContentPane().setLayout(null);
//		
//		JButton btnComešar = new JButton("Come\u00E7ar");
//		btnComešar.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				
//			}
//		});
//		btnComešar.setForeground(Color.BLUE);
//		btnComešar.setFont(new Font("Gill Sans MT Ext Condensed Bold", Font.PLAIN, 30));
//		btnComešar.setBounds(480, 250, 200, 200);
//		interFrameBemVindo.getContentPane().add(btnComešar);
//		interFrameBemVindo.setVisible(true);
//		
//		JMenuBar barraMenu = new JMenuBar();
//		barraMenu.setBounds(0, 0, 205, 37);
//		desktopPane.add(barraMenu);
//		
//		JMenu abaJanelas = new JMenu("Janelas");
//		barraMenu.add(abaJanelas);
//		
//		JMenu menuVerHist = new JMenu("Verificar Historico");
//		abaJanelas.add(menuVerHist);
//		
//		JMenuItem subMnVenRea = new JMenuItem("Vendas realizadas");
//		menuVerHist.add(subMnVenRea);
//		
//		JMenu menuGerProd = new JMenu("Gerenciar Produtos");
//		abaJanelas.add(menuGerProd);
//		
//		JMenuItem subMnCadNov = new JMenuItem("Cadastrar novo");
//		menuGerProd.add(subMnCadNov);
//		
//		JMenuItem subMnTabProd = new JMenuItem("Tabela de produtos");
//		menuGerProd.add(subMnTabProd);
//		
//		JMenu menuGerCli = new JMenu("Gerenciar Clientes");
//		abaJanelas.add(menuGerCli);
//		
//		JMenuItem subMnTabCli = new JMenuItem("Tabela de clientes");
//		menuGerCli.add(subMnTabCli);
//		
//		final JInternalFrame interFrameCadProd = new JInternalFrame("New JInternalFrame");
//		interFrameCadProd.setBounds(10, 90, 250, 600);
//		desktopPane.add(interFrameCadProd);
//		
//		final JInternalFrame interFrameTabProd = new JInternalFrame("New JInternalFrame");
//		interFrameTabProd.setBounds(273, 90, 930, 600);
//		desktopPane.add(interFrameTabProd);
//		
//		JInternalFrame interFrameTabCli = new JInternalFrame("New JInternalFrame");
//		interFrameTabCli.setBounds(10, 90, 1193, 600);
//		desktopPane.add(interFrameTabCli);
//		
//		JInternalFrame interFrameTabVen = new JInternalFrame("New JInternalFrame");
//		interFrameTabVen.setBounds(10, 90, 1193, 600);
//		desktopPane.add(interFrameTabVen);
//		
//		JInternalFrame interFrameAnlsPeriod = new JInternalFrame("New JInternalFrame");
//		interFrameAnlsPeriod.setBounds(0, 0, 1198, 598);
//		desktopPane.add(interFrameAnlsPeriod);
//		interFrameAnlsPeriod.setVisible(true);
//		interFrameTabVen.setVisible(true);
//		interFrameTabCli.setVisible(true);
//		interFrameTabProd.setVisible(true);
//		interFrameCadProd.dispose();
//		interFrameCadProd.setVisible(true);
//	}
//}
