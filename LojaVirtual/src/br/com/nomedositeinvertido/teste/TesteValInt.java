package br.com.nomedositeinvertido.teste;

public class TesteValInt {

	private static int valor;
	
	public static void main(String[] args) {

		System.out.println("Val: " + valor);
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

}
