package br.com.nomedositeinvertido.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {	
	
	public static Connection getConnection(){
		
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			// ip interno: 192.168.0.15:5432
			String url = "jdbc:postgresql://localhost:5432/postgres";
 			String usuario = "postgres";
 			String senha = "postgres";
 			con = DriverManager.getConnection(url, usuario, senha);
			//con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "postgres");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;		
	}
	
}
