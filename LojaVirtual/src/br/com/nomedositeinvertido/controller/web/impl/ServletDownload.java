package br.com.nomedositeinvertido.controller.web.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class ServletDownload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static final String SRC = "C:/Users/HP/Desktop/Desenvolvimento/WorkspaceEclipse/LojaVirtual/";
	
    public ServletDownload() {
        super();
    }

	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		response.setContentType("image/png");
        OutputStream out;
        FileInputStream in = null;
        byte[] buffer;
        int quantidadeLida;
        out = response.getOutputStream();
        
        Fachada	fachada = new Fachada();
        Produto produto = new Produto();
        
        String param = request.getParameter("img");
		try {						
			if(param.equals("Produto")){
				// Vai ler as imagens dos produtos
				produto.setId(Integer.parseInt(request.getParameter("id")));
				produto = (Produto) fachada.consultar(produto).getEntidades().get(0);	// Busca diretamente o objeto (Fachada-> DAO-> ListaResultado-> ObjetodaLista -> CastparaProduto)
	        	in = new FileInputStream(SRC + produto.getSrcWebCont());		   		// Le a imagem na pasta !!!!(POSSIBILIDADE DE REMOVER DOIS CAMPOS - SRCWEBCONT E ENDERIMAG)
		
			}else if(param.equals("LogoHeader")){
				// Vai ler as imagens do site
				in = new FileInputStream(SRC + "ImagensGUI/LogoHeader.png");

			}else if(param.equals("ImagemBemVindo")){
				in = new FileInputStream(SRC + "ImagensGUI/ImagemBemVindo.png");

			}else if(param.equals("FundoLoja")){
				in = new FileInputStream(SRC + "ImagensGUI/FundoLoja.png");

			}else if(param.equals("SetorOfertas")){
				in = new FileInputStream(SRC + "ImagensGUI/SetorOfertas.png");

			}
	        buffer = new byte[4096];										   // Cria um novo buffer reservando uma quantidade de bytes
	        while ((quantidadeLida = in.read(buffer, 0, buffer.length)) > 0) { // L� bit por bit no buffer e retornando 1 ou 0, quando termina retorna -1
	        	out.write(buffer, 0, quantidadeLida);						   // Escreve o conteudo do buffer na sa�da da servlet
	        }
	        in.close();
	        out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//////////////////////////////////	
	}
}
