package br.com.nomedositeinvertido.controller.web.impl;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(dispatcherTypes = {
				DispatcherType.REQUEST, 
				DispatcherType.FORWARD
		}
					, urlPatterns = { "/*" })
public class FiltroAutenticador implements Filter {

	// Construtor
    public FiltroAutenticador() {
    }

    // Executado quando o filtro � descarregado pelo container
	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// A sessao � criada a partir do login correto, dessa forma dever� ser destruida no logout
		// Ou a sessao pode ser criada ao acessar o site e destruida com a saida de tal
		// N�o esque�a de colocar timer para encerrar a sess�o, para n�o sobrecarregar o conteiner do servidor
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;		
		HttpSession sessao = httpServletRequest.getSession(false); 
		boolean verifSessao = httpServletRequest.isRequestedSessionIdValid(); // O teste com o ID n�o resolveu o problema
		String url = httpServletRequest.getRequestURI();		   // Busca a URI para a variavel
		
		System.out.println("URL: " + url);
		System.out.println("Boolean Sess�o: " + verifSessao);
		
		if(sessao != null){	
			System.out.println("Tem sess�o");			
			
			if(sessao.getAttribute("clienteLogado") == null){
				System.out.println("clienteLogado Null");
			    if(url.lastIndexOf("frmCarrinho.jsp") > -1 ||
				       url.lastIndexOf("HistPedds.jsp") > -1){ // Vai barrar o usuario nao logado que tentar acessar o carrinho ou o historico via url
			    	((HttpServletResponse) response) .sendRedirect("frmLoginAut.jsp");
			    }else{
			    	chain.doFilter(request, response);
			    }
			}else{				
				chain.doFilter(request, response);
			}
			
		}else{
			System.out.println("N�o tem sess�o");			

			if(url.lastIndexOf("frmCarrinho.jsp") > -1 || 
				url.lastIndexOf("HistPedds.jsp") > -1){ // Vai barrar o usuario nao logado que tentar acessar o carrinho ou o historico via url
				((HttpServletResponse) response) .sendRedirect("frmLoginAut.jsp");				
			}else{				
				chain.doFilter(request, response);
			}
		}
	}
	
	// Executado quando o filtro � carregado pelo container
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
