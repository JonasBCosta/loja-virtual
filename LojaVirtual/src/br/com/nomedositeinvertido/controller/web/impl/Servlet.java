package br.com.nomedositeinvertido.controller.web.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.FactoryMethodVH;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.negocio.command.ICommand;
import br.com.nomedositeinvertido.negocio.command.impl.CommandAlterar;
import br.com.nomedositeinvertido.negocio.command.impl.CommandConsultar;
import br.com.nomedositeinvertido.negocio.command.impl.CommandExcluir;
import br.com.nomedositeinvertido.negocio.command.impl.CommandSalvar;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PesquisarClientes;

public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Map<String, ICommand> cms = new HashMap<String, ICommand>();
		
	Fachada    		fachada   = new Fachada();
	Resultado  		resultado = null;	
	IViewHelperWeb  vh = null;
	
	// Construtor. Seta o hashmap de opera��es CRUD 
    public Servlet() {	// Considere instanciar a variavel de sess�o aqui no construtor
        super();
        cms.put("Cadastrar", new CommandSalvar());    // Retorno: null se OK, msgn se FAIL
		cms.put("Alterar", new CommandAlterar());	  // Retorno: null se OK, msgn se FAIL
		cms.put("Consultar", new CommandConsultar()); // Retorno: Objeto (entidade)
		cms.put("Exclu�r", new CommandExcluir());	  // Retorno: null
		cms.put("Entrar", new CommandConsultar());
		cms.put("Adicionar ao carrinho", new CommandSalvar());
		cms.put("Retirar do carrinho", new CommandExcluir());
		cms.put("Alterar a quantidade", new CommandAlterar());
		resultado = fachada.consultarTodos(new Produto());		
    }
    
    // Carrega as listas do Banco de Dados, Toda vez que isso ocorre as listas s�o atualizadas
	public void carregarDominios(HttpServletRequest request, HttpServletResponse response){		
		HttpSession session = request.getSession();
		Produto produto = new Produto();
		Resultado  resultado = null;	
		resultado = fachada.consultarTodos(produto);
		session.setAttribute("produtos", resultado.getEntidades()); // O cast ser� feito dentro da JSP		
//		List<EntidadeDominio> entList = new ArrayList<EntidadeDominio>();
//		Categoria categ = new Categoria();
		List<String> categList = new ArrayList<String>();
//		CategoriaDAO categDAO = new CategoriaDAO();
//		entList = categDAO.consultarTodos(null);
//		for (EntidadeDominio entidadeDominio : entList) {
//			categList.add((Categoria) entidadeDominio);
//		}
		categList.add("Eletrodom�sticos");
		categList.add("Telefones e Celulares");
		categList.add("Computadores e Notebooks");
		categList.add("M�veis");
		categList.add("Livros");
		categList.add("Utens�lios");
		session.setAttribute("categorias", categList);	
	} 
	
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			String path = request.getRequestURI();
			vh = FactoryMethodVH.create(path);
			super.service(request, response);	
			carregarDominios(request, response);
			// este resultado � o objeto, que pode assumir o valor de null, entidade ou mensagem
			vh.setView(resultado, request, response);  	// o resultado pode ser null, entidadedominio, mensagem de erro
			
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	// Somente UM objeto pode ser obtido, setado e retornado  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		resultado = null;
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		//Pega as chaves
		String operacao = request.getParameter("OPERACAO");	// CHAVE OPERA��O: Pega o parametro "opera��o" como chave que vem da jsp	
		//String path = request.getRequestURI();			// CHAVE CAMINHO: Pega o path (url b�sica sem parametros) atual pra usar como chave da VH
		// Com a CHAVE CAMINHO, vai selecionar a VH correspondente
		EntidadeDominio entidade = vh.getEntidade(request); // busca a entidade da JSP
		// A entidade pode chegar parcialmente setada
		// seta a entidade do VH com a entidade buscada mesmo que esteja parcialmente setada
		vh.setEntidade(entidade);
		// Naturalmente aqui dever� haver mudan�a na JSP
		// Com a CHAVE OPERA��O, vai selecionar a opera��o de fachada correspondente
		if(operacao != null && entidade != null){
			ICommand cmd = cms.get(operacao);		// Usa a chave "parametro" no mapa para selecionar a a��o requisitada 
			Object retorno = cmd.execute(entidade, cliente);		// executa a a��o selecionada pela chave para a entidade	
			resultado = (Resultado) retorno;
			if(operacao == "consultar"){ // Se foi consulta, o elemento era parcial, mas agora esta completo
				vh.setEntidade((EntidadeDominio) resultado.getEntidades().get(0)); // seta denovo, para setar o objeto completo 
			}
			if(cmd instanceof CommandAlterar){ // Qualquer altera�ao, � atualizada no cliente da sess�o.
				IFactory clienteFactory = new PesquisarClientes();
				cliente = clienteFactory.factoryPedidoCliente(cliente).get(0); // Quando passado o parametro, busca somente o cliente completo
				session.setAttribute("clienteLogado", cliente);
			}
		}
	}
}
