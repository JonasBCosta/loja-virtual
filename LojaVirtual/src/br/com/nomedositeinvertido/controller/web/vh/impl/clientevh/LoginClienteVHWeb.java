package br.com.nomedositeinvertido.controller.web.vh.impl.clientevh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class LoginClienteVHWeb implements IViewHelperWeb{

	// Retirar o static e alterar todas as chamadas de cliente estatico pela chamada da session com o cliente
	public Cliente cliente = new Cliente(); 
		
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		retirarValoresNull();		
		cliente.setLogin(request.getParameter("txtLogin"));
		cliente.setSenha(request.getParameter("txtSenha"));
		return cliente;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String msgErros = null;
		if(resultado != null){ 	
			// A l�gica foi alterada por quest�o de seguran�a
			if(cliente.getStatusValida��o() == "senha errada" || cliente.getStatusValida��o() == "em branco" || cliente.getStatusValida��o() == "login errado"){
				msgErros = "O apelido ou a senha n�o est�o corretos...";		
			}
		}  
		if(msgErros != null){	// Encontrou erro
			request.setAttribute("resultAut", msgErros);
			request.getRequestDispatcher("/frmLoginAut.jsp").forward(request, response);
		}
		else if(cliente.getId() <= 0){ // Cliente em branco
			request.getRequestDispatcher("/frmLoginAut.jsp").forward(request, response);
		}
		else{	// Encontrou login v�lido, vai criar a sess�o
			HttpSession session = request.getSession();
			session.setAttribute("clienteLogado", cliente);
			// Vai mandar como request (e n�o pela session) por enquanto para a pag de bemvindo acessar a session
			request.getRequestDispatcher("/frmBemVindo.jsp").forward(request, response);
		}
	}

	public void setEntidade(EntidadeDominio entidade) {
		cliente = (Cliente) entidade;
	}

	public void retirarValoresNull(){	
		cliente.setLogin("");    
 		cliente.setSenha(""); 
 		cliente.setNome(""); 
 		cliente.setCPF(""); 
 		cliente.setDataNascimento(""); 
 		cliente.setTelefone(""); 
 		cliente.setCelular(""); 
 		cliente.setEmail(""); 
 		cliente.getEndereco().setCep(""); 
 		cliente.getEndereco().setEstado(""); 
 		cliente.getEndereco().setCidade(""); 
 		cliente.getEndereco().setBairro(""); 
 		cliente.getEndereco().setRua(""); 
 		cliente.getEndereco().setNumero(""); 
 		cliente.getEndereco().setComplemento("");
 		cliente.setStatusValida��o("em branco"); 
 		cliente.setId(-1);
	}
}
