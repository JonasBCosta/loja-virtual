package br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;

public class ExcluiPedidoVHWeb implements IViewHelperWeb{

	Pedido pedido = new Pedido();

	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {		
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		pedido.setId(cliente.getPedEmUso().getId());
		pedido.setIdCliente(cliente.getId());		
		return pedido;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		PedidoDAO pedDAO = new PedidoDAO();
		ClienteDAO cliDAO = new ClienteDAO();
		pedDAO.cadastrar(pedido);	// Cadastra novo pedido (Vai receber id novo e Status "Em espera")		
		pedido = (Pedido) pedDAO.consultar(pedido);		
		// Atualiza o campo do pedido aberto(atual) do cliente
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		cliente.setPedEmUso(pedido);
		cliDAO.alterar(cliente);
		session.setAttribute("clienteLogado", cliente);
		response.sendRedirect("http://localhost:8083/LojaVirtual/frmBemVindo.jsp");
	}

	public void setEntidade(EntidadeDominio entidade) {		
		pedido = (Pedido) entidade;
	}
}
