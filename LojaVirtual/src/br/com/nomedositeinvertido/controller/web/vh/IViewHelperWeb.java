package br.com.nomedositeinvertido.controller.web.vh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;

public interface IViewHelperWeb {
	
	public EntidadeDominio getEntidade(HttpServletRequest request) throws ServletException, IOException;
	
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	
	public void setEntidade(EntidadeDominio entidade);
	
}
