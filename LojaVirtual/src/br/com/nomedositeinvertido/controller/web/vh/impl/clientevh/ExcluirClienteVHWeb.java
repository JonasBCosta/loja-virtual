package br.com.nomedositeinvertido.controller.web.vh.impl.clientevh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class ExcluirClienteVHWeb implements IViewHelperWeb{

	//Cliente cliente = new Cliente();
	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		return cliente;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("http://localhost:8083/LojaVirtual/frmBemVindo.jsp");
	}

	public void setEntidade(EntidadeDominio entidade) {
		
	}

}
