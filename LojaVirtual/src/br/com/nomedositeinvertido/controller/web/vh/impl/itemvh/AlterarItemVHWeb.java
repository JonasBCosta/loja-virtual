package br.com.nomedositeinvertido.controller.web.vh.impl.itemvh;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidoAbertoCliente;

public class AlterarItemVHWeb implements IViewHelperWeb{

	Item item = new Item();
	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		Item item = new Item();
		item.setId(Integer.parseInt(request.getParameter("txtIndice")));
		item.setQuantidade(Integer.parseInt(request.getParameter("txtQtde")));
		item.setIdPedido(Integer.parseInt(request.getParameter("txtPedido")));		
		item.getProduto().setId(Integer.parseInt(request.getParameter("txtProduto")));
		return item;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		IFactory factoryCliente = new PedidoAbertoCliente();
		HttpSession session = request.getSession();		
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");		
		cliente = factoryCliente.factoryPedidoCliente(cliente).get(0); // PedidoAbertoCliente()
		
		session.setAttribute("clientePedAberto", cliente);
		
		if(resultado != null){ // erro
			if(resultado.getMensagens() != null){ // erro
				request.setAttribute("msgItem", resultado.getMensagens().get(0).toString()); // guarda a qtde na gaveta pra exibir  	   					 
				item.setId(-1);
			}
		} 
		request.setAttribute("item", item);
		request.getRequestDispatcher("/frmCarrinho.jsp").forward(request, response);
	}

	public void setEntidade(EntidadeDominio entidade) {
		item = (Item) entidade;
	}
}
