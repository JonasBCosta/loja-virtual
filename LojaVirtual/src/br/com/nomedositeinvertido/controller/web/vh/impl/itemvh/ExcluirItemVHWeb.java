package br.com.nomedositeinvertido.controller.web.vh.impl.itemvh;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidoAbertoCliente;

public class ExcluirItemVHWeb implements IViewHelperWeb{

	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		Item item = new Item();
		item.setId(Integer.parseInt(request.getParameter("indice")));
		return item;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		IFactory factoryCliente = new PedidoAbertoCliente();
		HttpSession session = request.getSession();		
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");		
		cliente = factoryCliente.factoryPedidoCliente(cliente).get(0); // PedidoAbertoCliente()		
		session.setAttribute("clientePedAberto", cliente);	
		
		request.getRequestDispatcher("/frmCarrinho.jsp").forward(request, response);
	}

	public void setEntidade(EntidadeDominio entidade) {		
	}
}