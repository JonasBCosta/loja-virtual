package br.com.nomedositeinvertido.controller.web.vh.impl.clientevh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.controller.IFachada;
import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public class SalvarClienteVHWeb implements IViewHelperWeb {

	private Cliente cliente;	
	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {		
		Cliente cliente = new Cliente();
		
		cliente.setLogin(request.getParameter("txtLogin"));
		cliente.setSenha(request.getParameter("txtSenha"));
		cliente.setNome(request.getParameter("txtNome"));
		cliente.setCPF(request.getParameter("txtCPF"));
		cliente.setDataNascimento(request.getParameter("txtDataNasc"));
		cliente.setTelefone(request.getParameter("txtTelefone"));
		cliente.setCelular(request.getParameter("txtCelular"));
		cliente.setEmail(request.getParameter("txtEmail"));
		cliente.getEndereco().setCep(request.getParameter("txtCep"));
		cliente.getEndereco().setEstado(request.getParameter("txtEstado"));
		cliente.getEndereco().setCidade(request.getParameter("txtCidade"));
		cliente.getEndereco().setBairro(request.getParameter("txtBairro"));
		cliente.getEndereco().setRua(request.getParameter("txtRua"));
		cliente.getEndereco().setNumero(request.getParameter("txtNumero"));
		cliente.getEndereco().setComplemento(request.getParameter("txtComplemento"));
		
//		HttpSession session = request.getSession();
//		cliente = (Cliente) session.getAttribute("clienteLogado");
//		if(cliente != null){
//			cliente.setPedido(cliente.getPedido());
//			cliente.setId(cliente.getId());
//		}
		return cliente;
	}

	// Fun��o: Escreve mensagens de erro na mesma tela ou redireciona para outra jsp
	// Premissa: Cliente � cadastrado no BD e retorna o parametro "resultado"
	// Entrada "resultado": Se cadastrou, null, se n�o, lista de msng 
	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Recebeu como resultado, mnsg ou null
		// IFachada fachada = new Fachada();
		if(resultado == null){ // Cadastrou
			// LoginClienteVHWeb.cliente = (Cliente) fachada.consultar(cliente).getEntidades().get(0);
			request.getRequestDispatcher("/frmBemVindo.jsp").forward(request, response);
			//response.sendRedirect("http://localhost:8083/LojaVirtual/frmBemVindo.jsp");
		} else {// Voltou mensagem de erro
			String mensagemErro = "";
			for (Mensagem msgs : resultado.getMensagens()) {
				mensagemErro += msgs.toString();
			}
			request.setAttribute("mensagemErro", mensagemErro);
			request.getRequestDispatcher("/frmCadastro.jsp").forward(request, response);
		}
	}

	public void setEntidade(EntidadeDominio entidade) {
		this.cliente = (Cliente) entidade;
	}
}
