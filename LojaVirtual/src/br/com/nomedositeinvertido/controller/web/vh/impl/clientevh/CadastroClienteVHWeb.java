package br.com.nomedositeinvertido.controller.web.vh.impl.clientevh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;

public class CadastroClienteVHWeb implements IViewHelperWeb{

	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		return null;
	}
	
	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/frmCadastro.jsp").forward(request, response);
	}
	
	public void setEntidade(EntidadeDominio entidade) {
	}

}
