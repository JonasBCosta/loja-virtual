package br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;

public class AlteraStatusPedVHWeb implements IViewHelperWeb{

	Pedido pedido = new Pedido();
	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		pedido.setIdCliente(cliente.getId());
		PedidoDAO pedDAO = new PedidoDAO();
		pedido = (Pedido) pedDAO.consultar(pedido);
		ItemDAO itemDAO = new ItemDAO();
		Item item = new Item();
		List<EntidadeDominio> itensEntDom = new ArrayList<EntidadeDominio>();
		ArrayList<Item> itens = new ArrayList<Item>();
		ProdutoDAO prodDAO = new ProdutoDAO();
		item.setIdPedido(pedido.getId());
		itensEntDom = itemDAO.consultarTodos(item);
		
		for (int i = 0; i < itensEntDom.size(); i++) {
			itens.add((Item) itensEntDom.get(i));		
		}
		//////// VAI CONSULTAR O PEDIDO EM ESPERA/ABERTO DO CLIENTE -----------------
		//---------------------------------------------------------------------------

		for (int i = 0; i < itens.size(); i++) {			
			Produto produto = new Produto();
			produto.setId(itens.get(i).getIdProduto());
			produto = (Produto) prodDAO.consultar(produto);
			produto.setVendidos(produto.getVendidos() + itens.get(i).getQuantidade());
			prodDAO.alterar(produto);
		}
		
		return pedido;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/frmBemVindo.jsp").forward(request, response);
	}

	public void setEntidade(EntidadeDominio entidade) {
		this.pedido = (Pedido)entidade;
	}
}
