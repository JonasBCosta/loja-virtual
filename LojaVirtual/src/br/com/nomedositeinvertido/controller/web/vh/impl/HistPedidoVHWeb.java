package br.com.nomedositeinvertido.controller.web.vh.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.negocio.factory.IFactory;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidoAbertoCliente;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidosAguardPag;
import br.com.nomedositeinvertido.negocio.factory.impl.PedidosFinalizados;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;

public class HistPedidoVHWeb implements IViewHelperWeb{
		
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {		
		return null;
	}
	
	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		IFactory factoryClienteAP = new PedidosAguardPag();
		IFactory factoryClienteF = new PedidosFinalizados();
		
		HttpSession session = request.getSession();
		Cliente cliente = (Cliente) session.getAttribute("clienteLogado");
		
		cliente = factoryClienteAP.factoryPedidoCliente(cliente).get(0); // Apenas um cliente
		cliente.getPedidos().addAll(factoryClienteF.factoryPedidoCliente(cliente).get(0).getPedidos()); // Pedidos finalizados adicionados no final da lista de Pedidos AgPag
		session.setAttribute("pedidosCliente", cliente.getPedidos());
		
		request.getRequestDispatcher("/frmHistPedds.jsp").forward(request, response);
	}
	
	public void setEntidade(EntidadeDominio entidade) {
		
	}
}
