package br.com.nomedositeinvertido.controller.web.vh.impl.produtovh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class DescricaoProdutoVHWeb implements IViewHelperWeb{

	Produto produto = null;
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		produto = new Produto();
		produto.setId(Integer.parseInt(request.getParameter("indice")));
		return produto;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if(resultado != null){
			request.setAttribute("produto", resultado.getEntidades().get(0));
			request.getRequestDispatcher("/frmDescrProd.jsp").forward(request, response);
		}
	}

	public void setEntidade(EntidadeDominio entidade) {	
		produto = (Produto) entidade;
	}

}
