package br.com.nomedositeinvertido.controller.web.vh.impl.produtovh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.impl.Fachada;
import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class FiltrarBuscaVHWeb implements IViewHelperWeb{

	Produto produto = new Produto();
	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		produto.setCategoria((String) request.getParameter("hdnCateg"));
		System.out.println("\n\nFiltrarBuscaVHWeb - hdnCateg: " + produto.getCategoria());
		return produto;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {	
		String hdnIdCli = request.getParameter("hdnIdCli");
		if(hdnIdCli.isEmpty()){
			hdnIdCli = "-1"; // Para evitar erro de NumberFormatException
		}
		System.out.println("\n\nFiltrarBuscaVHWeb - hdnIdCli: " + hdnIdCli);
		request.setAttribute("idCliente", Integer.parseInt(hdnIdCli));	
		preparaSelecao(request);
		request.getRequestDispatcher("/frmProdutos.jsp").forward(request, response);
	}

	public void setEntidade(EntidadeDominio entidade) {
		this.produto = (Produto) entidade;
	}	
	
	private void preparaSelecao(HttpServletRequest request){
		HttpSession session = request.getSession();
		List<Produto> produtos = new ArrayList<Produto>();  	
		List<List<Produto>> listListProd = new ArrayList<List<Produto>>();
		List<Produto> auxProds = new ArrayList<Produto>();
		int count = 0;
		Fachada fachada = new Fachada();
		Resultado  resultado = null;	

		resultado = fachada.consultarTodos(produto);	
		for (EntidadeDominio prods : resultado.getEntidades()) {				// Verificar poss�vel erro de l�gica aqui
			Produto prod = (Produto) prods;		
			System.out.println("\n\nFiltrarBuscaVHWeb - produtos: " + prod.getCategoria());

			if(prod.getCategoria().equals(produto.getCategoria())){				// Compara a categoria do produto corrente com a categoria escolhida pelo usuario
				System.out.println("\n\nFiltrarBuscaVHWeb - produto categoria: " + prod.getCategoria());
				produtos.add(prod);
			}
		}
		for (Produto prod : produtos) {
			auxProds.add(prod);
			count++;
			if((count % 3) == 0 || count == produtos.size()){
				listListProd.add(auxProds);
				auxProds = new ArrayList<Produto>();
			}
		}
		session.setAttribute("produtos", listListProd); // O cast ser� feito dentro da JSP
	}
}
