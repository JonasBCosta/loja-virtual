package br.com.nomedositeinvertido.controller.web.vh;

import java.util.HashMap;
import java.util.Map;

import br.com.nomedositeinvertido.controller.web.vh.impl.BemVindoVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.HistPedidoVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.CadastroClienteVHWeb;
//import br.com.nomedositeinvertido.controller.web.vh.impl.FormasPagamentoVHWeb;
//import br.com.nomedositeinvertido.controller.web.vh.impl.HistPedidoVHWeb;
//import br.com.nomedositeinvertido.controller.web.vh.impl.SalvarHistoricoVHWeb;
//import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.CadastroClienteVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.ExcluirClienteVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LoginClienteVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.LogoutClienteVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.clientevh.SalvarClienteVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.itemvh.AlterarItemVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.itemvh.ExcluirItemVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.itemvh.SalvarItemVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh.AlteraStatusPedVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh.ExcluiPedidoVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh.ValidaPedidoVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.produtovh.DescricaoProdutoVHWeb;
import br.com.nomedositeinvertido.controller.web.vh.impl.produtovh.FiltrarBuscaVHWeb;

public class FactoryMethodVH {
	
	// Cria um hashmap para direcionar a fun��o correta para a pagina em uso
	private static Map<String, IViewHelperWeb> vhs = new HashMap<String, IViewHelperWeb>();

	// Seta o hashmap
	static{
		vhs.put("/LojaVirtual/CadastroCliente", new CadastroClienteVHWeb());
		vhs.put("/LojaVirtual/SalvarCliente", new SalvarClienteVHWeb());
		vhs.put("/LojaVirtual/CadastroItem", new SalvarItemVHWeb());
		vhs.put("/LojaVirtual/BemVindo", new BemVindoVHWeb());
		vhs.put("/LojaVirtual/LoginCliente", new LoginClienteVHWeb());
		vhs.put("/LojaVirtual/DescricaoProduto", new DescricaoProdutoVHWeb());
		vhs.put("/LojaVirtual/ExcluirCliente", new ExcluirClienteVHWeb());
		vhs.put("/LojaVirtual/ExcluirPedido", new ExcluiPedidoVHWeb());
		vhs.put("/LojaVirtual/ExcluirItem", new ExcluirItemVHWeb());
		vhs.put("/LojaVirtual/AlterarItem", new AlterarItemVHWeb());
		vhs.put("/LojaVirtual/FiltrarBusca", new FiltrarBuscaVHWeb());
		vhs.put("/LojaVirtual/AlteraStatusPed", new AlteraStatusPedVHWeb());
		vhs.put("/LojaVirtual/ValidarPedido", new ValidaPedidoVHWeb());
		vhs.put("/LojaVirtual/LogoutCliente", new LogoutClienteVHWeb());
		vhs.put("/LojaVirtual/HistoricoPedidos", new HistPedidoVHWeb());

	}
	
	// pega a URL corrente e, usando o hashmap, retorna a classe criada para tal pagina
	public static IViewHelperWeb create(String url)	{
		return vhs.get(url);
	}
}
