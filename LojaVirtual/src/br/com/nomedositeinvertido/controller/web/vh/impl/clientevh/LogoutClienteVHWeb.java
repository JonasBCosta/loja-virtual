package br.com.nomedositeinvertido.controller.web.vh.impl.clientevh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;

public class LogoutClienteVHWeb implements IViewHelperWeb{

	
	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		// A entidade cliente � buscada aqui atrav�s de metodo
		// N�o se faz necess�ria porque o cliente � est�tico
		return null;
	}

	
	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.invalidate();
		request.getRequestDispatcher("/frmBemVindo.jsp").forward(request, response);
	}


	public void setEntidade(EntidadeDominio entidade) {
		// Este metodo grava o objeto buscado no metodo getEntidade no atributo desta classe
		// para ser usado no setView
	}

	
}
