package br.com.nomedositeinvertido.controller.web.vh.impl.pedidovh;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.nomedositeinvertido.controller.web.vh.IViewHelperWeb;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.VerificaTimerCarrinho;

public class ValidaPedidoVHWeb implements IViewHelperWeb{

	public EntidadeDominio getEntidade(HttpServletRequest request)
			throws ServletException, IOException {
		IStrategy cmd = new VerificaTimerCarrinho();
		cmd.execute(null, null);
		return null;
	}

	public void setView(Resultado resultado, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	}

	public void setEntidade(EntidadeDominio entidade) {
		
	}

}
