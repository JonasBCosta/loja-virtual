package br.com.nomedositeinvertido.controller;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;

public interface IFachada {

	public Resultado cadastrar(EntidadeDominio entidade, Cliente cliente);
	public Resultado alterar(EntidadeDominio entidade, Cliente cliente);
	public Resultado excluir(EntidadeDominio entidade, Cliente cliente);
	public Resultado consultar(EntidadeDominio entidade);
	public Resultado consultarTodos(EntidadeDominio entidade);
}
