package br.com.nomedositeinvertido.controller.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.nomedositeinvertido.controller.IFachada;
import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Mensagem;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.dominio.item.Lote;
import br.com.nomedositeinvertido.dominio.loja.Categoria;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.jdbc.IDAO;
import br.com.nomedositeinvertido.jdbc.impl.CategoriaDAO;
import br.com.nomedositeinvertido.jdbc.impl.ClienteDAO;
import br.com.nomedositeinvertido.jdbc.impl.FornecedorDAO;
import br.com.nomedositeinvertido.jdbc.impl.ItemDAO;
import br.com.nomedositeinvertido.jdbc.impl.ItemEstoqueDAO;
import br.com.nomedositeinvertido.jdbc.impl.LoteDAO;
import br.com.nomedositeinvertido.jdbc.impl.PedidoDAO;
import br.com.nomedositeinvertido.jdbc.impl.ProdutoDAO;
import br.com.nomedositeinvertido.negocio.strategy.IStrategy;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.ComparaEstoque;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.ComparaItem;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.ValidacaoCPF;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.VerificaTimerCarrinho;
import br.com.nomedositeinvertido.negocio.strategy.impl.pesquisar.VerificacaoDuplicidade;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.AtualizarPedido;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.BaixaQuantidadeProduto;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.GravarItensEstoque;
import br.com.nomedositeinvertido.negocio.strategy.impl.salvar.GravarQuantidadeProduto;

public class Fachada implements IFachada{

	private Map<String, IDAO> daos;							
	private Map<String, List<IStrategy>> pre_rns;
	private Map<String, List<IStrategy>> pos_rns;
	private Map<String, List<IStrategy>> exc_rns;

	public Fachada (){
		daos = new HashMap<String, IDAO>();					
		pre_rns = new HashMap<String, List<IStrategy>>();
		pos_rns = new HashMap<String, List<IStrategy>>(); 
		exc_rns = new HashMap<String, List<IStrategy>>();
		
		//Cliente
		daos.put(Cliente.class.getName(), new ClienteDAO());   		
		List<IStrategy> pre_rnsCliente = new ArrayList<IStrategy>(); 		
		List<IStrategy> pos_rnsCliente = new ArrayList<IStrategy>();
		
		pre_rnsCliente.add(new ValidacaoCPF());					  
		pre_rnsCliente.add(new VerificacaoDuplicidade());
//		pos_rnsCliente.add(new ConsultarEntidade());
		pos_rnsCliente.add(new AtualizarPedido());
		
		pre_rns.put(Cliente.class.getName(), pre_rnsCliente);
		pos_rns.put(Cliente.class.getName(), pos_rnsCliente);
		
		//Produto
		daos.put(Produto.class.getName(), new ProdutoDAO());      
		List<IStrategy> pre_rnsProduto = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsProduto = new ArrayList<IStrategy>();

		pre_rnsProduto.add(new VerificacaoDuplicidade());
		
		pre_rns.put(Produto.class.getName(), pre_rnsProduto);
		pos_rns.put(Produto.class.getName(), pos_rnsProduto);

		//Pedido
		daos.put(Pedido.class.getName(), new PedidoDAO());
		List<IStrategy> pre_rnsPedido = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsPedido = new ArrayList<IStrategy>();

		pre_rnsPedido.add(new AtualizarPedido());
		
		pre_rns.put(Pedido.class.getName(), pre_rnsPedido);
		pos_rns.put(Pedido.class.getName(), pos_rnsPedido);

		//Item
		daos.put(Item.class.getName(), new ItemDAO());
		List<IStrategy> pre_rnsItem = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsItem = new ArrayList<IStrategy>();

		pre_rnsItem.add(new ComparaEstoque());
		pre_rnsItem.add(new VerificaTimerCarrinho());
		pre_rnsItem.add(new ComparaItem());
		
		pre_rns.put(Item.class.getName(), pre_rnsItem);
		pos_rns.put(Item.class.getName(), pos_rnsItem);
		
//////////// ESTOQUE -----------------------------------------------------------------------------
		//Fornecedor
		daos.put(Fornecedor.class.getName(), new FornecedorDAO());
		List<IStrategy> pre_rnsFornecedor = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsFornecedor = new ArrayList<IStrategy>();
		
		pre_rnsFornecedor.add(new VerificacaoDuplicidade());
		
		pre_rns.put(Fornecedor.class.getName(), pre_rnsFornecedor);
		pos_rns.put(Fornecedor.class.getName(), pos_rnsFornecedor);

		//Lote
		daos.put(Lote.class.getName(), new LoteDAO());
		List<IStrategy> pre_rnsLote = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsLote = new ArrayList<IStrategy>();
		List<IStrategy> exc_rnsLote = new ArrayList<IStrategy>();
				
		exc_rnsLote.add(new BaixaQuantidadeProduto());
		
		pre_rns.put(Lote.class.getName(), pre_rnsLote);
		pos_rns.put(Lote.class.getName(), pos_rnsLote);
		exc_rns.put(Lote.class.getName(), exc_rnsLote);
		
		//ItemEstoque
		daos.put(ItemEstoque.class.getName(), new ItemEstoqueDAO());
		List<IStrategy> pre_rnsItemEstoque = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsItemEstoque = new ArrayList<IStrategy>();
		
		pre_rnsItemEstoque.add(new GravarItensEstoque());
		pre_rnsItemEstoque.add(new GravarQuantidadeProduto());
				
		pre_rns.put(ItemEstoque.class.getName(), pre_rnsItemEstoque);
		pos_rns.put(ItemEstoque.class.getName(), pos_rnsItemEstoque);
		
////////////CONFIGURAÇÃO ------------------------------------------------------------------------
		//Configuração
		daos.put(Categoria.class.getName(), new CategoriaDAO());
		List<IStrategy> pre_rnsCategoria = new ArrayList<IStrategy>();
		List<IStrategy> pos_rnsCategoria = new ArrayList<IStrategy>();
						
		pre_rns.put(Categoria.class.getName(), pre_rnsCategoria);
		pos_rns.put(Categoria.class.getName(), pos_rnsCategoria);
	}	

	public Resultado consultarTodos(EntidadeDominio entidade){ 
		String nmClasse = entidade.getClass().getName();		
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		IDAO dao = daos.get(nmClasse);        		
		entidades = dao.consultarTodos(entidade); 		// Algumas classes usam a entidade nesta função
		return new Resultado(null, entidades);		
	}	
	
	public Resultado excluir(EntidadeDominio entidade, Cliente cliente){
		String nmClasse = entidade.getClass().getName(); 
		IDAO dao = daos.get(nmClasse);	
		List<IStrategy> exc_cmds = exc_rns.get(nmClasse);			
		List<Mensagem> msgs = new ArrayList<Mensagem>();
		String msg = null;		
		if(exc_cmds != null){
			for(IStrategy cmd : exc_cmds){	                  
				msg = (String) cmd.execute(entidade, cliente);  
				if(msg != null){						
					msgs.add(new Mensagem(msg));
				}
			}
		}
		if(msgs.size() == 0) // Nada errado
		{
			dao.excluir(entidade);
		}else{ // Algo errado
			return new Resultado(msgs);
		}		
		return null;
	}	
	
	public Resultado consultar(EntidadeDominio entidade){
		String nmClasse = entidade.getClass().getName(); 
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		IDAO dao = daos.get(nmClasse);         
		entidades.add(dao.consultar(entidade));
		return new Resultado(null, entidades);	  
	}	
	
	public Resultado alterar(EntidadeDominio entidade, Cliente cliente) {
		String nmClasse = entidade.getClass().getName(); 
		IDAO dao = daos.get(nmClasse);	
		List<IStrategy> pre_cmds = pre_rns.get(nmClasse);
		List<IStrategy> pos_cmds = pos_rns.get(nmClasse);
		
		List<Mensagem> msgs = new ArrayList<Mensagem>();
		String msg = null;		
		for(IStrategy cmd : pre_cmds){	
			msg = (String) cmd.execute(entidade, cliente);
			if(msg != null){						
				msgs.add(new Mensagem(msg));
				if(entidade instanceof Item){
					return new Resultado(msgs);
				}
			}
		}		  			
		if(msgs.size() == 0) 
		{
			dao.alterar(entidade);			
			for(IStrategy cmd : pos_cmds){	                  
				msg = (String) cmd.execute(entidade, cliente);  
			}			
		}else{ 
			return new Resultado(msgs);
		}		
		return null;
	}	
	
	public Resultado cadastrar(EntidadeDominio entidade, Cliente cliente) {
		String nmClasse = entidade.getClass().getName(); 
		IDAO dao = daos.get(nmClasse);	
		List<IStrategy> pre_cmds = pre_rns.get(nmClasse);
		List<IStrategy> pos_cmds = pos_rns.get(nmClasse);			
		List<Mensagem> msgs = new ArrayList<Mensagem>();
		String msg = null;		
		for(IStrategy cmd : pre_cmds){	                  
			msg = (String) cmd.execute(entidade, cliente);  
			if(msg != null){						
				msgs.add(new Mensagem(msg));
			}
		}
		if(msgs.size() == 0) // Nada errado
		{
			dao.cadastrar(entidade);			
			for(IStrategy cmd : pos_cmds){	                  
				msg = (String) cmd.execute(entidade, cliente);  
			}
		}else{ // Algo errado
			return new Resultado(msgs);
		}		
		return null;
	}
}
