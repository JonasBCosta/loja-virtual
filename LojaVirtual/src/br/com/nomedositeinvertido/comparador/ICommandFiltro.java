package br.com.nomedositeinvertido.comparador;

public interface ICommandFiltro {
	
	public int execute(Object termo1, Object termo2);
}
