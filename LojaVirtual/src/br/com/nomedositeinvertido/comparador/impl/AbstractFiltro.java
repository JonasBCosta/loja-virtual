package br.com.nomedositeinvertido.comparador.impl;

import java.util.Comparator;

import br.com.nomedositeinvertido.comparador.ICommandFiltro;

public abstract class AbstractFiltro implements ICommandFiltro, Comparator{

	public int execute(Object termo1, Object termo2) {

		return compare(termo1, termo2);
	}

	public abstract int compare(Object termo1, Object termo2);
}
