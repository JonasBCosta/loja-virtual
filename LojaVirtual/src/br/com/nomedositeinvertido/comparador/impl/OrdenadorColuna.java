package br.com.nomedositeinvertido.comparador.impl;

import java.util.Comparator;
import java.util.Vector;

public class OrdenadorColuna extends AbstractFiltro{

	int colIndex;
	boolean ascending;
	public OrdenadorColuna(int colIndex, boolean ascending){
		this.colIndex = colIndex;
		this.ascending = ascending;
	}
	
	public int compare(Object a, Object b) {	// a e b ser�o listas do tipo Historico
		Vector v1 = (Vector)a;	// cria a matriz a, com a lista Historico "a"
		Vector v2 = (Vector)b;	// cria a matriz b, com a lista Historico "b"
		Object o1 = v1.get(colIndex);	// coloca o valor do indice especificado da lista Historico no Object o1
		Object o2 = v2.get(colIndex);	// coloca o valor do indice especificado da lista Historico no Object o2
		if(o1 instanceof String && ((String)o1).length() == 0){ // Verifica se o1 � vazio
			o1 = null;	//Seta-o como nulo
		}
		if(o2 instanceof String && ((String)o2).length() == 0){ // Verifica se o2 � vazio
			o2 = null;//Seta-o como nulo
		}
		if(o1 == null && o2 == null){// faz verifica��es e manda o codigo de erro de cada um
			return 0;
		} else if(o1 == null){
			return 1;
		} else if(o2 == null){
			return -1;
		} else if(o1 instanceof Comparable){ // Se for comparable, faz verifica��o de Comparable
			if(ascending){
				return ((Comparable)o1).compareTo(o2);
			} else {
				return ((Comparable)o2).compareTo(o1);
			}
		} else { // Sen�o, passa tudo pra string e compara como string
			if (ascending) {
				return o1.toString().compareTo(o2.toString());
			} else {
				return o2.toString().compareTo(o1.toString());
			}
		}
		
	}

}
