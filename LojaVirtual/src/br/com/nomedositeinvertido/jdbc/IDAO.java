package br.com.nomedositeinvertido.jdbc;

import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;

public interface IDAO {

	public boolean cadastrar(EntidadeDominio entidade);
	public boolean alterar(EntidadeDominio entidade);
	public boolean excluir(EntidadeDominio entidade);
	public EntidadeDominio consultar(EntidadeDominio entidade);
	public boolean verifDuplicidade(EntidadeDominio entidade);
	public List<EntidadeDominio> 
	consultarTodos(EntidadeDominio entidade);	
}
