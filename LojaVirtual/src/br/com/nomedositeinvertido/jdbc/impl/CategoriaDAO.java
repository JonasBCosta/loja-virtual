package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.loja.Categoria;

public class CategoriaDAO extends AbstractDAO{

	@Override
	public boolean cadastrar(EntidadeDominio entidade){			
		
		openConnection();
		Categoria categoria = (Categoria)entidade;   
		
		String sql = "INSERT INTO categorias (categoria, data) " +
				"values (?,?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, categoria.getCategoria());
			preparador.setTimestamp(2, categoria.getData());
			preparador.execute();
			preparador.close();			
		} catch (SQLException e) {
			System.out.println("Erro no cadastro");
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean alterar(EntidadeDominio entidade){		
		
		openConnection();
		Categoria categoria = (Categoria)entidade;  
		String sql = "UPDATE categorias SET categoria=?, data=? WHERE id=?";		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(3, categoria.getId());
			preparador.setString(1, categoria.getCategoria());
			preparador.setTimestamp(2, categoria.getData());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			System.out.println("Erro na altera��o");
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean excluir(EntidadeDominio entidade){
		
		openConnection();		
		String sql = "DELETE FROM categorias WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
			System.out.println("Exclu�do com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro na exclus�o");
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public EntidadeDominio consultar(EntidadeDominio entidade){ 
		
		openConnection();
		Categoria categoria = (Categoria)entidade;
		String sql = "SELECT * FROM categorias WHERE id = ?";	
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, categoria.getId());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 
					categoria.setCategoria(resultado.getString("categoria"));
					categoria.setData(resultado.getTimestamp("data"));					
					entidade = categoria; 
					resultado.close();
					preparador.close();
					conexao.close();
					return entidade;			
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade; 
	}	
	
	public boolean verifDuplicidade(EntidadeDominio entidade){ 
		openConnection();
		String sql = "SELECT * FROM categorias";
		PreparedStatement preparador;
		try {
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			Categoria categoria = (Categoria)entidade;
			while(resultado.next()){
				if(resultado.getString("categoria").equals(categoria.getCategoria())){
					if(resultado.getInt("id") == categoria.getId()){ // Verifica se n�o � o mesmo cadastro
						return false;
					}
					else{ // n�o � o mesmo cadastro
						return true;
					}
				}					
			}
			conexao.close();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;    
		}	
		return false; 
	}

	
	@Override
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		
		String sql = "SELECT * FROM categorias";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			
			if(resultado != null){
				while(resultado.next()){					           
					Categoria categoria = new Categoria();
					categoria.setCategoria(resultado.getString("categoria"));			
					categoria.setId(resultado.getInt("id"));// login
					categoria.setData(resultado.getTimestamp("data"));
					entidades.add(categoria); 
				}				
			}			
			resultado.close(); 
			preparador.close();
			conexao.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}
}
