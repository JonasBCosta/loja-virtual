package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.pedido.Pedido;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class PedidoDAO extends AbstractDAO{
			
	// Entrada: entidadeDominio (Cliente)
	// Sa�da: boolean
	public boolean cadastrar(EntidadeDominio entidade){	
		Pedido pedido = (Pedido) entidade;
		openConnection();
		String sql = "INSERT INTO pedidos (idcliente, data, expiracao, status) values (?,?,?,?)";		  
		try{
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, pedido.getIdCliente());			
			preparador.setTimestamp(2, pedido.getData());		
			preparador.setTimestamp(3, pedido.getExpiracao());
			preparador.setString(4, "Em espera");
			preparador.execute();
			preparador.close();
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;		
	}

	// Fun��o: exclui um pedido, atualiza todos os produtos deste pedido
	// Entrada: entidadeDominio (Pedido)
	// Sa�da: boolean
	public boolean excluir(EntidadeDominio entidade){	
	
	/////// PREMISSAS /////// -----------------------------------------------------------------
		List<EntidadeDominio> itens;
		ItemDAO itemDAO = new ItemDAO();
		ProdutoDAO prodDAO = new ProdutoDAO(); 
		Item item = new Item();
		item.setIdPedido(entidade.getId());
		
		// Busca uma lista de itens que pertencem � um pedido
		itens = itemDAO.consultarTodos(item);				
		// Atravez de cada item da lista busca o produto em quest�o
		for (EntidadeDominio entItem : itens) {
			item = new Item();				// Cria um objeto item para cada itera��o
			item = (Item) entItem;				// Transforma a entidade item em Item
			Produto produto = new Produto();	// Cria um objeto produto para cada itera��o
			produto.setId(item.getProduto().getId()); // prepara o objeto produto para a busca
			produto = (Produto) prodDAO.consultar(produto); // busca o produto
			produto.setQuantidade(produto.getQuantidade() + item.getQuantidade()); // atualiza o produto referente a este item
			prodDAO.alterar(produto); // altera
		}		
	//////////////////////// ------------------------------------------------------------------
		
		openConnection();				
		PreparedStatement preparador;
		// deleta um pedido na tabela PEDIDO 
		String sql = "DELETE FROM pedidos WHERE id = ?";
		try {
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}		
		try{ conexao.close();
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}	
	
	// Fun��o: consulta um pedido
	// Entrada: entidadeDominio (Pedido)
	// Sa�da: entidadeDominio (Pedido)
	public EntidadeDominio consultar(EntidadeDominio entidade){
		Pedido pedido = (Pedido) entidade;
		openConnection();
		String sql = "SELECT * FROM pedidos WHERE idCliente = ?";
		PreparedStatement preparador;
		EntidadeDominio entPed = null;
		try {
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, pedido.getIdCliente());
			ResultSet resultado = preparador.executeQuery();
			while(resultado.next())	{
				if(resultado.getString("status").equals("Em espera") || resultado.getString("status").equals("Aberto")){
					pedido = new Pedido();
					pedido.setId(resultado.getInt("id"));
					pedido.setIdCliente(resultado.getInt("idcliente"));
					pedido.setData(resultado.getTimestamp("data"));
					pedido.setExpiracao(resultado.getTimestamp("expiracao"));
					pedido.setStatus(resultado.getString("status"));
					entPed = pedido;
				}
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entPed;
	}
	
	public boolean alterar(EntidadeDominio entidade) {
		Pedido pedido = (Pedido) entidade;
		openConnection();
		String sql = "UPDATE pedidos SET data=?, expiracao=?, status=? WHERE id=?";		  
		try{
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(4, pedido.getId());			
			preparador.setTimestamp(1, pedido.getData());		
			preparador.setTimestamp(2, pedido.getExpiracao());
			preparador.setString(3, pedido.getStatus());
			preparador.execute();
			preparador.close();
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
	}

	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {		
		openConnection();
		String sql = "SELECT * FROM pedidos WHERE idCliente = ?";
		PreparedStatement preparador;
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		try {
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, ((Pedido) entidade).getIdCliente());
			ResultSet resultado = preparador.executeQuery();
			while(resultado.next())
			{
				Pedido pedido = new Pedido();
				pedido.setId(resultado.getInt("id"));
				pedido.setIdCliente(resultado.getInt("idcliente"));
				pedido.setData(resultado.getTimestamp("data"));
				pedido.setExpiracao(resultado.getTimestamp("expiracao"));
				pedido.setStatus(resultado.getString("status"));
				entidades.add(pedido);
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}

	public boolean verifDuplicidade(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM pedidos";
		PreparedStatement preparador;
		try {
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			Cliente cliente = (Cliente)entidade;
			while(resultado.next())
			{
				if(resultado.getString("login").equals(cliente.getLogin())){
					return true;
				}					
			}
			conexao.close();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;    
		}	
		return false; 
	}
}
