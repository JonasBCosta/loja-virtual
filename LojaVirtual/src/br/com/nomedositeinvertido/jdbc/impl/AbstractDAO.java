package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.Connection;

import br.com.nomedositeinvertido.jdbc.IDAO;
import br.com.nomedositeinvertido.util.Conexao;

public abstract class AbstractDAO implements IDAO {

	protected Connection conexao;
	
	protected void openConnection(){
			conexao = Conexao.getConnection();
	}
}
