package br.com.nomedositeinvertido.jdbc.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.Resultado;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.item.Item;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class ItemDAO extends AbstractDAO{

	// Entrada: entidadeDominio (Item)
	// Saida: boolean 
	public boolean cadastrar(EntidadeDominio entidade){		

		/////// PREMISSAS /////// -----------------------------------------------------------
		ProdutoDAO prodDAO = new ProdutoDAO();
		Produto produto = new Produto();
		Item item;
		
		item = (Item) entidade; // consulta um item pelo seu ID
		produto.setId(item.getProduto().getId()); // salva o id do produto no objeto produto 
		// Vai buscar o produto de tabela IMAGENS
		produto = (Produto) prodDAO.consultar(produto);   
		// Vai subtrair a qtdd pedida do estoque do produto
		produto.setQuantidade(produto.getQuantidade() - item.getQuantidade());

		// Vai alterar o produto da tabela IMAGENS
		prodDAO.alterar(produto);                      
		///////////////////////// -----------------------------------------------------------
		
		openConnection();
		// Vai inserir uma linha na tabelaitens
		String sql = "INSERT INTO itens (idpedido, idproduto, quantidade, data)" + 
					"values (?,?,?,?)";
		try{
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, item.getIdPedido());
			preparador.setInt(2, item.getProduto().getId());
			preparador.setInt(3, item.getQuantidade());
			preparador.setTimestamp(4, item.getData());
			preparador.execute();
			preparador.close();			
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	// Entrada: entidadeDominio (item) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Saida: lista entidadedominio (itens)
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade){
		Item item = (Item) entidade;
		openConnection();				
		PreparedStatement preparador;
		ResultSet resultado;
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		String sql = "SELECT * FROM itens WHERE idpedido = ?"; 
		try{
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, item.getIdPedido());
			resultado = preparador.executeQuery(); 
			while(resultado.next()){
				item = new Item();
				item.setId(resultado.getInt("id"));
				item.setIdPedido(resultado.getInt("idpedido"));
				item.getProduto().setId(resultado.getInt("idproduto"));
				item.setIdProduto(resultado.getInt("idproduto"));
				item.setQuantidade(resultado.getInt("quantidade"));
				item.setData(resultado.getTimestamp("data"));
				entidades.add(item);
			}
		}catch (SQLException e){
			e.printStackTrace();
			return entidades;
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}
	
	// Entrada: entidadeDominio (Item)
	// Sa�da: entidadeDominio (Item)
	public EntidadeDominio consultar(EntidadeDominio entidade){
		openConnection();
		PreparedStatement preparador;
		ResultSet resultado;
		String sql = "SELECT * FROM itens WHERE id = ?"; 
		try{
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			resultado = preparador.executeQuery(); 
			while(resultado.next()){
				Item item = new Item();
				item.setId(resultado.getInt("id"));
				item.setIdPedido(resultado.getInt("idpedido"));
				item.getProduto().setId(resultado.getInt("idproduto"));
				item.setQuantidade(resultado.getInt("quantidade"));
				item.setData(resultado.getTimestamp("data"));
				entidade = item;
			}			
		}catch (SQLException e){
			e.printStackTrace();
			return null;
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return entidade;
	}
	
	// Fun��o: Exclui um item da tabela Item, atualiza este produto
	// Entrada: entidadeDominio (Item)
	// Sa�da: boolean
	public boolean excluir(EntidadeDominio entidade){
		
		/**
		 * ESTAS RESPONSABILIDADES DE REGRAS DE NEG�CIO DEVEM SER COLOCADAS EM OUTRO LUGAR
		 */
	/////// PREMISSAS /////// -----------------------------------------------------------
		ProdutoDAO prodDAO = new ProdutoDAO();
		Produto produto = new Produto();
		Item item;
		
		item = (Item) consultar(entidade); // consulta um item pelo seu ID
		produto.setId(item.getProduto().getId());  // salva o id do produto no objeto produto 
		// Vai buscar o produto de tabela IMAGENS
		produto = (Produto) prodDAO.consultar(produto);               
		// Vai retornar a qtdd do produto � qtdd anterior
		produto.setQuantidade(produto.getQuantidade() + item.getQuantidade());
		// Vai alterar o produto da tabela IMAGENS
		prodDAO.alterar(produto);                      
	///////////////////////// -----------------------------------------------------------	
		
		openConnection();
		PreparedStatement preparador;	
		String sql = "DELETE FROM itens WHERE id = ?";
		try {
			preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}		
		try{ conexao.close();
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}	

	public boolean alterar(EntidadeDominio entidade) {
		
		/////// PREMISSAS /////// -----------------------------------------------------------
		ProdutoDAO prodDAO = new ProdutoDAO();
		Produto produto = new Produto();
		Item item, itemAux = new Item();		
		item = (Item) entidade;           	  // Este � o item atualizado
		itemAux.setId(item.getId());
		itemAux = (Item) consultar(itemAux);	  // Este � o item antigo
		produto.setId(item.getProduto().getId());   // salva o id do produto no objeto produto
		
		
		// Vai buscar o produto de tabela IMAGENS
		produto = (Produto) prodDAO.consultar(produto);		
		// Vai subtrair a qtdd pedida do estoque do produto
		produto.setQuantidade(produto.getQuantidade() + (itemAux.getQuantidade() - item.getQuantidade()));
		// Vai alterar o produto da tabela IMAGENS
		prodDAO.alterar(produto);
		///////////////////////// -----------------------------------------------------------		
		
		openConnection(); 
		String sql = "UPDATE itens SET idpedido=?, idproduto=?, quantidade=?, data=? WHERE id=?";		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(5, entidade.getId());
			preparador.setInt(1, item.getIdPedido());
			preparador.setInt(2, item.getProduto().getId());
			preparador.setInt(3, item.getQuantidade());
			preparador.setTimestamp(4, item.getData());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean verifDuplicidade(EntidadeDominio entidade) {
		return false;
	}
	
}
