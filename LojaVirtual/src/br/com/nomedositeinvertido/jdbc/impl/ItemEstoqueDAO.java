package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;

public class ItemEstoqueDAO extends AbstractDAO{

	@Override
	public boolean cadastrar(EntidadeDominio entidade) {
		openConnection();
		ItemEstoque itemEstq = (ItemEstoque) entidade;
		String sql = "INSERT INTO itemestoque (localizacao, codigo, idproduto, idlote, data)" + 
					"values (?,?,?,?,?)";
		try{
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, itemEstq.getLocalizacao());
			preparador.setString(2, itemEstq.getCodigo());
			preparador.setInt(3, itemEstq.getIdProduto());
			preparador.setInt(4, itemEstq.getIdLote());
			preparador.setTimestamp(5, itemEstq.getData());
			preparador.execute();
			preparador.close();			
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean alterar(EntidadeDominio entidade) {
		openConnection();
		ItemEstoque itemEstq = (ItemEstoque) entidade;
		String sql = "UPDATE itemestoque SET localizacao=?, codigo=?, idproduto=?, idlote=?, data=?, WHERE id=?"; 
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, itemEstq.getLocalizacao());
			preparador.setString(2, itemEstq.getCodigo());
			preparador.setInt(3, itemEstq.getIdProduto());
			preparador.setInt(4, itemEstq.getIdLote());
			preparador.setTimestamp(5, itemEstq.getData());
			preparador.setInt(6, itemEstq.getId());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			System.out.println("Erro na altera��o");
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean excluir(EntidadeDominio entidade) {
		openConnection();		
		String sql = "DELETE FROM itemestoque WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
			System.out.println("Exclu�do com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro na exclus�o");
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public EntidadeDominio consultar(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM itemestoque WHERE id = ?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 				
				ItemEstoque itemEstq = new ItemEstoque();
				itemEstq.setLocalizacao(resultado.getString("localizacao"));
				itemEstq.setCodigo(resultado.getString("codigo"));
				itemEstq.setIdProduto(resultado.getInt("idproduto"));
				itemEstq.setIdLote(resultado.getInt("idlote"));
				itemEstq.setData(resultado.getTimestamp("data"));
				itemEstq.setId(resultado.getInt("id"));
				entidade = itemEstq; 				
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 					
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade;
	}

	@Override
	public boolean verifDuplicidade(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();		
		String sql = "SELECT * FROM itemestoque";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			if(resultado != null){
				while(resultado.next()){ 				
					ItemEstoque itemEstq = new ItemEstoque();
					itemEstq.setLocalizacao(resultado.getString("localizacao"));
					itemEstq.setCodigo(resultado.getString("codigo"));
					itemEstq.setIdProduto(resultado.getInt("idproduto"));
					itemEstq.setIdLote(resultado.getInt("idlote"));
					itemEstq.setData(resultado.getTimestamp("data"));
					itemEstq.setId(resultado.getInt("id"));
					entidades.add(itemEstq); 
				}
			}
			resultado.close();
			preparador.close();
			conexao.close();
			return entidades; 									
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}

}
