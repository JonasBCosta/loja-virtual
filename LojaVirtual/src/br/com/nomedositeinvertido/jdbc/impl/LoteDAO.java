package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.item.ItemEstoque;
import br.com.nomedositeinvertido.dominio.item.Lote;

public class LoteDAO extends AbstractDAO{

	@Override
	public boolean cadastrar(EntidadeDominio entidade) {
		openConnection();
		Lote lote = (Lote) entidade;
		String sql = "INSERT INTO lotes (valorcompra, codigo, data, idfornecedor)" + 
					"values (?,?,?,?)";
		try{
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setBigDecimal(1, lote.getValorCompra());
			preparador.setString(2, lote.getCodigo());
			preparador.setTimestamp(3, lote.getData());
			preparador.setInt(4, lote.getFornecedor().getId());
			preparador.execute();
			preparador.close();			
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean alterar(EntidadeDominio entidade) {
		openConnection();
		Lote lote = (Lote) entidade;
		String sql = "UPDATE lotes SET valorcompra=?, codigo=?, data=?, idfornecedor=? WHERE id=?"; 
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setBigDecimal(1, lote.getValorCompra());
			preparador.setString(2, lote.getCodigo());
			preparador.setTimestamp(3, lote.getData());
			preparador.setInt(4, lote.getFornecedor().getId());
			preparador.setInt(5, lote.getId());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			System.out.println("Erro na altera��o");
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean excluir(EntidadeDominio entidade) {
		openConnection();		
		String sql = "DELETE FROM lotes WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
			System.out.println("Exclu�do com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro na exclus�o");
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public EntidadeDominio consultar(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM lotes WHERE id = ?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 				
				Lote lote = (Lote) entidade;
				lote.setValorCompra(resultado.getBigDecimal("valorcompra"));
				lote.setCodigo(resultado.getString("codigo"));
				lote.setData(resultado.getTimestamp("data"));
				lote.setId(resultado.getInt("id"));
				lote.setIdfornecedor(resultado.getInt("idfornecedor"));
				entidade = lote; 				
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 					
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade;
	}

	@Override
	public boolean verifDuplicidade(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();		
		String sql = "SELECT * FROM lotes";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			if(resultado != null){
				while(resultado.next()){ 				
					Lote lote = new Lote();
					lote.setValorCompra(resultado.getBigDecimal("valorcompra"));
					lote.setCodigo(resultado.getString("codigo"));
					lote.setData(resultado.getTimestamp("data"));
					lote.setId(resultado.getInt("id"));
					lote.setIdfornecedor(resultado.getInt("idfornecedor"));
					entidades.add(lote); 
				}
			}
			resultado.close();
			preparador.close();
			conexao.close();
			return entidades; 									
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}

}
