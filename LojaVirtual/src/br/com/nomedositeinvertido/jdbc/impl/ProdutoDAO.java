package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.produto.Produto;

public class ProdutoDAO extends AbstractDAO{
	
	public boolean cadastrar(EntidadeDominio entidade){
		openConnection();
		Produto produto = (Produto)entidade;
			String sql = "INSERT INTO produtos (nome, descricao, preco, srcwebcont, quantidade, enderecoimagem, categoria, vendidos, data, estoqueminimo) " +
					"values (?,?,?,?,?,?,?,?,?,?)";
			try {
				PreparedStatement preparador = conexao.prepareStatement(sql);				
				 
				preparador.setString(1, produto.getNome());
				preparador.setString(2, produto.getDescricao());
				preparador.setBigDecimal(3, produto.getPreco());
				preparador.setString(4, produto.getSrcWebCont());
				preparador.setInt(5, produto.getQuantidade());
				preparador.setString(6, produto.getEnderecoImagem());
				preparador.setString(7, produto.getCategoria());
				preparador.setInt(8, produto.getVendidos());
				preparador.setTimestamp(9, produto.getData());
				preparador.setInt(10, produto.getEstqMinimo());
				
				preparador.execute();
				preparador.close();			
			} catch (SQLException e) { 
				e.printStackTrace();
				return false;
			} 
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean alterar(EntidadeDominio entidade){	
		openConnection();
		Produto produto = (Produto)entidade;
		String sql = "UPDATE produtos SET nome=?, descricao=?, preco=?, " +
				"srcwebcont=?, quantidade=?, enderecoimagem=?, categoria=?, vendidos=?, data=?, estoqueminimo=? WHERE id=?";	
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(11, entidade.getId());		
			preparador.setString(1, produto.getNome());
			preparador.setString(2, produto.getDescricao());
			preparador.setBigDecimal(3, produto.getPreco());
			preparador.setString(4, produto.getSrcWebCont());
			preparador.setInt(5, produto.getQuantidade());
			preparador.setString(6, produto.getEnderecoImagem());
			preparador.setString(7, produto.getCategoria());
			preparador.setInt(8, produto.getVendidos());
			preparador.setTimestamp(9, produto.getData());
			preparador.setInt(10, produto.getEstqMinimo());
			preparador.execute();
			preparador.close();			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean excluir(EntidadeDominio entidade){
		
		openConnection();
		String sql = "DELETE FROM produtos WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade){
		
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		
		String sql = "SELECT * FROM produtos";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			
			if(resultado != null){
				while(resultado.next()){					           
					Produto produto = new Produto();
					produto.setNome(resultado.getString("nome"));
					produto.setPreco(resultado.getBigDecimal("preco"));
					produto.setDescricao(resultado.getString("descricao"));
					produto.setSrcWebCont(resultado.getString("srcwebcont"));
					produto.setId(resultado.getInt("id"));
					produto.setQuantidade(resultado.getInt("quantidade"));
					produto.setEnderecoImagem(resultado.getString("enderecoimagem"));
					produto.setCategoria(resultado.getString("categoria"));	
					produto.setVendidos(resultado.getInt("vendidos"));
					produto.setData(resultado.getTimestamp("data"));
					produto.setEstqMinimo(resultado.getInt("estoqueminimo"));
					entidades.add(produto);
				}				
			}			
			resultado.close(); 
			preparador.close();
			conexao.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;		
	}
	
	
	public EntidadeDominio consultar(EntidadeDominio entidade){
		openConnection();
		Produto produto = new Produto();
		String sql = "SELECT * FROM produtos WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			ResultSet resultado = preparador.executeQuery();
			if(resultado != null){
				while(resultado.next()){              
					produto.setNome(resultado.getString("nome"));
					produto.setPreco(resultado.getBigDecimal("preco"));
					produto.setDescricao(resultado.getString("descricao"));
					produto.setSrcWebCont(resultado.getString("srcwebcont"));
					produto.setId(resultado.getInt("id"));
					produto.setQuantidade(resultado.getInt("quantidade"));
					produto.setEnderecoImagem(resultado.getString("enderecoimagem"));
					produto.setCategoria(resultado.getString("categoria"));
					produto.setVendidos(resultado.getInt("vendidos"));
					produto.setData(resultado.getTimestamp("data"));
					produto.setEstqMinimo(resultado.getInt("estoqueminimo"));
					entidade = produto;
				}
			}
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return entidade;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return entidade;
		}
		return entidade;		
	}
	
	public boolean verifDuplicidade(EntidadeDominio entidade){ 
		openConnection();
		String sql = "SELECT * FROM produtos";
		PreparedStatement preparador;
		try {
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			Produto produto = (Produto)entidade;
			while(resultado.next())
			{
				if(resultado.getString("nome").equals(produto.getNome()) && 
						!resultado.getString("id").equals(String.valueOf(produto.getId()))){
					return true;
				}					
			}
			preparador.close();
			conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;    
		}
		return false; // deu certo
	}
}
