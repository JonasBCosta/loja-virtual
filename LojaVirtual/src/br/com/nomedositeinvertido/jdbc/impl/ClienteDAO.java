package br.com.nomedositeinvertido.jdbc.impl;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.cliente.Cliente;
import br.com.nomedositeinvertido.dominio.produto.Produto;
import br.com.nomedositeinvertido.util.Conexao;

public class ClienteDAO extends AbstractDAO{
	
	public boolean cadastrar(EntidadeDominio entidade){			
		
		openConnection();
		Cliente cliente = (Cliente)entidade;   
		
		String sql = "INSERT INTO clientes (nome, cpf, " +
				"login, telefone, celular, email, cep, " +
				"estado, cidade, bairro, rua, senha, " +
				"numero, complemento, datanascimento, carrinho, data) " +
				"values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, cliente.getNome());
			preparador.setString(2, cliente.getCPF()); 
			preparador.setString(3, cliente.getLogin());
			preparador.setString(4, cliente.getTelefone());
			preparador.setString(5, cliente.getCelular());
			preparador.setString(6, cliente.getEmail());
			preparador.setString(7, cliente.getEndereco().getCep());
			preparador.setString(8, cliente.getEndereco().getEstado());
			preparador.setString(9, cliente.getEndereco().getCidade());
			preparador.setString(10, cliente.getEndereco().getBairro());
			preparador.setString(11, cliente.getEndereco().getRua());
			preparador.setString(12, cliente.getSenha());
			preparador.setInt(13, Integer.parseInt(cliente.getEndereco().getNumero()));
			preparador.setString(14, cliente.getEndereco().getComplemento());
			preparador.setString(15, cliente.getDataNascimento());
			preparador.setInt(16, cliente.getPedEmUso().getId());
			preparador.setTimestamp(17, cliente.getData());
			preparador.execute();
			preparador.close();			
		} catch (SQLException e) {
			System.out.println("Erro no cadastro");
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean alterar(EntidadeDominio entidade){		
		
		openConnection();
		Cliente cliente = (Cliente)entidade;  
		String sql = "UPDATE clientes SET nome=?, cpf=?, login=?, " +
				"telefone=?, celular=?, email=?, cep=?, estado=?, " +
				"cidade=?, bairro=?, rua=?, senha=?, numero=?, " +
				"complemento=?, datanascimento=?, carrinho=?, data=? WHERE id=?";		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(18, cliente.getId());
			preparador.setString(1, cliente.getNome());
			preparador.setString(2, cliente.getCPF());
			preparador.setString(3, cliente.getLogin());
			preparador.setString(4, cliente.getTelefone());
			preparador.setString(5, cliente.getCelular());
			preparador.setString(6, cliente.getEmail());
			preparador.setString(7, cliente.getEndereco().getCep());
			preparador.setString(8, cliente.getEndereco().getEstado());
			preparador.setString(9, cliente.getEndereco().getCidade());
			preparador.setString(10, cliente.getEndereco().getBairro());
			preparador.setString(11, cliente.getEndereco().getRua());
			preparador.setString(12, cliente.getSenha());
			preparador.setInt(13, Integer.parseInt(cliente.getEndereco().getNumero()));
			preparador.setString(14, cliente.getEndereco().getComplemento());
			preparador.setString(15, cliente.getDataNascimento());
			preparador.setInt(16, cliente.getPedEmUso().getId());
			preparador.setTimestamp(17, cliente.getData());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			System.out.println("Erro na altera��o");
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean excluir(EntidadeDominio entidade){
		
		openConnection();		
		String sql = "DELETE FROM clientes WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
			System.out.println("Exclu�do com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro na exclus�o");
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public EntidadeDominio buscar(EntidadeDominio entidade, int id){ 
		
		openConnection();
		String sql = "SELECT * FROM clientes WHERE id = ?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, id);
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 
				
				Cliente cliente = new Cliente();
				cliente.setLogin(resultado.getString("login"));																  // login
				cliente.setSenha(resultado.getString("senha"));																  // senha
				cliente.setNome(resultado.getString("nome"));                       			  // nome
				cliente.setCPF(resultado.getString("cpf"));								  // cpf
				cliente.setDataNascimento(resultado.getString("datanascimento"));				  // datansc
				cliente.setTelefone(resultado.getString("telefone"));							  // telefone
				cliente.setCelular(resultado.getString("celular"));								  // celular
				cliente.setEmail(resultado.getString("email"));						 			  // email
				cliente.getEndereco().setCep(resultado.getString("cep"));						  // cep
				cliente.getEndereco().setEstado(resultado.getString("estado"));					  // estado
				cliente.getEndereco().setCidade(resultado.getString("cidade"));		 			  // cidade
				cliente.getEndereco().setBairro(resultado.getString("bairro"));		  			  // bairro
				cliente.getEndereco().setRua(resultado.getString("rua"));			  			  // rua
				cliente.getEndereco().setNumero(String.valueOf(resultado.getInt("numero")));		  			  // numero
				cliente.getEndereco().setComplemento(resultado.getString("complemento")); // pontoref
				cliente.setId(resultado.getInt("id"));								  			  // id
				cliente.setStatusValida��o("autenticado");				
				cliente.getPedEmUso().setId(resultado.getInt("carrinho"));
				cliente.setData(resultado.getTimestamp("data"));
				entidade = cliente; 
				
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 					
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade; 
	}
	
	public EntidadeDominio consultar(EntidadeDominio entidade){ 
		
		openConnection();
		Cliente cliente = (Cliente)entidade;
		String sql = "SELECT * FROM clientes WHERE login = ?";	
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, cliente.getLogin());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 
				if(resultado.getString("senha").equals(cliente.getSenha())){ 
					cliente.setNome(resultado.getString("nome"));
					cliente.setCPF(resultado.getString("cpf"));
					cliente.setDataNascimento(resultado.getString("datanascimento"));
					cliente.setTelefone(resultado.getString("telefone"));
					cliente.setCelular(resultado.getString("celular"));
					cliente.setEmail(resultado.getString("email"));
					cliente.getEndereco().setCep(resultado.getString("cep"));
					cliente.getEndereco().setEstado(resultado.getString("estado"));
					cliente.getEndereco().setCidade(resultado.getString("cidade"));
					cliente.getEndereco().setBairro(resultado.getString("bairro"));
					cliente.getEndereco().setRua(resultado.getString("rua"));
					cliente.getEndereco().setNumero(String.valueOf(resultado.getInt("numero")));
					cliente.getEndereco().setComplemento(resultado.getString("complemento"));
					cliente.setId(resultado.getInt("id"));
					cliente.setStatusValida��o("autenticado");
					cliente.getPedEmUso().setId(resultado.getInt("carrinho"));
					cliente.setData(resultado.getTimestamp("data"));
					
					entidade = cliente; 

					resultado.close();
					preparador.close();
					conexao.close();
					return entidade; 
				}
				else{ 
					cliente.setStatusValida��o("senha errada");
					entidade = cliente; 
					resultado.close();
					preparador.close();
					conexao.close();
					return entidade;
				}				
			}
			else{
				cliente.setStatusValida��o("login errado");
				entidade = cliente;
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		cliente.setStatusValida��o("Erro");
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public boolean verifDuplicidade(EntidadeDominio entidade){ 
		openConnection();
		String sql = "SELECT * FROM clientes";
		PreparedStatement preparador;
		try {
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			Cliente cliente = (Cliente)entidade;
			while(resultado.next()){
				if(resultado.getString("login").equals(cliente.getLogin())){
					if(resultado.getInt("id") == cliente.getId()){ // Verifica se n�o � o mesmo cadastro
						return false;
					}
					else{ // n�o � o mesmo cadastro
						return true;
					}
				}					
			}
			conexao.close();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;    
		}	
		return false; 
	}

	
	@Override
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
		
		String sql = "SELECT * FROM clientes";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			
			if(resultado != null){
				while(resultado.next()){					           
					Cliente cliente = new Cliente();
					cliente.setLogin(resultado.getString("login"));																  // login
					cliente.setSenha(resultado.getString("senha"));																  // senha
					cliente.setNome(resultado.getString("nome"));                       			  // nome
					cliente.setCPF(resultado.getString("cpf"));								  // cpf
					cliente.setDataNascimento(resultado.getString("datanascimento"));				  // datansc
					cliente.setTelefone(resultado.getString("telefone"));							  // telefone
					cliente.setCelular(resultado.getString("celular"));								  // celular
					cliente.setEmail(resultado.getString("email"));						 			  // email
					cliente.getEndereco().setCep(resultado.getString("cep"));						  // cep
					cliente.getEndereco().setEstado(resultado.getString("estado"));					  // estado
					cliente.getEndereco().setCidade(resultado.getString("cidade"));		 			  // cidade
					cliente.getEndereco().setBairro(resultado.getString("bairro"));		  			  // bairro
					cliente.getEndereco().setRua(resultado.getString("rua"));			  			  // rua
					cliente.getEndereco().setNumero(String.valueOf(resultado.getInt("numero")));		  			  // numero
					cliente.getEndereco().setComplemento(resultado.getString("complemento")); // pontoref
					cliente.setId(resultado.getInt("id"));								  			  // id				
					cliente.getPedEmUso().setId(resultado.getInt("carrinho"));
					cliente.setData(resultado.getTimestamp("data"));
					entidades.add(cliente); 
				}				
			}			
			resultado.close(); 
			preparador.close();
			conexao.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}
}
