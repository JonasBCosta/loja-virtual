package br.com.nomedositeinvertido.jdbc.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.nomedositeinvertido.dominio.EntidadeDominio;
import br.com.nomedositeinvertido.dominio.fornecedor.Fornecedor;

public class FornecedorDAO extends AbstractDAO{

	@Override
	public boolean cadastrar(EntidadeDominio entidade) {
		openConnection();
		Fornecedor fornecedor = (Fornecedor)entidade;   
		
		String sql = "INSERT INTO fornecedores (nome, data) " +
				"values (?,?)";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, fornecedor.getNomeFornec());
			preparador.setTimestamp(2, fornecedor.getData());
			preparador.execute();
			preparador.close();			
		} catch (SQLException e) {
			System.out.println("Erro no cadastro");
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean alterar(EntidadeDominio entidade) {
		openConnection();
		Fornecedor fornecedor = (Fornecedor)entidade;   
		String sql = "UPDATE fornecedores SET nome=?, data=? WHERE id=?";		
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(3, fornecedor.getId());
			preparador.setString(1, fornecedor.getNomeFornec());
			preparador.setTimestamp(2, fornecedor.getData());
			preparador.execute();
			preparador.close();
		} catch (SQLException e) {
			System.out.println("Erro na altera��o");
			e.printStackTrace();
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean excluir(EntidadeDominio entidade) {
		openConnection();		
		String sql = "DELETE FROM fornecedores WHERE id=?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			preparador.execute();
			preparador.close();
			System.out.println("Exclu�do com sucesso!");
		} catch (SQLException e) {
			System.out.println("Erro na exclus�o");
			e.printStackTrace();
			return false;
		}		
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public EntidadeDominio consultar(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM fornecedores WHERE id = ?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setInt(1, entidade.getId());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 				
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setNomeFornec(resultado.getString("nome"));
				fornecedor.setData(resultado.getTimestamp("data"));
				entidade = fornecedor; 				
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 					
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade; 
	}

	@Override
	public boolean verifDuplicidade(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM fornecedores";
		PreparedStatement preparador;
		try {
			preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			Fornecedor fornecedor = (Fornecedor)entidade;
			while(resultado.next()){
				if(resultado.getString("nome").equals(fornecedor.getNomeFornec())){
					if(resultado.getInt("id") == fornecedor.getId()){ // Verifica se n�o � o mesmo cadastro
						return false;
					}
					else{ // n�o � o mesmo cadastro
						return true;
					}
				}					
			}
			conexao.close();
			preparador.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;    
		}	
		return false;
	}

	@Override
	public List<EntidadeDominio> consultarTodos(EntidadeDominio entidade) {
		openConnection();
		List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();		
		String sql = "SELECT * FROM fornecedores";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();			
			if(resultado != null){
				while(resultado.next()){					           
					Fornecedor fornecedor = new Fornecedor();
					fornecedor.setNomeFornec(resultado.getString("nome"));
					fornecedor.setId(resultado.getInt("id"));			
					fornecedor.setData(resultado.getTimestamp("data"));
					entidades.add(fornecedor); 
				}				
			}			
			resultado.close(); 
			preparador.close();
			conexao.close();
			return entidades;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidades;
	}
	
	public EntidadeDominio buscar(EntidadeDominio entidade) {
		openConnection();
		String sql = "SELECT * FROM fornecedores WHERE nome = ?";
		try {
			PreparedStatement preparador = conexao.prepareStatement(sql);
			preparador.setString(1, ((Fornecedor)entidade).getNomeFornec());
			ResultSet resultado = preparador.executeQuery();
			if(resultado.next()){ 				
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId(resultado.getInt("id"));
				fornecedor.setData(resultado.getTimestamp("data"));
				entidade = fornecedor; 				
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 					
			}
			else{ 
				resultado.close();
				preparador.close();
				conexao.close();
				return entidade; 
			}				
		} catch (SQLException e) {
			System.out.println("Erro na Busca");
			e.printStackTrace();
		}	
		try {conexao.close(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entidade; 
	}

}
